from django.dispatch import receiver
from realtime import connected_sockets
from realtime.signals import socket_connected, socket_disconnected, \
    socket_client_event, socket_client_message, socket_client_event_by_type
from realtime.util import failure, success
from random import randrange

import serial # Pour utiliser le port serie
import string

ser = serial.Serial('/dev/rfcomm0', 9600, timeout=1) # PORT = /dev/ttyUSB0 (COM0 sur dos)
#ser = ""

tabLeds = {"ledBlanche01":'A',
               "ledBlanche02":'B',
               "ledBlanche03":'C',
               "ledBlanche04":'D',
               "ledBlanche05":'E',
               "ledBlanche06":'F',
               "ledBlanche11":'G',
               "ledBlanche12":'H',
               "ledBlanche13":'I',
               "ledBlanche14":'J',
               "ledBlanche15":'K',
               "ledBlanche16":'L',
               "ledBlanche17":'M',
               "ledBlanche18":'N',
               "ledBlanche19":'O',
               }
semaphore_leds = {"ledBlanche01":0,
               "ledBlanche02":0,
               "ledBlanche03":0,
               "ledBlanche04":0,
               "ledBlanche05":0,
               "ledBlanche06":0,
               "ledBlanche11":0,
               "ledBlanche12":0,
               "ledBlanche13":0,
               "ledBlanche14":0,
               "ledBlanche15":0,
               "ledBlanche16":0,
               "ledBlanche17":0,
               "ledBlanche18":0,
               "ledBlanche19":0,
               }


@receiver(socket_client_event_by_type['echo'])
def handle_echo(sender, request, event, **kwargs):
    return event.args

@receiver(socket_connected)
def handle_connected(sender, request, **kwargs):
    socket = sender
    session = socket.session
    del session['user_name']

@receiver(socket_disconnected)
def handle_disconnected(sender, request, **kwargs):
    socket = sender
    session = socket.session
    namespace = kwargs['namespace']
    if 'user_name' in session:
        namespace.broadcast_event_not_me('system_message', '{0} has left'.format(session['user_name']))

@receiver(socket_client_event_by_type['chat_set_name'])
def handle_set_name(sender, request, event, **kwargs):
    socket = sender
    user_name = event.args[0]
    namespace = kwargs['namespace']
    print user_name
    session = socket.session
    if 'user_name' in session:
        namespace.broadcast_event('system_message', '{0} changed his name to {1}'.format(
            session['user_name'], user_name))
    else:
        namespace.broadcast_event('system_message', '{0} has joined'.format(user_name))

    session['user_name'] = user_name
    return success()

@receiver(socket_client_event_by_type['get_random_number'])
def handle_get_random_number(sender, request, event, **kwargs):
    socket = sender
    namespace = kwargs['namespace']
    irand = randrange(0, 99)
    namespace.broadcast_event('set_random_number', irand)


@receiver(socket_client_event_by_type['allumer_led'])
def handle_allumer_led(sender, request, event, **kwargs):
    socket = sender
    namespace = kwargs['namespace']
    etage = event.args[0]
    led = event.args[1]
    if semaphore_leds[led] == 0:
        print "LED: ", led, " ON, Etage: ", etage, " envoye via USART: ", tabLeds[led]
        #ser.write(tabLeds[led])  
        namespace.broadcast_event('led_allumee', etage, led) 
        semaphore_leds[led] = 1
    return success()  

@receiver(socket_client_event_by_type['allumer_eteindre_led'])
def handle_allumer_eteindre_led(sender, request, event, **kwargs):
    socket = sender
    namespace = kwargs['namespace']
    etat = event.args[0]
    etage = event.args[1]
    led = event.args[2]
    if etat == "eteindre":
        if semaphore_leds[led] == 1:
            print "LED: ", led, " OFF, Etage: ", etage, " envoye via USART: ", tabLeds[led].lower()
            ser.write(tabLeds[led].lower())  
            namespace.broadcast_event('etat_led',"eteinte", etage, led) 
            semaphore_leds[led] = 0
    elif etat == "allumer":
        if semaphore_leds[led] == 0:
            print "LED: ", led, " ON, Etage: ", etage, " envoye via USART: ", tabLeds[led]
            ser.write(tabLeds[led])  
            namespace.broadcast_event('etat_led',"allumee", etage, led) 
            semaphore_leds[led] = 1
    return success() 

@receiver(socket_client_event_by_type['get_etat_des_leds'])
def handle_get_etat_des_leds(sender,request,event,**kwargs):
    socket = sender
    namespace = kwargs['namespace']
    namespace.broadcast_event('etat_des_leds',semaphore_leds) 

@receiver(socket_client_event_by_type['chat_message'])
def handle_chat_message(sender, request, event, **kwargs):
    socket = sender
    session = socket.session

    message = event.args[0]
    namespace = kwargs['namespace']

    if 'user_name' not in session:
        result = failure()
    else:
        namespace.broadcast_event('chat_message', session['user_name'], message)
        result = success()

    return result
