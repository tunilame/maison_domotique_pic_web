
_random:

;maison_domotique_pic16f887.c,2 :: 		int random(){
;maison_domotique_pic16f887.c,3 :: 		srand(1);
	MOVLW      1
	MOVWF      FARG_srand_x+0
	MOVLW      0
	MOVWF      FARG_srand_x+1
	CALL       _srand+0
;maison_domotique_pic16f887.c,4 :: 		return rand()%8999 + 100;
	CALL       _rand+0
	MOVLW      39
	MOVWF      R4+0
	MOVLW      35
	MOVWF      R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVLW      100
	ADDWF      R0+0, 1
	BTFSC      STATUS+0, 0
	INCF       R0+1, 1
;maison_domotique_pic16f887.c,5 :: 		}
	RETURN
; end of _random

_main:

;maison_domotique_pic16f887.c,6 :: 		void main() {
;maison_domotique_pic16f887.c,7 :: 		int n= 0;
;maison_domotique_pic16f887.c,8 :: 		ANSEL = 0; // All I/O are configured as digital
	CLRF       ANSEL+0
;maison_domotique_pic16f887.c,9 :: 		ANSELH = 0;
	CLRF       ANSELH+0
;maison_domotique_pic16f887.c,10 :: 		PORTB = 0x00;  // hexadecimal combination on port B, 0 on all pins
	CLRF       PORTB+0
;maison_domotique_pic16f887.c,11 :: 		TRISB = 0x00; // All port B pins configured as output
	CLRF       TRISB+0
;maison_domotique_pic16f887.c,12 :: 		UART1_Init(115200);               // Initialize UART module at 9600 bps
	MOVLW      10
	MOVWF      SPBRG+0
	BSF        TXSTA+0, 2
	CALL       _UART1_Init+0
;maison_domotique_pic16f887.c,13 :: 		Delay_ms(100);                  // Wait for UART module to stabilize
	MOVLW      3
	MOVWF      R11+0
	MOVLW      138
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_main0:
	DECFSZ     R13+0, 1
	GOTO       L_main0
	DECFSZ     R12+0, 1
	GOTO       L_main0
	DECFSZ     R11+0, 1
	GOTO       L_main0
	NOP
	NOP
;maison_domotique_pic16f887.c,14 :: 		PORTB = 0x0F;
	MOVLW      15
	MOVWF      PORTB+0
;maison_domotique_pic16f887.c,15 :: 		UART1_Write_Text("Start");
	MOVLW      ?lstr1_maison_domotique_pic16f887+0
	MOVWF      FARG_UART1_Write_Text_uart_text+0
	CALL       _UART1_Write_Text+0
;maison_domotique_pic16f887.c,16 :: 		UART1_Write(10);
	MOVLW      10
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;maison_domotique_pic16f887.c,17 :: 		UART1_Write(13);
	MOVLW      13
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;maison_domotique_pic16f887.c,18 :: 		Delay_ms(700);
	MOVLW      18
	MOVWF      R11+0
	MOVLW      194
	MOVWF      R12+0
	MOVLW      102
	MOVWF      R13+0
L_main1:
	DECFSZ     R13+0, 1
	GOTO       L_main1
	DECFSZ     R12+0, 1
	GOTO       L_main1
	DECFSZ     R11+0, 1
	GOTO       L_main1
	NOP
;maison_domotique_pic16f887.c,19 :: 		PORTB = ~PORTB;
	COMF       PORTB+0, 1
;maison_domotique_pic16f887.c,20 :: 		Delay_ms(700);
	MOVLW      18
	MOVWF      R11+0
	MOVLW      194
	MOVWF      R12+0
	MOVLW      102
	MOVWF      R13+0
L_main2:
	DECFSZ     R13+0, 1
	GOTO       L_main2
	DECFSZ     R12+0, 1
	GOTO       L_main2
	DECFSZ     R11+0, 1
	GOTO       L_main2
	NOP
;maison_domotique_pic16f887.c,21 :: 		PORTB = 0x00;
	CLRF       PORTB+0
;maison_domotique_pic16f887.c,22 :: 		while (1) {                     // Endless loop
L_main3:
;maison_domotique_pic16f887.c,23 :: 		while (!UART1_Data_Ready());
L_main5:
	CALL       _UART1_Data_Ready+0
	MOVF       R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L_main6
	GOTO       L_main5
L_main6:
;maison_domotique_pic16f887.c,24 :: 		if (UART1_Data_Ready()){
	CALL       _UART1_Data_Ready+0
	MOVF       R0+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main7
;maison_domotique_pic16f887.c,25 :: 		i = UART1_Read(); // read it
	CALL       _UART1_Read+0
	MOVF       R0+0, 0
	MOVWF      _i+0
;maison_domotique_pic16f887.c,26 :: 		if (i == 'A'){
	MOVF       R0+0, 0
	XORLW      65
	BTFSS      STATUS+0, 2
	GOTO       L_main8
;maison_domotique_pic16f887.c,27 :: 		RB0_bit = 1;
	BSF        RB0_bit+0, 0
;maison_domotique_pic16f887.c,28 :: 		}
	GOTO       L_main9
L_main8:
;maison_domotique_pic16f887.c,29 :: 		else if (i == 'B'){
	MOVF       _i+0, 0
	XORLW      66
	BTFSS      STATUS+0, 2
	GOTO       L_main10
;maison_domotique_pic16f887.c,30 :: 		RB1_bit = 1;
	BSF        RB1_bit+0, 1
;maison_domotique_pic16f887.c,31 :: 		}
	GOTO       L_main11
L_main10:
;maison_domotique_pic16f887.c,32 :: 		else if (i == 'C'){
	MOVF       _i+0, 0
	XORLW      67
	BTFSS      STATUS+0, 2
	GOTO       L_main12
;maison_domotique_pic16f887.c,33 :: 		RB2_bit = 1;
	BSF        RB2_bit+0, 2
;maison_domotique_pic16f887.c,34 :: 		}
	GOTO       L_main13
L_main12:
;maison_domotique_pic16f887.c,35 :: 		else if (i == 'D'){
	MOVF       _i+0, 0
	XORLW      68
	BTFSS      STATUS+0, 2
	GOTO       L_main14
;maison_domotique_pic16f887.c,36 :: 		RB3_bit = 1;
	BSF        RB3_bit+0, 3
;maison_domotique_pic16f887.c,37 :: 		}
	GOTO       L_main15
L_main14:
;maison_domotique_pic16f887.c,38 :: 		else if (i == 'E'){
	MOVF       _i+0, 0
	XORLW      69
	BTFSS      STATUS+0, 2
	GOTO       L_main16
;maison_domotique_pic16f887.c,39 :: 		RB4_bit = 1;
	BSF        RB4_bit+0, 4
;maison_domotique_pic16f887.c,40 :: 		}
	GOTO       L_main17
L_main16:
;maison_domotique_pic16f887.c,41 :: 		else if (i == 'F'){
	MOVF       _i+0, 0
	XORLW      70
	BTFSS      STATUS+0, 2
	GOTO       L_main18
;maison_domotique_pic16f887.c,42 :: 		RB5_bit = 1;
	BSF        RB5_bit+0, 5
;maison_domotique_pic16f887.c,43 :: 		}
	GOTO       L_main19
L_main18:
;maison_domotique_pic16f887.c,44 :: 		else if (i == 'G'){
	MOVF       _i+0, 0
	XORLW      71
	BTFSS      STATUS+0, 2
	GOTO       L_main20
;maison_domotique_pic16f887.c,45 :: 		RB6_bit = 1;
	BSF        RB6_bit+0, 6
;maison_domotique_pic16f887.c,46 :: 		}
	GOTO       L_main21
L_main20:
;maison_domotique_pic16f887.c,47 :: 		else if (i == 'H'){
	MOVF       _i+0, 0
	XORLW      72
	BTFSS      STATUS+0, 2
	GOTO       L_main22
;maison_domotique_pic16f887.c,48 :: 		RB7_bit = 1;
	BSF        RB7_bit+0, 7
;maison_domotique_pic16f887.c,49 :: 		}
	GOTO       L_main23
L_main22:
;maison_domotique_pic16f887.c,50 :: 		else if (i == 'a'){
	MOVF       _i+0, 0
	XORLW      97
	BTFSS      STATUS+0, 2
	GOTO       L_main24
;maison_domotique_pic16f887.c,51 :: 		RB0_bit = 0;
	BCF        RB0_bit+0, 0
;maison_domotique_pic16f887.c,52 :: 		}
	GOTO       L_main25
L_main24:
;maison_domotique_pic16f887.c,53 :: 		else if (i == 'b'){
	MOVF       _i+0, 0
	XORLW      98
	BTFSS      STATUS+0, 2
	GOTO       L_main26
;maison_domotique_pic16f887.c,54 :: 		RB1_bit = 0;
	BCF        RB1_bit+0, 1
;maison_domotique_pic16f887.c,55 :: 		}
	GOTO       L_main27
L_main26:
;maison_domotique_pic16f887.c,56 :: 		else if (i == 'c'){
	MOVF       _i+0, 0
	XORLW      99
	BTFSS      STATUS+0, 2
	GOTO       L_main28
;maison_domotique_pic16f887.c,57 :: 		RB2_bit = 0;
	BCF        RB2_bit+0, 2
;maison_domotique_pic16f887.c,58 :: 		}
	GOTO       L_main29
L_main28:
;maison_domotique_pic16f887.c,59 :: 		else if (i == 'd'){
	MOVF       _i+0, 0
	XORLW      100
	BTFSS      STATUS+0, 2
	GOTO       L_main30
;maison_domotique_pic16f887.c,60 :: 		RB3_bit = 0;
	BCF        RB3_bit+0, 3
;maison_domotique_pic16f887.c,61 :: 		}
	GOTO       L_main31
L_main30:
;maison_domotique_pic16f887.c,62 :: 		else if (i == 'e'){
	MOVF       _i+0, 0
	XORLW      101
	BTFSS      STATUS+0, 2
	GOTO       L_main32
;maison_domotique_pic16f887.c,63 :: 		RB4_bit = 0;
	BCF        RB4_bit+0, 4
;maison_domotique_pic16f887.c,64 :: 		}
	GOTO       L_main33
L_main32:
;maison_domotique_pic16f887.c,65 :: 		else if (i == 'f'){
	MOVF       _i+0, 0
	XORLW      102
	BTFSS      STATUS+0, 2
	GOTO       L_main34
;maison_domotique_pic16f887.c,66 :: 		RB5_bit = 0;
	BCF        RB5_bit+0, 5
;maison_domotique_pic16f887.c,67 :: 		}
	GOTO       L_main35
L_main34:
;maison_domotique_pic16f887.c,68 :: 		else if (i == 'g'){
	MOVF       _i+0, 0
	XORLW      103
	BTFSS      STATUS+0, 2
	GOTO       L_main36
;maison_domotique_pic16f887.c,69 :: 		RB6_bit = 0;
	BCF        RB6_bit+0, 6
;maison_domotique_pic16f887.c,70 :: 		}
	GOTO       L_main37
L_main36:
;maison_domotique_pic16f887.c,71 :: 		else if (i == 'h'){
	MOVF       _i+0, 0
	XORLW      104
	BTFSS      STATUS+0, 2
	GOTO       L_main38
;maison_domotique_pic16f887.c,72 :: 		RB7_bit = 0;
	BCF        RB7_bit+0, 7
;maison_domotique_pic16f887.c,73 :: 		}
L_main38:
L_main37:
L_main35:
L_main33:
L_main31:
L_main29:
L_main27:
L_main25:
L_main23:
L_main21:
L_main19:
L_main17:
L_main15:
L_main13:
L_main11:
L_main9:
;maison_domotique_pic16f887.c,74 :: 		}
L_main7:
;maison_domotique_pic16f887.c,76 :: 		}
	GOTO       L_main3
;maison_domotique_pic16f887.c,77 :: 		}
	GOTO       $+0
; end of _main
