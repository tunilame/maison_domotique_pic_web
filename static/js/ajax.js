i = 0;
function getXMLHttpRequest() {
	var xhr = null;

	if (window.XMLHttpRequest || window.ActiveXObject) {
		if (window.ActiveXObject) {
			try {
				xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		} else {
			xhr = new XMLHttpRequest();
		}
	} else {
		alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
		return null;
	}

	return xhr;
}
function afficherMsg(msg, type,name,etage) {
	if (type == "error")
		document.getElementById("error"+etage+"0").innerHTML = '<div class="error">'
				+ msg + '</div>';
	else
		document.getElementById("error"+etage+"0").innerHTML = '<div class="success"> LED '
			+ name + ' allumé/éteinte!</div>';
	document.getElementById("error"+etage+"0").style.visibility = "visible";
	setTimeout(function() {
		document.getElementById("error"+etage+"0").style.visibility = "hidden"
	}, 3000);

	/*
	 * $('#error00').show(2000); $('#error00').hide(2000);
	 */
}
function reponsePHP(sData,name,etage) {
	if (sData == "Succes") {
		afficherMsg(sData, "success",name,etage);
	} else {
		afficherMsg(sData, "error",name,etage);
	}
}
function Allumer_Eteindre(led,etage) {
	if (led.checked) { // Allumer
		i = 1;
		socket.emit('allumer_eteindre_led','allumer',etage, led.name, function() {
		});
		
		console.log("ENVOYE: LED allumé: "+led.name+" Etage:"+etage);
	} else {			// Eteindre
		i = 0;
		
		socket.emit('allumer_eteindre_led','eteindre',etage, led.name, function() {
		});
		
		console.log("ENVOYE: LED etteinte: "+led.name+" Etage:"+etage);
	}
	
}
$(":checkbox").switchbutton({
	classes : 'ui-switchbutton-thin',
	duration : 100,
	checkedLabel : 'I',
	uncheckedLabel : 'O'
});
for (j = 1; j < 7; j++) {
	$("#led0" + j).change(function() {
		Allumer_Eteindre(this,0);
	});
}
for (j = 1; j < 10; j++) {
	$("#led1" + j).change(function() {
		Allumer_Eteindre(this,1);
	});
}