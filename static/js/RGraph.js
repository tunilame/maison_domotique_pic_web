    /**
    * o------------------------------------------------------------------------------o
    * | This file is part of the RGraph package - you can learn more at:             |
    * |                                                                              |
    * |                          http://www.rgraph.net                               |
    * |                                                                              |
    * | This package is licensed under the RGraph license. For all kinds of business |
    * | purposes there is a small one-time licensing fee to pay and for non          |
    * | commercial  purposes it is free to use. You can read the full license here:  |
    * |                                                                              |
    * |                      http://www.rgraph.net/license                           |
    * o------------------------------------------------------------------------------o
    */

    /**
    * Initialise the various objects
    */
    if (typeof(RGraph) == 'undefined') RGraph = {isRGraph:true,type:'common'};

    RGraph.Registry       = {};
    RGraph.Registry.store = [];
    RGraph.Registry.store['chart.event.handlers']       = [];
    RGraph.Registry.store['__rgraph_event_listeners__'] = []; // Used in the new system for tooltips
    RGraph.background     = {};
    RGraph.objects        = [];
    RGraph.Resizing       = {};
    RGraph.events         = [];
    RGraph.cursor         = [];

    RGraph.ObjectRegistry                    = {};
    RGraph.ObjectRegistry.objects            = {};
    RGraph.ObjectRegistry.objects.byUID      = [];
    RGraph.ObjectRegistry.objects.byCanvasID = [];


    /**
    * Some "constants"
    */
    PI       = Math.PI;
    HALFPI   = PI / 2;
    TWOPI    = PI * 2;
    ISFF     = navigator.userAgent.indexOf('Firefox') != -1;
    ISOPERA  = navigator.userAgent.indexOf('Opera') != -1;
    ISCHROME = navigator.userAgent.indexOf('Chrome') != -1;
    ISSAFARI = navigator.userAgent.indexOf('Safari') != -1;
    ISWEBKIT = navigator.userAgent.indexOf('WebKit') != -1;
    //ISIE     is defined below
    //ISIE6    is defined below
    //ISIE7    is defined below
    //ISIE8    is defined below
    //ISIE9    is defined below
    //ISIE9    is defined below
    //ISIE9UP  is defined below
    //ISIE10   is defined below
    //ISIE10UP is defined below
    //ISOLD    is defined below


    /**
    * Returns five values which are used as a nice scale
    * 
    * @param  max int    The maximum value of the graph
    * @param  obj object The graph object
    * @return     array   An appropriate scale
    */
    RGraph.getScale = function (max, obj)
    {
        /**
        * Special case for 0
        */
        if (max == 0) {
            return ['0.2', '0.4', '0.6', '0.8', '1.0'];
        }

        var original_max = max;

        /**
        * Manually do decimals
        */
        if (max <= 1) {
            if (max > 0.5) {
                return [0.2,0.4,0.6,0.8, Number(1).toFixed(1)];

            } else if (max >= 0.1) {
                return obj.Get('chart.scale.round') ? [0.2,0.4,0.6,0.8,1] : [0.1,0.2,0.3,0.4,0.5];

            } else {

                var tmp = max;
                var exp = 0;

                while (tmp < 1.01) {
                    exp += 1;
                    tmp *= 10;
                }

                var ret = ['2e-' + exp, '4e-' + exp, '6e-' + exp, '8e-' + exp, '10e-' + exp];


                if (max <= ('5e-' + exp)) {
                    ret = ['1e-' + exp, '2e-' + exp, '3e-' + exp, '4e-' + exp, '5e-' + exp];
                }

                return ret;
            }
        }

        // Take off any decimals
        if (String(max).indexOf('.') > 0) {
            max = String(max).replace(/\.\d+$/, '');
        }

        var interval = Math.pow(10, Number(String(Number(max)).length - 1));
        var topValue = interval;

        while (topValue < max) {
            topValue += (interval / 2);
        }

        // Handles cases where the max is (for example) 50.5
        if (Number(original_max) > Number(topValue)) {
            topValue += (interval / 2);
        }

        // Custom if the max is greater than 5 and less than 10
        if (max < 10) {
            topValue = (Number(original_max) <= 5 ? 5 : 10);
        }
        
        /**
        * Added 02/11/2010 to create "nicer" scales
        */
        if (obj && typeof(obj.Get('chart.scale.round')) == 'boolean' && obj.Get('chart.scale.round')) {
            topValue = 10 * interval;
        }

        return [topValue * 0.2, topValue * 0.4, topValue * 0.6, topValue * 0.8, topValue];
    }
























    /**
    * Returns an appropriate scale. The return value is actualy anm object consiosting of:
    *  scale.max
    *  scale.min
    *  scale.scale
    * 
    * @param  obj object  The graph object
    * @param  prop object An object consisting of configuration properties
    * @return     object  An object containg scale information
    */
    RGraph.getScale2 = function (obj, opt)
    {
        var RG   = RGraph;
        var ca   = obj.canvas;
        var co   = obj.context;
        var prop = obj.properties;
        
        var numlabels    = typeof(opt['ylabels.count']) == 'number' ? opt['ylabels.count'] : 5;
        var units_pre    = typeof(opt['units.pre']) == 'string' ? opt['units.pre'] : '';
        var units_post   = typeof(opt['units.post']) == 'string' ? opt['units.post'] : '';
        var max          = Number(opt['max']);
        var min          = typeof(opt['min']) == 'number' ? opt['min'] : 0;
        var strict       = opt['strict'];
        var decimals     = Number(opt['scale.decimals']); // Sometimes the default is null
        var point        = opt['scale.point']; // Default is a string in all chart libraries so no need to cast it
        var thousand     = opt['scale.thousand']; // Default is a string in all chart libraries so no need to cast it
        var original_max = max;
        var round        = opt['scale.round'];
        var scale        = {'max':1,'labels':[]};



        /**
        * Special case for 0
        * 
        * ** Must be first **
        */
        if (!max) {

            var max   = 1;

            var scale = {max:1,min:0,labels:[]};

            for (var i=0; i<numlabels; ++i) {
                var label = ((((max - min) / numlabels) + min) * (i + 1)).toFixed(decimals);
                scale.labels.push(units_pre + label + units_post);
            }

        /**
        * Manually do decimals
        */
        } else if (max <= 1 && !strict) {

            if (max > 0.5) {

                max  = 1;
                min  = min;
                scale.min = min;

                for (var i=0; i<numlabels; ++i) {
                    var label = ((((max - min) / numlabels) * (i + 1)) + min).toFixed(decimals);

                    scale.labels.push(units_pre + label + units_post);
                }

            } else if (max >= 0.1) {
                
                max   = 0.5;
                min   = min;
                scale = {'max': 0.5, 'min':min,'labels':[]}

                for (var i=0; i<numlabels; ++i) {
                    var label = ((((max - min) / numlabels) + min) * (i + 1)).toFixed(decimals);
                    scale.labels.push(units_pre + label + units_post);
                }

            } else {
                scale = {'min':min,'labels':[]}
                var max_str = String(max);
                
                if (max_str.indexOf('e') > 0) {
                    var numdecimals = Math.abs(max_str.substring(max_str.indexOf('e') + 1));
                } else {
                    var numdecimals = String(max).length - 2;
                }

                var max = 1  / Math.pow(10,numdecimals - 1);

                for (var i=0; i<numlabels; ++i) {
                    var label = ((((max - min) / numlabels) + min) * (i + 1));
                        label = label.toExponential();
                        label = label.split(/e/);
                        label[0] = Math.round(label[0]);
                        label = label.join('e');
                    scale.labels.push(label);
                }

                //This makes the top scale value of the format 10e-2 instead of 1e-1
                tmp = scale.labels[scale.labels.length - 1].split(/e/);
                tmp[0] += 0;
                tmp[1] = Number(tmp[1]) - 1;
                tmp = tmp[0] + 'e' + tmp[1];
                scale.labels[scale.labels.length - 1] = tmp;
                
                // Add the units
                for (var i=0; i<scale.labels.length ; ++i) {
                    scale.labels[i] = units_pre + scale.labels[i] + units_post;
                }
                
                scale.max = Number(max);
            }


        } else if (!strict) {

    
            /**
            * Now comes the scale handling for integer values
            */


            // This accomodates decimals by rounding the max up to the next integer
            max = Math.ceil(max);

            var interval = Math.pow(10, Number(String(Number(max) - Number(min)).length - 1));
    
            var topValue = interval;
    
            while (topValue < max) {
                topValue += (interval / 2);
            }
    
            // Handles cases where the max is (for example) 50.5
            if (Number(original_max) > Number(topValue)) {
                topValue += (interval / 2);
            }
    
            // Custom if the max is greater than 5 and less than 10
            if (max <= 10) {
                topValue = (Number(original_max) <= 5 ? 5 : 10);
            }
    
    
            // Added 02/11/2010 to create "nicer" scales
            if (obj && typeof(round) == 'boolean' && round) {
                topValue = 10 * interval;
            }

            scale.max = topValue;

            // Now generate the scale. Temporarily set the objects chart.scale.decimal and chart.scale.point to those
            //that we've been given as the number_format functuion looks at those instead of using argumrnts.
            var tmp_point    = prop['chart.scale.point'];
            var tmp_thousand = prop['chart.scale.thousand'];

            obj.Set('chart.scale.thousand', thousand);
            obj.Set('chart.scale.point', point);


            for (var i=0; i<numlabels; ++i) {
                scale.labels.push( RG.number_format(obj, ((((i+1) / numlabels) * (topValue - min)) + min).toFixed(decimals), units_pre, units_post) );
            }

            obj.Set('chart.scale.thousand', tmp_thousand);
            obj.Set('chart.scale.point', tmp_point);
        
        } else if (typeof(max) == 'number' && strict) {

            /**
            * ymax is set and also strict
            */
            for (var i=0; i<numlabels; ++i) {
                scale.labels.push( RG.number_format(obj, ((((i+1) / numlabels) * (max - min)) + min).toFixed(decimals), units_pre, units_post) );
            }
            
            // ???
            scale.max = max;
        }

        
        scale.units_pre  = units_pre;
        scale.units_post = units_post;
        scale.point      = point;
        scale.decimals   = decimals;
        scale.thousand   = thousand;
        scale.numlabels  = numlabels;
        scale.round      = Boolean(round);
        scale.min        = min;


        return scale;
    }












    /**
    * Returns the maximum numeric value which is in an array
    * 
    * @param  array arr The array (can also be a number, in which case it's returned as-is)
    * @param  int       Whether to ignore signs (ie negative/positive)
    * @return int       The maximum value in the array
    */
    RGraph.array_max = function (arr)
    {
        var max       = null;
        var MathLocal = Math;
        
        if (typeof(arr) == 'number') {
            return arr;
        }
        
        for (var i=0,len=arr.length; i<len; ++i) {
            if (typeof(arr[i]) == 'number') {

                var val = arguments[1] ? MathLocal.abs(arr[i]) : arr[i];
                
                if (typeof max == 'number') {
                    max = MathLocal.max(max, val);
                } else {
                    max = val;
                }
            }
        }
        
        return max;
    }


    /**
    * Returns the maximum value which is in an array
    * 
    * @param  array arr The array
    * @param  int   len The length to pad the array to
    * @param  mixed     The value to use to pad the array (optional)
    */
    RGraph.array_pad = function (arr, len)
    {
        if (arr.length < len) {
            var val = arguments[2] ? arguments[2] : null;
            
            for (var i=arr.length; i<len; ++i) {
                arr[i] = val;
            }
        }
        
        return arr;
    }





    /**
    * An array sum function
    * 
    * @param  array arr The  array to calculate the total of
    * @return int       The summed total of the arrays elements
    */
    RGraph.array_sum = function (arr)
    {
        // Allow integers
        if (typeof(arr) == 'number') {
            return arr;
        }

        var i, sum;
        var len = arr.length;

        for(i=0,sum=0;i<len;sum+=arr[i++]);
        return sum;
    }





    /**
    * Takes any number of arguments and adds them to one big linear array
    * which is then returned
    * 
    * @param ... mixed The data to linearise. You can strings, booleans, numbers or arrays
    */
    RGraph.array_linearize = function ()
    {
        var arr  = [];
        var args = arguments;

        for (var i=0,len=args.length; i<len; ++i) {

            if (typeof(args[i]) == 'object' && args[i]) {
                for (var j=0; j<args[i].length; ++j) {
                    var sub = RGraph.array_linearize(args[i][j]);
                    
                    for (var k=0; k<sub.length; ++k) {
                        arr.push(sub[k]);
                    }
                }
            } else {
                arr.push(args[i]);
            }
        }

        return arr;
    }





    /**
    * This is a useful function which is basically a shortcut for drawing left, right, top and bottom alligned text.
    * 
    * @param object context The context
    * @param string font    The font
    * @param int    size    The size of the text
    * @param int    x       The X coordinate
    * @param int    y       The Y coordinate
    * @param string text    The text to draw
    * @parm  string         The vertical alignment. Can be null. "center" gives center aligned  text, "top" gives top aligned text.
    *                       Anything else produces bottom aligned text. Default is bottom.
    * @param  string        The horizontal alignment. Can be null. "center" gives center aligned  text, "right" gives right aligned text.
    *                       Anything else produces left aligned text. Default is left.
    * @param  bool          Whether to show a bounding box around the text. Defaults not to
    * @param int            The angle that the text should be rotate at (IN DEGREES)
    * @param string         Background color for the text
    * @param bool           Whether the text is bold or not
    */
    RGraph.Text = function (context, font, size, x, y, text)
    {
        // "Cache" the args as a local variable
        var args = arguments;

        // Handle undefined - change it to an empty string
        if ((typeof(text) != 'string' && typeof(text) != 'number') || text == 'undefined') {
            return;
        }




        /**
        * This accommodates multi-line text
        */
        if (typeof(text) == 'string' && text.match(/\r\n/)) {

            var dimensions = RGraph.MeasureText('M', args[11], font, size);

            /**
            * Measure the text (width and height)
            */

            var arr = text.split('\r\n');

            /**
            * Adjust the Y position
            */
            
            // This adjusts the initial y position
            if (args[6] && args[6] == 'center') y = (y - (dimensions[1] * ((arr.length - 1) / 2)));

            for (var i=1; i<arr.length; ++i) {
    
                RGraph.Text(context,
                            font,
                            size,
                            args[9] == -90 ? (x + (size * 1.5)) : x,
                            y + (dimensions[1] * i),
                            arr[i],
                            args[6] ? args[6] : null,
                            args[7],
                            args[8],
                            args[9],
                            args[10],
                            args[11],
                            args[12]);
            }
            
            // Update text to just be the first line
            text = arr[0];
        }


        // Accommodate MSIE
        if (document.all && ISOLD) {
            y += 2;
        }


        context.font = (args[11] ? 'Bold ': '') + size + 'pt ' + font;

        var i;
        var origX = x;
        var origY = y;
        var originalFillStyle = context.fillStyle;
        var originalLineWidth = context.lineWidth;

        // Need these now the angle can be specified, ie defaults for the former two args
        if (typeof(args[6])  == 'undefined') args[6]  = 'bottom'; // Vertical alignment. Default to bottom/baseline
        if (typeof(args[7])  == 'undefined') args[7]  = 'left';   // Horizontal alignment. Default to left
        if (typeof(args[8])  == 'undefined') args[8]  = null;     // Show a bounding box. Useful for positioning during development. Defaults to false
        if (typeof(args[9])  == 'undefined') args[9]  = 0;        // Angle (IN DEGREES) that the text should be drawn at. 0 is middle right, and it goes clockwise

        // The alignment is recorded here for purposes of Opera compatibility
        if (navigator.userAgent.indexOf('Opera') != -1) {
            context.canvas.__rgraph_valign__ = args[6];
            context.canvas.__rgraph_halign__ = args[7];
        }

        // First, translate to x/y coords
        context.save();

            context.canvas.__rgraph_originalx__ = x;
            context.canvas.__rgraph_originaly__ = y;

            context.translate(x, y);
            x = 0;
            y = 0;

            // Rotate the canvas if need be
            if (args[9]) {
                context.rotate(args[9] / (180 / PI));
            }


            // Vertical alignment - defaults to bottom
            if (args[6]) {

                var vAlign = args[6];

                if (vAlign == 'center') {
                    context.textBaseline = 'middle';
                } else if (vAlign == 'top') {
                    context.textBaseline = 'top';
                }
            }


            // Hoeizontal alignment - defaults to left
            if (args[7]) {

                var hAlign = args[7];
                var width  = context.measureText(text).width;
    
                if (hAlign) {
                    if (hAlign == 'center') {
                        context.textAlign = 'center';
                    } else if (hAlign == 'right') {
                        context.textAlign = 'right';
                    }
                }
            }
            
            
            context.fillStyle = originalFillStyle;

            /**
            * Draw a bounding box if requested
            */
            context.save();
                 context.fillText(text,0,0);
                 context.lineWidth = 1;

                var width = context.measureText(text).width;
                var width_offset = (hAlign == 'center' ? (width / 2) : (hAlign == 'right' ? width : 0));
                var height = size * 1.5; // !!!
                var height_offset = (vAlign == 'center' ? (height / 2) : (vAlign == 'top' ? height : 0));
                var ieOffset = ISOLD ? 2 : 0;

                if (args[8]) {

                    context.strokeRect(-3 - width_offset,
                                       0 - 3 - height - ieOffset + height_offset,
                                       width + 6,
                                       height + 6);
                    /**
                    * If requested, draw a background for the text
                    */
                    if (args[10]) {
                        context.fillStyle = args[10];
                        context.fillRect(-3 - width_offset,
                                           0 - 3 - height - ieOffset + height_offset,
                                           width + 6,
                                           height + 6);
                    }

                    
                    context.fillStyle = originalFillStyle;


                    /**
                    * Do the actual drawing of the text
                    */
                    context.fillText(text,0,0);
                }
            context.restore();
            
            // Reset the lineWidth
            context.lineWidth = originalLineWidth;

        context.restore();
    }





    /**
    * Clears the canvas by setting the width. You can specify a colour if you wish.
    * 
    * @param object canvas The canvas to clear
    */
    RGraph.Clear = function (ca)
    {
        var RG    = RGraph;
        var co    = ca.getContext('2d');
        var color = arguments[1];

        if (!ca) {
            return;
        }
        
        RGraph.FireCustomEvent(ca.__object__, 'onbeforeclear');

        if (ISIE8 && !color) {
            color = 'white';
        }

        /**
        * Can now clear the canvas back to fully transparent
        */
        if (!color || (color && color == 'rgba(0,0,0,0)' || color == 'transparent')) {

            co.clearRect(0,0,ca.width, ca.height);
            
            // Reset the globalCompositeOperation
            co.globalCompositeOperation = 'source-over';

        } else {

            co.fillStyle = color;
            co.beginPath();

            if (ISIE8) {
                co.fillRect(0,0,ca.width,ca.height);
            } else {
                co.fillRect(-10,-10,ca.width + 20,ca.height + 20);
            }

            co.fill();
        }
        
        //if (RG.ClearAnnotations) {
            //RG.ClearAnnotations(ca.id);
        //}
        
        /**
        * This removes any background image that may be present
        */
        if (RG.Registry.Get('chart.background.image.' + ca.id)) {
            var img = RG.Registry.Get('chart.background.image.' + ca.id);
            img.style.position = 'absolute';
            img.style.left     = '-10000px';
            img.style.top      = '-10000px';
        }
        
        /**
        * This hides the tooltip that is showing IF it has the same canvas ID as
        * that which is being cleared
        */
        if (RG.Registry.Get('chart.tooltip')) {
            RG.HideTooltip(ca);
            //RG.Redraw();
        }

        /**
        * Set the cursor to default
        */
        ca.style.cursor = 'default';

        RG.FireCustomEvent(ca.__object__, 'onclear');
    }





    /**
    * Draws the title of the graph
    * 
    * @param object  canvas The canvas object
    * @param string  text   The title to write
    * @param integer gutter The size of the gutter
    * @param integer        The center X point (optional - if not given it will be generated from the canvas width)
    * @param integer        Size of the text. If not given it will be 14
    */
    RGraph.DrawTitle = function (obj, text, gutterTop)
    {
        var ca = canvas  = obj.canvas;
        var co = context = obj.context;
        var prop         = obj.properties;

        var gutterLeft   = prop['chart.gutter.left'];
        var gutterRight  = prop['chart.gutter.right'];
        var gutterTop    = gutterTop;
        var gutterBottom = prop['chart.gutter.bottom'];
        var size         = arguments[4] ? arguments[4] : 12;
        var bold         = prop['chart.title.bold'];
        var centerx      = (arguments[3] ? arguments[3] : ((ca.width - gutterLeft - gutterRight) / 2) + gutterLeft);
        var keypos       = prop['chart.key.position'];
        var vpos         = prop['chart.title.vpos'];
        var hpos         = prop['chart.title.hpos'];
        var bgcolor      = prop['chart.title.background'];
        var x            = prop['chart.title.x'];
        var y            = prop['chart.title.y'];
        var halign       = 'center';
        var valign       = 'center';

        // Account for 3D effect by faking the key position
        if (obj.type == 'bar' && prop['chart.variant'] == '3d') {
            keypos = 'gutter';
        }

        co.beginPath();
        co.fillStyle = prop['chart.text.color'] ? prop['chart.text.color'] : 'black';





        /**
        * Vertically center the text if the key is not present
        */
        if (keypos && keypos != 'gutter') {
            var valign = 'center';

        } else if (!keypos) {
            var valign = 'center';

        } else {
            var valign = 'bottom';
        }





        // if chart.title.vpos is a number, use that
        if (typeof(prop['chart.title.vpos']) == 'number') {
            vpos = prop['chart.title.vpos'] * gutterTop;

            if (prop['chart.xaxispos'] == 'top') {
                vpos = prop['chart.title.vpos'] * gutterBottom + gutterTop + (ca.height - gutterTop - gutterBottom);
            }

        } else {
            vpos = gutterTop - size - 5;

            if (prop['chart.xaxispos'] == 'top') {
                vpos = ca.height  - gutterBottom + size + 5;
            }
        }




        // if chart.title.hpos is a number, use that. It's multiplied with the (entire) canvas width
        if (typeof(hpos) == 'number') {
            centerx = hpos * ca.width;
        }

        /**
        * Now the chart.title.x and chart.title.y settings override (is set) the above
        */
        if (typeof(x) == 'number') centerx = x;
        if (typeof(y) == 'number') vpos    = y;




        /**
        * Horizontal alignment can now (Jan 2013) be specified
        */
        if (typeof(prop['chart.title.halign']) == 'string') {
            halign = prop['chart.title.halign'];
        }
        
        /**
        * Vertical alignment can now (Jan 2013) be specified
        */
        if (typeof(prop['chart.title.valign']) == 'string') {
            valign = prop['chart.title.valign'];
        }




        
        // Set the colour
        if (typeof(prop['chart.title.color'] != null)) {
            var oldColor = co.fillStyle
            var newColor = prop['chart.title.color']
            co.fillStyle = newColor ? newColor : 'black';
        }




        /**
        * Default font is Arial
        */
        var font = prop['chart.text.font'];




        /**
        * Override the default font with chart.title.font
        */
        if (typeof(prop['chart.title.font']) == 'string') {
            font = prop['chart.title.font'];
        }




        /**
        * Draw the title
        */
        RGraph.Text2(obj,{'font':font,
                          'size':size,
                          'x':centerx,
                          'y':vpos,
                          'text':text,
                          'valign':valign,
                          'halign':halign,
                          'bounding':bgcolor != null,
                          'bounding.fill':bgcolor,
                          'bold':bold,
                          'tag':'title'
                         });
        
        // Reset the fill colour
        co.fillStyle = oldColor;
    }





    /**
    * This function returns the mouse position in relation to the canvas
    * 
    * @param object e The event object.
    *
    RGraph.getMouseXY = function (e)
    {
        var el = (ISOLD ? event.srcElement : e.target);
        var x;
        var y;

        // ???
        var paddingLeft = el.style.paddingLeft ? parseInt(el.style.paddingLeft) : 0;
        var paddingTop  = el.style.paddingTop ? parseInt(el.style.paddingTop) : 0;
        var borderLeft  = el.style.borderLeftWidth ? parseInt(el.style.borderLeftWidth) : 0;
        var borderTop   = el.style.borderTopWidth  ? parseInt(el.style.borderTopWidth) : 0;
        
        if (ISIE8) e = event;

        // Browser with offsetX and offsetY
        if (typeof(e.offsetX) == 'number' && typeof(e.offsetY) == 'number') {
            x = e.offsetX;
            y = e.offsetY;

        // FF and other
        } else {
            x = 0;
            y = 0;

            while (el != document.body && el) {
                x += el.offsetLeft;
                y += el.offsetTop;

                el = el.offsetParent;
            }

            x = e.pageX - x;
            y = e.pageY - y;
        }

        return [x, y];
    }*/


    RGraph.getMouseXY = function(e)
    {
        var el      = e.target;
        var ca      = el;
        var caStyle = ca.style;
        var offsetX = 0;
        var offsetY = 0;
        var x;
        var y;
        var ISFIXED     = (ca.style.position == 'fixed');
        var borderLeft  = parseInt(caStyle.borderLeftWidth) || 0;
        var borderTop   = parseInt(caStyle.borderTopWidth) || 0;
        var paddingLeft = parseInt(caStyle.paddingLeft) || 0
        var paddingTop  = parseInt(caStyle.paddingTop) || 0
        var additionalX = borderLeft + paddingLeft;
        var additionalY = borderTop + paddingTop;


        if (typeof(e.offsetX) == 'number' && typeof(e.offsetY) == 'number') {

            if (ISFIXED) {
                if (ISOPERA) {
                    x = e.offsetX;
                    y = e.offsetY;
                
                } else if (ISWEBKIT) {
                    x = e.offsetX - paddingLeft - borderLeft;
                    y = e.offsetY - paddingTop - borderTop;
                
                } else if (ISIE) {
                    x = e.offsetX - paddingLeft;
                    y = e.offsetY - paddingTop;
    
                } else {
                    x = e.offsetX;
                    y = e.offsetY;
                }
    
    
    
    
            } else {
    
    
    
    
                if (!ISIE && !ISOPERA) {
                    x = e.offsetX - borderLeft - paddingLeft;
                    y = e.offsetY - borderTop - paddingTop;
                
                } else if (ISIE) {
                    x = e.offsetX - paddingLeft;
                    y = e.offsetY - paddingTop;
                
                } else {
                    x = e.offsetX;
                    y = e.offsetY;
                }
            }

        /**
        * This is for Firefox - which doesn't support .offsetX or .offsetY
        */
        } else if (e.layerX && e.layerY) {

            // Give the canvas position if it has none
            if (!ca.style.position) {
                ca.style.position = 'relative';
                ca.style.top = 0;
                ca.style.left = 0;
            }
                
            x = e.layerX - borderLeft - paddingLeft;
            y = e.layerY - borderTop - paddingTop;


            if (ISFIXED) {
                x = e.layerX - borderLeft - paddingLeft;
                y = e.layerY - borderTop - paddingTop;
            }

        } else {

            if (typeof(el.offsetParent) != 'undefined') {
                do {
                    offsetX += el.offsetLeft;
                    offsetY += el.offsetTop;
                } while ((el = el.offsetParent));
            }

            x = e.pageX - offsetX - additionalX;
            y = e.pageY - offsetY - additionalY;

            x -= (2 * (parseInt(document.body.style.borderLeftWidth) || 0));
            y -= (2 * (parseInt(document.body.style.borderTopWidth) || 0));

            x += (parseInt(caStyle.borderLeftWidth) || 0);
            y += (parseInt(caStyle.borderTopWidth) || 0);
        }

        // We return a javascript array with x and y defined
        return [x, y];
    }



    /**
    * This function returns a two element array of the canvas x/y position in
    * relation to the page
    * 
    * @param object canvas
    */
    RGraph.getCanvasXY = function (canvas)
    {
        var x  = 0;
        var y  = 0;
        var el = canvas; // !!!

        do {

            x += el.offsetLeft;
            y += el.offsetTop;
            
            // ACCOUNT FOR TABLES IN wEBkIT
            if (el.tagName.toLowerCase() == 'table' && (ISCHROME || ISSAFARI)) {
                x += parseInt(el.border) || 0;
                y += parseInt(el.border) || 0;
            }

            el = el.offsetParent;

        } while (el && el.tagName.toLowerCase() != 'body');


        var paddingLeft = canvas.style.paddingLeft ? parseInt(canvas.style.paddingLeft) : 0;
        var paddingTop  = canvas.style.paddingTop ? parseInt(canvas.style.paddingTop) : 0;
        var borderLeft  = canvas.style.borderLeftWidth ? parseInt(canvas.style.borderLeftWidth) : 0;
        var borderTop   = canvas.style.borderTopWidth  ? parseInt(canvas.style.borderTopWidth) : 0;

        if (navigator.userAgent.indexOf('Firefox') > 0) {
            x += parseInt(document.body.style.borderLeftWidth) || 0;
            y += parseInt(document.body.style.borderTopWidth) || 0;
        }

        return [x + paddingLeft + borderLeft, y + paddingTop + borderTop];
    }



    /**
    * This function determines whther a canvas is fixed (CSS positioning) or not. If not it returns
    * false. If it is then the element that is fixed is returned (it may be a parent of the canvas).
    * 
    * @return Either false or the fixed positioned element
    */
    RGraph.isFixed = function (canvas)
    {
        var obj = canvas;
        var i = 0;

        while (obj.tagName.toLowerCase() != 'body' && i < 99) {

            if (obj.style.position == 'fixed') {
                return obj;
            }
            
            obj = obj.offsetParent;
        }

        return false;
    }




    /**
    * Registers a graph object (used when the canvas is redrawn)
    * 
    * @param object obj The object to be registered
    */
    RGraph.Register = function (obj)
    {
        // Checking this property ensures the object is only registered once
        if (!obj.Get('chart.noregister')) {
            // As of 21st/1/2012 the object registry is now used
            RGraph.ObjectRegistry.Add(obj);
            obj.Set('chart.noregister', true);
        }
    }


    /**
    * Causes all registered objects to be redrawn
    * 
    * @param string An optional color to use to clear the canvas
    */
    RGraph.Redraw = function ()
    {
        var objectRegistry = RGraph.ObjectRegistry.objects.byCanvasID;

        // Get all of the canvas tags on the page
        var tags = document.getElementsByTagName('canvas');
        for (var i=0; i<tags.length; ++i) {
            if (tags[i].__object__ && tags[i].__object__.isRGraph) {
                
                // Only clear the canvas if it's not Trace'ing - this applies to the Line/Scatter Trace effects
                if (!tags[i].noclear) {
                    RGraph.Clear(tags[i], arguments[0] ? arguments[0] : null);
                }
            }
        }

        // Go through the object registry and redraw *all* of the canvas'es that have been registered
        for (var i=0; i<objectRegistry.length; ++i) {
            if (objectRegistry[i]) {
                var id = objectRegistry[i][0];
                objectRegistry[i][1].Draw();
            }
        }
    }



    /**
    * Causes all registered objects ON THE GIVEN CANVAS to be redrawn
    * 
    * @param canvas object The canvas object to redraw
    * @param        bool   Optional boolean which defaults to true and determines whether to clear the canvas
    */
    RGraph.RedrawCanvas = function (canvas)
    {
        var objects = RGraph.ObjectRegistry.getObjectsByCanvasID(canvas.id);

        /**
        * First clear the canvas
        */
        if (!arguments[1] || (typeof(arguments[1]) == 'boolean' && !arguments[1] == false) ) {
            RGraph.Clear(canvas);
        }

        /**
        * Now redraw all the charts associated with that canvas
        */
        for (var i=0; i<objects.length; ++i) {
            if (objects[i]) {
                if (objects[i] && objects[i].isRGraph) { // Is it an RGraph object ??
                    objects[i].Draw();
                }
            }
        }
    }


    /**
    * This function draws the background for the bar chart, line chart and scatter chart.
    * 
    * @param  object obj The graph object
    */
    RGraph.background.Draw = function (obj)
    {
        var ca = canvas  = obj.canvas;
        var co = context = obj.context;
        var prop         = obj.properties;

        var height       = 0;
        var gutterLeft   = obj.gutterLeft;
        var gutterRight  = obj.gutterRight;
        var gutterTop    = obj.gutterTop;
        var gutterBottom = obj.gutterBottom;
        var variant      = prop['chart.variant'];
        
        co.fillStyle = prop['chart.text.color'];
        
        // If it's a bar and 3D variant, translate
        if (variant == '3d') {
            co.save();
            co.translate(10, -5);
        }

        // X axis title
        if (typeof(prop['chart.title.xaxis']) == 'string' && prop['chart.title.xaxis'].length) {
        
            var size = prop['chart.text.size'] + 2;
            var font = prop['chart.text.font'];
            var bold = prop['chart.title.xaxis.bold'];

            if (typeof(prop['chart.title.xaxis.size']) == 'number') {
                size = prop['chart.title.xaxis.size'];
            }

            if (typeof(prop['chart.title.xaxis.font']) == 'string') {
                font = prop['chart.title.xaxis.font'];
            }
            
            var hpos = ((ca.width - gutterLeft - gutterRight) / 2) + gutterLeft;
            var vpos = ca.height - gutterBottom + 25;
            
            if (typeof(prop['chart.title.xaxis.pos']) == 'number') {
                vpos = ca.height - (gutterBottom * prop['chart.title.xaxis.pos']);
            }

            RGraph.Text2(obj, {'font':font,
                               'size':size,
                               'x':hpos,
                               'y':vpos,
                               'text':prop['chart.title.xaxis'],
                               'halign':'center',
                               'valign':'center',
                               'bold':bold,
                               'tag': 'title xaxis'
                              });
            
        }

        // Y axis title
        if (typeof(prop['chart.title.yaxis']) == 'string' && prop['chart.title.yaxis'].length) {

            var size  = prop['chart.text.size'] + 2;
            var font  = prop['chart.text.font'];
            var angle = 270;
            var bold  = prop['chart.title.yaxis.bold'];
            var color = prop['chart.title.yaxis.color'];

            if (typeof(prop['chart.title.yaxis.pos']) == 'number') {
                var yaxis_title_pos = prop['chart.title.yaxis.pos'] * gutterLeft;
            } else {
                var yaxis_title_pos = ((gutterLeft - 25) / gutterLeft) * gutterLeft;
            }

            if (typeof(prop['chart.title.yaxis.size']) == 'number') {
                size = prop['chart.title.yaxis.size'];
            }

            if (typeof(prop['chart.title.yaxis.font']) == 'string') {
                font = prop['chart.title.yaxis.font'];
            }

            if (prop['chart.title.yaxis.align'] == 'right' || prop['chart.title.yaxis.position'] == 'right') {
                angle = 90;
                yaxis_title_pos = prop['chart.title.yaxis.pos'] ? (ca.width - gutterRight) + (prop['chart.title.yaxis.pos'] * gutterRight) :
                                                                   ca.width - gutterRight + prop['chart.text.size'] + 5;
            } else {
                yaxis_title_pos = yaxis_title_pos;
            }

            context.fillStyle = color;
            RGraph.Text2(obj, {'font':font,
                               'size':size,
                               'x':yaxis_title_pos,
                               'y':((ca.height - gutterTop - gutterBottom) / 2) + gutterTop,
                               'valign':'center',
                               'halign':'center',
                               'angle':angle,
                               'bold':bold,
                               'text':prop['chart.title.yaxis'],
                               'tag':'title yaxis'
                              });
        }

        /**
        * If the background color is spec ified - draw that. It's a rectangle that fills the
        * entire are within the gutters
        */
        var bgcolor = prop['chart.background.color'];
        if (bgcolor) {
            co.fillStyle = bgcolor;
            co.fillRect(gutterLeft, gutterTop, ca.width - gutterLeft - gutterRight, ca.height - gutterTop - gutterBottom);
        }

        /**
        * Draw horizontal background bars
        */
        co.beginPath(); // Necessary?

        co.fillStyle   = prop['chart.background.barcolor1'];
        co.strokeStyle = co.fillStyle;
        height = (ca.height - gutterBottom);

        for (var i=gutterTop; i < height ; i+=80) {
            co.fillRect(gutterLeft, i, ca.width - gutterLeft - gutterRight, Math.min(40, ca.height - gutterBottom - i) );
        }

        co.fillStyle   = prop['chart.background.barcolor2'];
        co.strokeStyle = co.fillStyle;
        height = (ca.height - gutterBottom);

        for (var i= (40 + gutterTop); i < height; i+=80) {
            co.fillRect(gutterLeft, i, ca.width - gutterLeft - gutterRight, i + 40 > (ca.height - gutterBottom) ? ca.height - (gutterBottom + i) : 40);
        }
        
        //context.stroke();
        co.beginPath();
    

        // Draw the background grid
        if (prop['chart.background.grid']) {

            // If autofit is specified, use the .numhlines and .numvlines along with the width to work
            // out the hsize and vsize
            if (prop['chart.background.grid.autofit']) {

                /**
                * Align the grid to the tickmarks
                */
                if (prop['chart.background.grid.autofit.align']) {
                    
                    // Align the horizontal lines
                    obj.Set('chart.background.grid.autofit.numhlines', prop['chart.ylabels.count']);

                    // Align the vertical lines for the line
                    if (obj.type == 'line') {
                        if (prop['chart.labels'] && prop['chart.labels'].length) {
                            obj.Set('chart.background.grid.autofit.numvlines', prop['chart.labels'].length - 1);
                        } else {
                            obj.Set('chart.background.grid.autofit.numvlines', obj.data[0].length - 1);
                        }

                    // Align the vertical lines for the bar
                    } else if (obj.type == 'bar' && prop['chart.labels'] && prop['chart.labels'].length) {
                        obj.Set('chart.background.grid.autofit.numvlines', prop['chart.labels'].length);
                    }
                }

                var vsize = ((ca.width - gutterLeft - gutterRight)) / prop['chart.background.grid.autofit.numvlines'];
                var hsize = (ca.height - gutterTop - gutterBottom) / prop['chart.background.grid.autofit.numhlines'];

                obj.Set('chart.background.grid.vsize', vsize);
                obj.Set('chart.background.grid.hsize', hsize);
            }

            co.beginPath();
            co.lineWidth   = prop['chart.background.grid.width'] ? prop['chart.background.grid.width'] : 1;
            co.strokeStyle = prop['chart.background.grid.color'];

            // Draw the horizontal lines
            if (prop['chart.background.grid.hlines']) {
                height = (ca.height - gutterBottom)
                for (y=gutterTop; y<height; y+=prop['chart.background.grid.hsize']) {
                    context.moveTo(gutterLeft, Math.round(y));
                    context.lineTo(ca.width - gutterRight, Math.round(y));
                }
            }

            if (prop['chart.background.grid.vlines']) {
                // Draw the vertical lines
                var width = (ca.width - gutterRight)
                for (x=gutterLeft; x<=width; x+=prop['chart.background.grid.vsize']) {
                    co.moveTo(Math.round(x), gutterTop);
                    co.lineTo(Math.round(x), ca.height - gutterBottom);
                }
            }

            if (prop['chart.background.grid.border']) {
                // Make sure a rectangle, the same colour as the grid goes around the graph
                co.strokeStyle = prop['chart.background.grid.color'];
                co.strokeRect(Math.round(gutterLeft), Math.round(gutterTop), ca.width - gutterLeft - gutterRight, ca.height - gutterTop - gutterBottom);
            }
        }

        context.stroke();

        // If it's a bar and 3D variant, translate
        if (variant == '3d') {
            co.restore();
        }

        // Draw the title if one is set
        if ( typeof(prop['chart.title']) == 'string') {

            if (obj.type == 'gantt') {
                gutterTop -= 10;
            }

            RGraph.DrawTitle(obj,
                             prop['chart.title'],
                             gutterTop,
                             null,
                             prop['chart.title.size'] ? prop['chart.title.size'] : prop['chart.text.size'] + 2);
        }

        co.stroke();
    }


    /**
    * Makes a clone of an object
    * 
    * @param obj val The object to clone
    */
    RGraph.array_clone = function (obj)
    {
        if(obj == null || typeof(obj) != 'object') {
            return obj;
        }

        var temp = [];

        for (var i=0;i<obj.length; ++i) {

            if (typeof(obj[i]) == 'number') {
                temp[i] = (function (arg) {return Number(arg);})(obj[i]);
            } else if (typeof(obj[i]) == 'string') {
                temp[i] = (function (arg) {return String(arg);})(obj[i]);
            } else if (typeof(obj[i]) == 'function') {
                temp[i] = obj[i];
            
            } else {
                temp[i] = RGraph.array_clone(obj[i]);
            }
        }

        return temp;
    }


    /**
    * Formats a number with thousand seperators so it's easier to read
    * 
    * @param  integer obj The chart object
    * @param  integer num The number to format
    * @param  string      The (optional) string to prepend to the string
    * @param  string      The (optional) string to append to the string
    * @return string      The formatted number
    */
    RGraph.number_format = function (obj, num)
    {
        var ca   = obj.canvas;
        var co   = obj.context;
        var prop = obj.properties;

        var i;
        var prepend = arguments[2] ? String(arguments[2]) : '';
        var append  = arguments[3] ? String(arguments[3]) : '';
        var output  = '';
        var decimal = '';
        var decimal_seperator  = typeof(prop['chart.scale.point']) == 'string' ? prop['chart.scale.point'] : '.';
        var thousand_seperator = typeof(prop['chart.scale.thousand']) == 'string' ? prop['chart.scale.thousand'] : ',';
        RegExp.$1   = '';
        var i,j;

        if (typeof(prop['chart.scale.formatter']) == 'function') {
            return prop['chart.scale.formatter'](obj, num);
        }

        // Ignore the preformatted version of "1e-2"
        if (String(num).indexOf('e') > 0) {
            return String(prepend + String(num) + append);
        }

        // We need then number as a string
        num = String(num);
        
        // Take off the decimal part - we re-append it later
        if (num.indexOf('.') > 0) {
            var tmp = num;
            num     = num.replace(/\.(.*)/, ''); // The front part of the number
            decimal = tmp.replace(/(.*)\.(.*)/, '$2'); // The decimal part of the number
        }

        // Thousand seperator
        //var seperator = arguments[1] ? String(arguments[1]) : ',';
        var seperator = thousand_seperator;
        
        /**
        * Work backwards adding the thousand seperators
        */
        var foundPoint;
        for (i=(num.length - 1),j=0; i>=0; j++,i--) {
            var character = num.charAt(i);
            
            if ( j % 3 == 0 && j != 0) {
                output += seperator;
            }
            
            /**
            * Build the output
            */
            output += character;
        }
        
        /**
        * Now need to reverse the string
        */
        var rev = output;
        output = '';
        for (i=(rev.length - 1); i>=0; i--) {
            output += rev.charAt(i);
        }

        // Tidy up
        //output = output.replace(/^-,/, '-');
        if (output.indexOf('-' + prop['chart.scale.thousand']) == 0) {
            output = '-' + output.substr(('-' + prop['chart.scale.thousand']).length);
        }

        // Reappend the decimal
        if (decimal.length) {
            output =  output + decimal_seperator + decimal;
            decimal = '';
            RegExp.$1 = '';
        }

        // Minor bugette
        if (output.charAt(0) == '-') {
            output = output.replace(/-/, '');
            prepend = '-' + prepend;
        }

        return prepend + output + append;
    }



    /**
    * Draws horizontal coloured bars on something like the bar, line or scatter
    */
    RGraph.DrawBars = function (obj)
    {
        var hbars = obj.Get('chart.background.hbars');

        /**
        * Draws a horizontal bar
        */
        obj.context.beginPath();

        for (i=0; i<hbars.length; ++i) {
        
            var start  = hbars[i][0];
            var length = hbars[i][1];
            var color  = hbars[i][2];
            

            // Perform some bounds checking
            if(RGraph.is_null(start))start = obj.scale2.max
            if (start > obj.scale2.max) start = obj.scale2.max;
            if (RGraph.is_null(length)) length = obj.scale2.max - start;
            if (start + length > obj.scale2.max) length = obj.scale2.max - start;
            if (start + length < (-1 * obj.scale2.max) ) length = (-1 * obj.scale2.max) - start;

            if (obj.properties['chart.xaxispos'] == 'center' && start == obj.scale2.max && length < (obj.scale2.max * -2)) {
                length = obj.scale2.max * -2;
            }


            /**
            * Draw the bar
            */
            var x = obj.Get('chart.gutter.left');
            var y = obj.getYCoord(start);
            var w = obj.canvas.width - obj.Get('chart.gutter.left') - obj.Get('chart.gutter.right');
            var h = obj.getYCoord(start + length) - y;

            // Accommodate Opera :-/
            if (ISOPERA != -1 && obj.Get('chart.xaxispos') == 'center' && h < 0) {
                h *= -1;
                y = y - h;
            }

            /**
            * Account for X axis at the top
            */
            if (obj.Get('chart.xaxispos') == 'top') {
                y  = obj.canvas.height - y;
                h *= -1;
            }

            obj.context.fillStyle = color;
            obj.context.fillRect(x, y, w, h);
        }
/*


            


            // If the X axis is at the bottom, and a negative max is given, warn the user
            if (obj.Get('chart.xaxispos') == 'bottom' && (hbars[i][0] < 0 || (hbars[i][1] + hbars[i][1] < 0)) ) {
                alert('[' + obj.type.toUpperCase() + ' (ID: ' + obj.id + ') BACKGROUND HBARS] You have a negative value in one of your background hbars values, whilst the X axis is in the center');
            }

            var ystart = (obj.grapharea - (((hbars[i][0] - obj.scale2.min) / (obj.scale2.max - obj.scale2.min)) * obj.grapharea));
            //var height = (Math.min(hbars[i][1], obj.max - hbars[i][0]) / (obj.scale2.max - obj.scale2.min)) * obj.grapharea;
            var height = obj.getYCoord(hbars[i][0]) - obj.getYCoord(hbars[i][1]);

            // Account for the X axis being in the center
            if (obj.Get('chart.xaxispos') == 'center') {
                ystart /= 2;
                //height /= 2;
            }
            
            ystart += obj.Get('chart.gutter.top')

            var x = obj.Get('chart.gutter.left');
            var y = ystart - height;
            var w = obj.canvas.width - obj.Get('chart.gutter.left') - obj.Get('chart.gutter.right');
            var h = height;

            // Accommodate Opera :-/
            if (navigator.userAgent.indexOf('Opera') != -1 && obj.Get('chart.xaxispos') == 'center' && h < 0) {
                h *= -1;
                y = y - h;
            }
            
            /**
            * Account for X axis at the top
            */
            //if (obj.Get('chart.xaxispos') == 'top') {
            //    y  = obj.canvas.height - y;
            //    h *= -1;
            //}

            //obj.context.fillStyle = hbars[i][2];
            //obj.context.fillRect(x, y, w, h);
        //}
    }


    /**
    * Draws in-graph labels.
    * 
    * @param object obj The graph object
    */
    RGraph.DrawInGraphLabels = function (obj)
    {
        var canvas  = obj.canvas;
        var context = obj.context;
        var labels  = obj.Get('chart.labels.ingraph');
        var labels_processed = [];

        // Defaults
        var fgcolor   = 'black';
        var bgcolor   = 'white';
        var direction = 1;

        if (!labels) {
            return;
        }

        /**
        * Preprocess the labels array. Numbers are expanded
        */
        for (var i=0; i<labels.length; ++i) {
            if (typeof(labels[i]) == 'number') {
                for (var j=0; j<labels[i]; ++j) {
                    labels_processed.push(null);
                }
            } else if (typeof(labels[i]) == 'string' || typeof(labels[i]) == 'object') {
                labels_processed.push(labels[i]);
            
            } else {
                labels_processed.push('');
            }
        }

        /**
        * Turn off any shadow
        */
        RGraph.NoShadow(obj);

        if (labels_processed && labels_processed.length > 0) {

            for (var i=0; i<labels_processed.length; ++i) {
                if (labels_processed[i]) {
                    var coords = obj.coords[i];
                    
                    if (coords && coords.length > 0) {
                        var x      = (obj.type == 'bar' ? coords[0] + (coords[2] / 2) : coords[0]);
                        var y      = (obj.type == 'bar' ? coords[1] + (coords[3] / 2) : coords[1]);
                        var length = typeof(labels_processed[i][4]) == 'number' ? labels_processed[i][4] : 25;
    
                        context.beginPath();
                        context.fillStyle   = 'black';
                        context.strokeStyle = 'black';
                        
    
                        if (obj.type == 'bar') {
                        
                            /**
                            * X axis at the top
                            */
                            if (obj.Get('chart.xaxispos') == 'top') {
                                length *= -1;
                            }
    
                            if (obj.Get('chart.variant') == 'dot') {
                                context.moveTo(Math.round(x), obj.coords[i][1] - 5);
                                context.lineTo(Math.round(x), obj.coords[i][1] - 5 - length);
                                
                                var text_x = Math.round(x);
                                var text_y = obj.coords[i][1] - 5 - length;
                            
                            } else if (obj.Get('chart.variant') == 'arrow') {
                                context.moveTo(Math.round(x), obj.coords[i][1] - 5);
                                context.lineTo(Math.round(x), obj.coords[i][1] - 5 - length);
                                
                                var text_x = Math.round(x);
                                var text_y = obj.coords[i][1] - 5 - length;
                            
                            } else {
    
                                context.arc(Math.round(x), y, 2.5, 0, 6.28, 0);
                                context.moveTo(Math.round(x), y);
                                context.lineTo(Math.round(x), y - length);

                                var text_x = Math.round(x);
                                var text_y = y - length;
                            }

                            context.stroke();
                            context.fill();
                            
    
                        } else if (obj.type == 'line') {
                        
                            if (
                                typeof(labels_processed[i]) == 'object' &&
                                typeof(labels_processed[i][3]) == 'number' &&
                                labels_processed[i][3] == -1
                               ) {

                                context.moveTo(Math.round(x), y + 5);
                                context.lineTo(Math.round(x), y + 5 + length);
                                
                                context.stroke();
                                context.beginPath();                                
                                
                                // This draws the arrow
                                context.moveTo(Math.round(x), y + 5);
                                context.lineTo(Math.round(x) - 3, y + 10);
                                context.lineTo(Math.round(x) + 3, y + 10);
                                context.closePath();
                                
                                var text_x = x;
                                var text_y = y + 5 + length;
                            
                            } else {
                                
                                var text_x = x;
                                var text_y = y - 5 - length;

                                context.moveTo(Math.round(x), y - 5);
                                context.lineTo(Math.round(x), y - 5 - length);
                                
                                context.stroke();
                                context.beginPath();
                                
                                // This draws the arrow
                                context.moveTo(Math.round(x), y - 5);
                                context.lineTo(Math.round(x) - 3, y - 10);
                                context.lineTo(Math.round(x) + 3, y - 10);
                                context.closePath();
                            }
                        
                            context.fill();
                        }

                        // Taken out on the 10th Nov 2010 - unnecessary
                        //var width = context.measureText(labels[i]).width;
                        
                        context.beginPath();
                            
                            // Fore ground color
                            context.fillStyle = (typeof(labels_processed[i]) == 'object' && typeof(labels_processed[i][1]) == 'string') ? labels_processed[i][1] : 'black';

                            RGraph.Text2(obj,{'font':obj.Get('chart.text.font'),
                                              'size':obj.Get('chart.text.size'),
                                              'x':text_x,
                                              'y':text_y,
                                              'text': (typeof(labels_processed[i]) == 'object' && typeof(labels_processed[i][0]) == 'string') ? labels_processed[i][0] : labels_processed[i],
                                              'valign': 'bottom',
                                              'halign':'center',
                                              'bounding':true,
                                              'bounding.fill': (typeof(labels_processed[i]) == 'object' && typeof(labels_processed[i][2]) == 'string') ? labels_processed[i][2] : 'white',
                                              'tag':'labels ingraph'
                                             });
                        context.fill();
                    }
                }
            }
        }
    }


    /**
    * This function "fills in" key missing properties that various implementations lack
    * 
    * @param object e The event object
    */
    RGraph.FixEventObject = function (e)
    {
        if (ISOLD) {
            var e = event;

            e.pageX  = (event.clientX + document.body.scrollLeft);
            e.pageY  = (event.clientY + document.body.scrollTop);
            e.target = event.srcElement;
            
            if (!document.body.scrollTop && document.documentElement.scrollTop) {
                e.pageX += parseInt(document.documentElement.scrollLeft);
                e.pageY += parseInt(document.documentElement.scrollTop);
            }
        }

        
        // Any browser that doesn't implement stopPropagation() (MSIE)
        if (!e.stopPropagation) {
            e.stopPropagation = function () {window.event.cancelBubble = true;}
        }
        
        return e;
    }

    /**
    * Thisz function hides the crosshairs coordinates
    */
    RGraph.HideCrosshairCoords = function ()
    {
        var div = RGraph.Registry.Get('chart.coordinates.coords.div');

        if (   div
            && div.style.opacity == 1
            && div.__object__.Get('chart.crosshairs.coords.fadeout')
           ) {
            setTimeout(function() {RGraph.Registry.Get('chart.coordinates.coords.div').style.opacity = 0.9;}, 50);
            setTimeout(function() {RGraph.Registry.Get('chart.coordinates.coords.div').style.opacity = 0.8;}, 100);
            setTimeout(function() {RGraph.Registry.Get('chart.coordinates.coords.div').style.opacity = 0.7;}, 150);
            setTimeout(function() {RGraph.Registry.Get('chart.coordinates.coords.div').style.opacity = 0.6;}, 200);
            setTimeout(function() {RGraph.Registry.Get('chart.coordinates.coords.div').style.opacity = 0.5;}, 250);
            setTimeout(function() {RGraph.Registry.Get('chart.coordinates.coords.div').style.opacity = 0.4;}, 300);
            setTimeout(function() {RGraph.Registry.Get('chart.coordinates.coords.div').style.opacity = 0.3;}, 350);
            setTimeout(function() {RGraph.Registry.Get('chart.coordinates.coords.div').style.opacity = 0.2;}, 400);
            setTimeout(function() {RGraph.Registry.Get('chart.coordinates.coords.div').style.opacity = 0.1;}, 450);
            setTimeout(function() {RGraph.Registry.Get('chart.coordinates.coords.div').style.opacity = 0;}, 500);
            setTimeout(function() {RGraph.Registry.Get('chart.coordinates.coords.div').style.display = 'none';}, 550);
        }
    }


    /**
    * Draws the3D axes/background
    */
    RGraph.Draw3DAxes = function (obj)
    {
        var gutterLeft    = obj.Get('chart.gutter.left');
        var gutterRight   = obj.Get('chart.gutter.right');
        var gutterTop     = obj.Get('chart.gutter.top');
        var gutterBottom  = obj.Get('chart.gutter.bottom');

        var context = obj.context;
        var canvas  = obj.canvas;

        context.strokeStyle = '#aaa';
        context.fillStyle = '#ddd';

        // Draw the vertical left side
        context.beginPath();
            context.moveTo(gutterLeft, gutterTop);
            context.lineTo(gutterLeft + 10, gutterTop - 5);
            context.lineTo(gutterLeft + 10, canvas.height - gutterBottom - 5);
            context.lineTo(gutterLeft, canvas.height - gutterBottom);
        context.closePath();
        
        context.stroke();
        context.fill();

        // Draw the bottom floor
        context.beginPath();
            context.moveTo(gutterLeft, canvas.height - gutterBottom);
            context.lineTo(gutterLeft + 10, canvas.height - gutterBottom - 5);
            context.lineTo(canvas.width - gutterRight + 10,  canvas.height - gutterBottom - 5);
            context.lineTo(canvas.width - gutterRight, canvas.height - gutterBottom);
        context.closePath();
        
        context.stroke();
        context.fill();
    }



    /**
    * This function attempts to "fill in" missing functions from the canvas
    * context object. Only two at the moment - measureText() nd fillText().
    * 
    * @param object context The canvas 2D context
    */
    RGraph.OldBrowserCompat = function (context)
    {
        if (!context) {
            return;
        }

        if (!context.measureText) {
        
            // This emulates the measureText() function
            context.measureText = function (text)
            {
                var textObj = document.createElement('DIV');
                textObj.innerHTML = text;
                textObj.style.position = 'absolute';
                textObj.style.top = '-100px';
                textObj.style.left = 0;
                document.body.appendChild(textObj);

                var width = {width: textObj.offsetWidth};
                
                textObj.style.display = 'none';
                
                return width;
            }
        }

        if (!context.fillText) {
            // This emulates the fillText() method
            context.fillText    = function (text, targetX, targetY)
            {
                return false;
            }
        }
        
        // If IE8, add addEventListener()
        if (!context.canvas.addEventListener) {
            window.addEventListener = function (ev, func, bubble)
            {
                return this.attachEvent('on' + ev, func);
            }

            context.canvas.addEventListener = function (ev, func, bubble)
            {
                return this.attachEvent('on' + ev, func);
            }
        }
    }


    /**
    * Draws a rectangle with curvy corners
    * 
    * @param context object The context
    * @param x       number The X coordinate (top left of the square)
    * @param y       number The Y coordinate (top left of the square)
    * @param w       number The width of the rectangle
    * @param h       number The height of the rectangle
    * @param         number The radius of the curved corners
    * @param         boolean Whether the top left corner is curvy
    * @param         boolean Whether the top right corner is curvy
    * @param         boolean Whether the bottom right corner is curvy
    * @param         boolean Whether the bottom left corner is curvy
    */
    RGraph.strokedCurvyRect = function (context, x, y, w, h)
    {
        // The corner radius
        var r = arguments[5] ? arguments[5] : 3;

        // The corners
        var corner_tl = (arguments[6] || arguments[6] == null) ? true : false;
        var corner_tr = (arguments[7] || arguments[7] == null) ? true : false;
        var corner_br = (arguments[8] || arguments[8] == null) ? true : false;
        var corner_bl = (arguments[9] || arguments[9] == null) ? true : false;

        context.beginPath();

            // Top left side
            context.moveTo(x + (corner_tl ? r : 0), y);
            context.lineTo(x + w - (corner_tr ? r : 0), y);
            
            // Top right corner
            if (corner_tr) {
                context.arc(x + w - r, y + r, r, PI + HALFPI, TWOPI, false);
            }

            // Top right side
            context.lineTo(x + w, y + h - (corner_br ? r : 0) );

            // Bottom right corner
            if (corner_br) {
                context.arc(x + w - r, y - r + h, r, TWOPI, HALFPI, false);
            }

            // Bottom right side
            context.lineTo(x + (corner_bl ? r : 0), y + h);

            // Bottom left corner
            if (corner_bl) {
                context.arc(x + r, y - r + h, r, HALFPI, PI, false);
            }

            // Bottom left side
            context.lineTo(x, y + (corner_tl ? r : 0) );

            // Top left corner
            if (corner_tl) {
                context.arc(x + r, y + r, r, PI, PI + HALFPI, false);
            }

        context.stroke();
    }


    /**
    * Draws a filled rectangle with curvy corners
    * 
    * @param context object The context
    * @param x       number The X coordinate (top left of the square)
    * @param y       number The Y coordinate (top left of the square)
    * @param w       number The width of the rectangle
    * @param h       number The height of the rectangle
    * @param         number The radius of the curved corners
    * @param         boolean Whether the top left corner is curvy
    * @param         boolean Whether the top right corner is curvy
    * @param         boolean Whether the bottom right corner is curvy
    * @param         boolean Whether the bottom left corner is curvy
    */
    RGraph.filledCurvyRect = function (context, x, y, w, h)
    {
        // The corner radius
        var r = arguments[5] ? arguments[5] : 3;

        // The corners
        var corner_tl = (arguments[6] || arguments[6] == null) ? true : false;
        var corner_tr = (arguments[7] || arguments[7] == null) ? true : false;
        var corner_br = (arguments[8] || arguments[8] == null) ? true : false;
        var corner_bl = (arguments[9] || arguments[9] == null) ? true : false;

        context.beginPath();

            // First draw the corners

            // Top left corner
            if (corner_tl) {
                context.moveTo(x + r, y + r);
                context.arc(x + r, y + r, r, PI, PI + HALFPI, false);
            } else {
                context.fillRect(x, y, r, r);
            }

            // Top right corner
            if (corner_tr) {
                context.moveTo(x + w - r, y + r);
                context.arc(x + w - r, y + r, r, PI + HALFPI, 0, false);
            } else {
                context.moveTo(x + w - r, y);
                context.fillRect(x + w - r, y, r, r);
            }


            // Bottom right corner
            if (corner_br) {
                context.moveTo(x + w - r, y + h - r);
                context.arc(x + w - r, y - r + h, r, 0, HALFPI, false);
            } else {
                context.moveTo(x + w - r, y + h - r);
                context.fillRect(x + w - r, y + h - r, r, r);
            }

            // Bottom left corner
            if (corner_bl) {
                context.moveTo(x + r, y + h - r);
                context.arc(x + r, y - r + h, r, HALFPI, PI, false);
            } else {
                context.moveTo(x, y + h - r);
                context.fillRect(x, y + h - r, r, r);
            }

            // Now fill it in
            context.fillRect(x + r, y, w - r - r, h);
            context.fillRect(x, y + r, r + 1, h - r - r);
            context.fillRect(x + w - r - 1, y + r, r + 1, h - r - r);

        context.fill();
    }


    /**
    * Hides the zoomed canvas
    */
    RGraph.HideZoomedCanvas = function ()
    {
        var interval = 15;
        var frames   = 10;

        if (typeof(__zoomedimage__) == 'object') {
            obj = __zoomedimage__.obj;
        } else {
            return;
        }

        if (obj.Get('chart.zoom.fade.out')) {
            for (var i=frames,j=1; i>=0; --i, ++j) {
                if (typeof(__zoomedimage__) == 'object') {
                    setTimeout("__zoomedimage__.style.opacity = " + String(i / 10), j * interval);
                }
            }

            if (typeof(__zoomedbackground__) == 'object') {
                setTimeout("__zoomedbackground__.style.opacity = " + String(i / frames), j * interval);
            }
        }

        if (typeof(__zoomedimage__) == 'object') {
            setTimeout("__zoomedimage__.style.display = 'none'", obj.Get('chart.zoom.fade.out') ? (frames * interval) + 10 : 0);
        }

        if (typeof(__zoomedbackground__) == 'object') {
            setTimeout("__zoomedbackground__.style.display = 'none'", obj.Get('chart.zoom.fade.out') ? (frames * interval) + 10 : 0);
        }
    }


    /**
    * Adds an event handler
    * 
    * @param object obj   The graph object
    * @param string event The name of the event, eg ontooltip
    * @param object func  The callback function
    */
    RGraph.AddCustomEventListener = function (obj, name, func)
    {
        if (typeof(RGraph.events[obj.uid]) == 'undefined') {
            RGraph.events[obj.uid] = [];
        }

        RGraph.events[obj.uid].push([obj, name, func]);
        
        return RGraph.events[obj.uid].length - 1;
    }


    /**
    * Used to fire one of the RGraph custom events
    * 
    * @param object obj   The graph object that fires the event
    * @param string event The name of the event to fire
    */
    RGraph.FireCustomEvent = function (obj, name)
    {
        if (obj && obj.isRGraph) {
        
            // New style of adding custom events
            if (obj[name]) {
                (obj[name])(obj);
            }
            
            var uid = obj.uid;
    
            if (   typeof(uid) == 'string'
                && typeof(RGraph.events) == 'object'
                && typeof(RGraph.events[uid]) == 'object'
                && RGraph.events[uid].length > 0) {
    
                for(var j=0; j<RGraph.events[uid].length; ++j) {
                    if (RGraph.events[uid][j] && RGraph.events[uid][j][1] == name) {
                        RGraph.events[uid][j][2](obj);
                    }
                }
            }
        }
    }



    /**
    * If you prefer, you can use the SetConfig() method to set the configuration information
    * for your chart. You may find that setting the configuration this way eases reuse.
    * 
    * @param object obj    The graph object
    * @param object config The graph configuration information
    */
    RGraph.SetConfig = function (obj, c)
    {
        for (i in c) {
            if (typeof(i) == 'string') {
                obj.Set(i, c[i]);
            }
        }
        
        return obj;
    }


    /**
    * Clears all the custom event listeners that have been registered
    * 
    * @param    string Limits the clearing to this object ID
    */
    RGraph.RemoveAllCustomEventListeners = function ()
    {
        var id = arguments[0];

        if (id && RGraph.events[id]) {
            RGraph.events[id] = [];
        } else {
            RGraph.events = [];
        }
    }


    /**
    * Clears a particular custom event listener
    * 
    * @param object obj The graph object
    * @param number i   This is the index that is return by .AddCustomEventListener()
    */
    RGraph.RemoveCustomEventListener = function (obj, i)
    {
        if (   typeof(RGraph.events) == 'object'
            && typeof(RGraph.events[obj.id]) == 'object'
            && typeof(RGraph.events[obj.id][i]) == 'object') {
            
            RGraph.events[obj.id][i] = null;
        }
    }



    /**
    * This draws the background
    * 
    * @param object obj The graph object
    */
    RGraph.DrawBackgroundImage = function (obj)
    {
        if (typeof(obj.Get('chart.background.image')) == 'string') {
            if (typeof(obj.canvas.__rgraph_background_image__) == 'undefined') {
                var img = new Image();
                img.__object__  = obj;
                img.__canvas__  = obj.canvas;
                img.__context__ = obj.context;
                img.src         = obj.Get('chart.background.image');
                
                obj.canvas.__rgraph_background_image__ = img;
            } else {
                img = obj.canvas.__rgraph_background_image__;
            }
            
            // When the image has loaded - redraw the canvas
            img.onload = function ()
            {
                obj.__rgraph_background_image_loaded__ = true;
                RGraph.Clear(obj.canvas);
                RGraph.RedrawCanvas(obj.canvas);
            }
                
            var gutterLeft   = obj.Get('chart.gutter.left');
            var gutterRight  = obj.Get('chart.gutter.right');
            var gutterTop    = obj.Get('chart.gutter.top');
            var gutterBottom = obj.Get('chart.gutter.bottom');
            var stretch      = obj.Get('chart.background.image.stretch');
            var align        = obj.Get('chart.background.image.align');
    
            // Handle chart.background.image.align
            if (typeof(align) == 'string') {
                if (align.indexOf('right') != -1) {
                    var x = obj.canvas.width - img.width - gutterRight;
                } else {
                    var x = gutterLeft;
                }
    
                if (align.indexOf('bottom') != -1) {
                    var y = obj.canvas.height - img.height - gutterBottom;
                } else {
                    var y = gutterTop;
                }
            } else {
                var x = gutterLeft;
                var y = gutterTop;
            }
            
            // X/Y coords take precedence over the align
            var x = typeof(obj.Get('chart.background.image.x')) == 'number' ? obj.Get('chart.background.image.x') : x;
            var y = typeof(obj.Get('chart.background.image.y')) == 'number' ? obj.Get('chart.background.image.y') : y;
            var w = stretch ? obj.canvas.width - gutterLeft - gutterRight : img.width;
            var h = stretch ? obj.canvas.height - gutterTop - gutterBottom : img.height;
            
            /**
            * You can now specify the width and height of the image
            */
            if (typeof(obj.Get('chart.background.image.w')) == 'number') w  = obj.Get('chart.background.image.w');
            if (typeof(obj.Get('chart.background.image.h')) == 'number') h = obj.Get('chart.background.image.h');
    
            obj.context.drawImage(img,x,y,w, h);
        }
    }



    /**
    * This function determines wshether an object has tooltips or not
    * 
    * @param object obj The chart object
    */
    RGraph.hasTooltips = function (obj)
    {
        if (typeof(obj.Get('chart.tooltips')) == 'object' && obj.Get('chart.tooltips')) {
            for (var i=0; i<obj.Get('chart.tooltips').length; ++i) {
                if (!RGraph.is_null(obj.Get('chart.tooltips')[i])) {
                    return true;
                }
            }
        } else if (typeof(obj.Get('chart.tooltips')) == 'function') {
            return true;
        }
        
        return false;
    }



    /**
    * This function creates a (G)UID which can be used to identify objects.
    * 
    * @return string (g)uid The (G)UID
    */
    RGraph.CreateUID = function ()
    {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c)
        {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }



    /**
    * This is the new object registry, used to facilitate multiple objects per canvas.
    * 
    * @param object obj The object to register
    */
    RGraph.ObjectRegistry.Add = function (obj)
    {
        var uid      = obj.uid;
        var canvasID = obj.canvas.id;

        /**
        * Index the objects by UID
        */
        RGraph.ObjectRegistry.objects.byUID.push([uid, obj]);
        
        /**
        * Index the objects by the canvas that they're drawn on
        */
        RGraph.ObjectRegistry.objects.byCanvasID.push([canvasID, obj]);
    }



    /**
    * Remove an object from the object registry
    * 
    * @param object obj The object to remove.
    */
    RGraph.ObjectRegistry.Remove = function (obj)
    {
        var id  = obj.id;
        var uid = obj.uid;

        for (var i=0; i<RGraph.ObjectRegistry.objects.byUID.length; ++i) {
            if (RGraph.ObjectRegistry.objects.byUID[i] && RGraph.ObjectRegistry.objects.byUID[i][1].uid == uid) {
                RGraph.ObjectRegistry.objects.byUID[i] = null;
            }
        }


        for (var i=0; i<RGraph.ObjectRegistry.objects.byCanvasID.length; ++i) {
            if (   RGraph.ObjectRegistry.objects.byCanvasID[i]
                && RGraph.ObjectRegistry.objects.byCanvasID[i][1]
                && RGraph.ObjectRegistry.objects.byCanvasID[i][1].uid == uid) {
                
                RGraph.ObjectRegistry.objects.byCanvasID[i] = null;
            }
        }

    }



    /**
    * Removes all objects from the ObjectRegistry. If either the ID of a canvas is supplied,
    * or the canvas itself, then only objects pertaining to that canvas are cleared.
    * 
    * @param mixed   Either a canvas object (as returned by document.getElementById()
    *                or the ID of a canvas (ie a string)
    */
    RGraph.ObjectRegistry.Clear = function ()
    {
        // If an ID is supplied restrict the learing to that
        if (arguments[0]) {

            var id      = (typeof(arguments[0]) == 'object' ? arguments[0].id : arguments[0]);
            var objects = RGraph.ObjectRegistry.getObjectsByCanvasID(id);

            for (var i=0; i<objects.length; ++i) {
                RGraph.ObjectRegistry.Remove(objects[i]);
            }

        } else {

            RGraph.ObjectRegistry.objects            = {};
            RGraph.ObjectRegistry.objects.byUID      = [];
            RGraph.ObjectRegistry.objects.byCanvasID = [];
        }
    }



    /**
    * Lists all objects in the ObjectRegistry
    * 
    * @param boolean ret Whether to return the list or alert() it
    */
    RGraph.ObjectRegistry.List = function ()
    {
        var list = [];

        for (var i=0; i<RGraph.ObjectRegistry.objects.byUID.length; ++i) {
            if (RGraph.ObjectRegistry.objects.byUID[i]) {
                list.push(RGraph.ObjectRegistry.objects.byUID[i][1].type);
            }
        }
        
        if (arguments[0]) {
            return list;
        } else {
            p(list);
        }
    }



    /**
    * Clears the ObjectRegistry of objects that are of a certain given type
    * 
    * @param type string The type to clear
    */
    RGraph.ObjectRegistry.ClearByType = function (type)
    {
        var objects = RGraph.ObjectRegistry.objects.byUID;

        for (var i=0; i<objects.length; ++i) {
            if (objects[i]) {
                var uid = objects[i][0];
                var obj = objects[i][1];
                
                if (obj && obj.type == type) {
                    RGraph.ObjectRegistry.Remove(obj);
                }
            }
        }
    }



    /**
    * This function provides an easy way to go through all of the objects that are held in the
    * Registry
    * 
    * @param func function This function is run for every object. Its passed the object as an argument
    * @param string type Optionally, you can pass a type of object to look for
    */
    RGraph.ObjectRegistry.Iterate = function (func)
    {
        var objects = RGraph.ObjectRegistry.objects.byUID;

        for (var i=0; i<objects.length; ++i) {
        
            if (typeof arguments[1] == 'string') {
                
                var types = arguments[1].split(/,/);

                for (var j=0; j<types.length; ++j) {
                    if (types[j] == objects[i][1].type) {
                        func(objects[i][1]);
                    }
                }
            } else {
                func(objects[i][1]);
            }
        }
    }



    /**
    * Retrieves all objects for a given canvas id
    * 
    * @patarm id string The canvas ID to get objects for.
    */
    RGraph.ObjectRegistry.getObjectsByCanvasID = function (id)
    {
        var store = RGraph.ObjectRegistry.objects.byCanvasID;
        var ret = [];

        // Loop through all of the objects and return the appropriate ones
        for (var i=0; i<store.length; ++i) {
            if (store[i] && store[i][0] == id ) {
                ret.push(store[i][1]);
            }
        }

        return ret;
    }



    /**
    * Retrieves the relevant object based on the X/Y position.
    * 
    * @param  object e The event object
    * @return object   The applicable (if any) object
    */
    RGraph.ObjectRegistry.getFirstObjectByXY =
    RGraph.ObjectRegistry.getObjectByXY = function (e)
    {
        var canvas  = e.target;
        var ret     = null;
        var objects = RGraph.ObjectRegistry.getObjectsByCanvasID(canvas.id);

        for (var i=(objects.length - 1); i>=0; --i) {

            var obj = objects[i].getObjectByXY(e);

            if (obj) {
                return obj;
            }
        }
    }



    /**
    * Retrieves the relevant objects based on the X/Y position.
    * NOTE This function returns an array of objects
    * 
    * @param  object e The event object
    * @return          An array of pertinent objects. Note the there may be only one object
    */
    RGraph.ObjectRegistry.getObjectsByXY = function (e)
    {
        var canvas  = e.target;
        var ret     = [];
        var objects = RGraph.ObjectRegistry.getObjectsByCanvasID(canvas.id);

        // Retrieve objects "front to back"
        for (var i=(objects.length - 1); i>=0; --i) {

            var obj = objects[i].getObjectByXY(e);

            if (obj) {
                ret.push(obj);
            }
        }
        
        return ret;
    }


    /**
    * Retrieves the object with the corresponding UID
    * 
    * @param string uid The UID to get the relevant object for
    */
    RGraph.ObjectRegistry.getObjectByUID = function (uid)
    {
        var objects = RGraph.ObjectRegistry.objects.byUID;

        for (var i=0; i<objects.length; ++i) {
            if (objects[i] && objects[i][1].uid == uid) {
                return objects[i][1];
            }
        }
    }


    /**
    * Retrieves the objects that are the given type
    * 
    * @param  mixed canvas  The canvas to check. It can either be the canvas object itself or just the ID
    * @param  string type   The type to look for
    * @return array         An array of one or more objects
    */
    RGraph.ObjectRegistry.getObjectsByType = function (type)
    {
        var objects = RGraph.ObjectRegistry.objects.byUID;
        var ret     = [];

        for (var i=0; i<objects.length; ++i) {

            if (objects[i] && objects[i][1] && objects[i][1].type && objects[i][1].type && objects[i][1].type == type) {
                ret.push(objects[i][1]);
            }
        }

        return ret;
    }


    /**
    * Retrieves the FIRST object that matches the given type
    *
    * @param  string type   The type of object to look for
    * @return object        The FIRST object that matches the given type
    */
    RGraph.ObjectRegistry.getFirstObjectByType = function (type)
    {
        var objects = RGraph.ObjectRegistry.objects.byUID;
    
        for (var i=0; i<objects.length; ++i) {
            if (objects[i] && objects[i][1] && objects[i][1].type == type) {
                return objects[i][1];
            }
        }
        
        return null;
    }



    /**
    * This takes centerx, centery, x and y coordinates and returns the
    * appropriate angle relative to the canvas angle system. Remember
    * that the canvas angle system starts at the EAST axis
    * 
    * @param  number cx  The centerx coordinate
    * @param  number cy  The centery coordinate
    * @param  number x   The X coordinate (eg the mouseX if coming from a click)
    * @param  number y   The Y coordinate (eg the mouseY if coming from a click)
    * @return number     The relevant angle (measured in in RADIANS)
    */
    RGraph.getAngleByXY = function (cx, cy, x, y)
    {
        var angle = Math.atan((y - cy) / (x - cx));
            angle = Math.abs(angle)

        if (x >= cx && y >= cy) {
            angle += TWOPI;

        } else if (x >= cx && y < cy) {
            angle = (HALFPI - angle) + (PI + HALFPI);

        } else if (x < cx && y < cy) {
            angle += PI;

        } else {
            angle = PI - angle;
        }

        /**
        * Upper and lower limit checking
        */
        if (angle > TWOPI) {
            angle -= TWOPI;
        }

        return angle;
    }



    /**
    * This function returns the distance between two points. In effect the
    * radius of an imaginary circle that is centered on x1 and y1. The name
    * of this function is derived from the word "Hypoteneuse", which in
    * trigonmetry is the longest side of a triangle
    * 
    * @param number x1 The original X coordinate
    * @param number y1 The original Y coordinate
    * @param number x2 The target X coordinate
    * @param number y2 The target Y  coordinate
    */
    RGraph.getHypLength = function (x1, y1, x2, y2)
    {
        var ret = Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));

        return ret;
    }



    /**
    * This function gets the end point (X/Y coordinates) of a given radius.
    * You pass it the center X/Y and the radius and this function will return
    * the endpoint X/Y coordinates.
    * 
    * @param number cx The center X coord
    * @param number cy The center Y coord
    * @param number r  The lrngth of the radius
    */
    RGraph.getRadiusEndPoint = function (cx, cy, angle, radius)
    {
        var x = cx + (Math.cos(angle) * radius);
        var y = cy + (Math.sin(angle) * radius);
        
        return [x, y];
    }



    /**
    * This installs all of the event listeners
    * 
    * @param object obj The chart object
    */
    RGraph.InstallEventListeners = function (obj)
    {
        /**
        * Don't attempt to install event listeners for older versions of MSIE
        */
        if (RGraph.isOld()) {
            return;
        }

        /**
        * If this function exists, then the dynamic file has been included.
        */
        if (RGraph.InstallCanvasClickListener) {

            RGraph.InstallWindowMousedownListener(obj);
            RGraph.InstallWindowMouseupListener(obj);
            RGraph.InstallCanvasMousemoveListener(obj);
            RGraph.InstallCanvasMouseupListener(obj);
            RGraph.InstallCanvasMousedownListener(obj);
            RGraph.InstallCanvasClickListener(obj);
        
        } else if (   RGraph.hasTooltips(obj)
                   || obj.Get('chart.adjustable')
                   || obj.Get('chart.annotatable')
                   || obj.Get('chart.contextmenu')
                   || obj.Get('chart.resizable')
                   || obj.Get('chart.key.interactive')
                   || obj.Get('chart.events.click')
                   || obj.Get('chart.events.mousemove')
                   || typeof obj.onclick == 'function'
                   || typeof obj.onmousemove == 'function'
                  ) {

            alert('[RGRAPH] You appear to have used dynamic features but not included the file: RGraph.common.dynamic.js');
        }
    }



    /**
    * Loosly mimicks the PHP function print_r();
    */
    RGraph.pr = function (obj)
    {

        var indent = (arguments[2] ? arguments[2] : '    ');
        var str    = '';

        var counter = typeof arguments[3] == 'number' ? arguments[3] : 0;
        
        if (counter >= 5) {
            return '';
        }
        
        switch (typeof obj) {
            
            case 'string':    str += obj + ' (' + (typeof obj) + ', ' + obj.length + ')'; break;
            case 'number':    str += obj + ' (' + (typeof obj) + ')'; break;
            case 'boolean':   str += obj + ' (' + (typeof obj) + ')'; break;
            case 'function':  str += 'function () {}'; break;
            case 'undefined': str += 'undefined'; break;
            case 'null':      str += 'null'; break;
            
            case 'object':
                // In case of null
                if (RGraph.is_null(obj)) {
                    str += indent + 'null\n';
                } else {
                    str += indent + 'Object {' + '\n'
                    for (j in obj) {
                        str += indent + '    ' + j + ' => ' + RGraph.pr(obj[j], true, indent + '    ', counter + 1) + '\n';
                    }
                    str += indent + '}';
                }
                break;
            
            
            default:
                str += 'Unknown type: ' + typeof obj + '';
                break;
        }


        /**
        * Finished, now either return if we're in a recursed call, or alert()
        * if we're not.
        */
        if (!arguments[1]) {
            alert(str);
        }
        
        return str;
    }



    /**
    * Produces a dashed line
    * 
    * @param
    */
    RGraph.DashedLine = function(context, x1, y1, x2, y2)
    {
        /**
        * This is the size of the dashes
        */
        var size = 5;

        /**
        * The optional fifth argument can be the size of the dashes
        */
        if (typeof(arguments[5]) == 'number') {
            size = arguments[5];
        }

        var dx  = x2 - x1;
        var dy  = y2 - y1;
        var num = Math.floor(Math.sqrt((dx * dx) + (dy * dy)) / size);

        var xLen = dx / num;
        var yLen = dy / num;

        var count = 0;

        do {
            (count % 2 == 0 && count > 0) ? context.lineTo(x1, y1) : context.moveTo(x1, y1);

            x1 += xLen;
            y1 += yLen;
        } while(count++ <= num);
    }


    /**
    * Makes an AJAX call. It calls the given callback (a function) when ready
    * 
    * @param string   url      The URL to retrieve
    * @param function callback A function that is called when the response is ready, there's an example below
    *                          called "myCallback".
    */
    RGraph.AJAX = function (url, callback)
    {
        // Mozilla, Safari, ...
        if (window.XMLHttpRequest) {
            var httpRequest = new XMLHttpRequest();

        // MSIE
        } else if (window.ActiveXObject) {
            var httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }

        httpRequest.onreadystatechange = function ()
            {
                if (this.readyState == 4 && this.status == 200) {
                    this.__user_callback__ = callback;
                    this.__user_callback__();
                }
            }

        httpRequest.open('GET', url, true);
        httpRequest.send();
    }
    
    /**
    * Rotates the canvas
    * 
    * @param object canvas The canvas to rotate
    * @param  int   x      The X coordinate about which to rotate the canvas
    * @param  int   y      The Y coordinate about which to rotate the canvas
    * @param  int   angle  The angle(in RADIANS) to rotate the canvas by
    */
    RGraph.RotateCanvas = function (canvas, x, y, angle)
    {
        var context = canvas.getContext('2d');

        context.translate(x, y);
        context.rotate(angle);
        context.translate(0 - x, 0 - y);    
    }



    /**
    * Measures text by creating a DIV in the document and adding the relevant text to it.
    * Then checking the .offsetWidth and .offsetHeight.
    * 
    * @param  string text   The text to measure
    * @param  bool   bold   Whether the text is bold or not
    * @param  string font   The font to use
    * @param  size   number The size of the text (in pts)
    * @return array         A two element array of the width and height of the text
    */
    RGraph.MeasureText = function (text, bold, font, size)
    {
        // Add the sizes to the cache as adding DOM elements is costly and causes slow downs
        if (typeof(__rgraph_measuretext_cache__) == 'undefined') {
            __rgraph_measuretext_cache__ = [];
        }

        var str = text + ':' + bold + ':' + font + ':' + size;
        if (typeof(__rgraph_measuretext_cache__) == 'object' && __rgraph_measuretext_cache__[str]) {
            return __rgraph_measuretext_cache__[str];
        }
        
        if (!__rgraph_measuretext_cache__['text-div']) {
            var div = document.createElement('DIV');
                div.style.position = 'absolute';
                div.style.top = '-100px';
                div.style.left = '-100px';
            document.body.appendChild(div);
            
            // Now store the newly created DIV
            __rgraph_measuretext_cache__['text-div'] = div;

        } else if (__rgraph_measuretext_cache__['text-div']) {
            var div = __rgraph_measuretext_cache__['text-div'];
        }

        div.innerHTML = text.replace(/\r\n/g, '<br />');
        div.style.fontFamily = font;
        div.style.fontWeight = bold ? 'bold' : 'normal';
        div.style.fontSize = size + 'pt';
        
        var size = [div.offsetWidth, div.offsetHeight];

        //document.body.removeChild(div);
        __rgraph_measuretext_cache__[str] = size;
        
        return size;
    }



    /* New text function. Accepts two arguments:
    *  o obj - The chart object
    *  o opt - An object/hash/map of properties. This can consist of:
    *          x                The X coordinate (REQUIRED)
    *          y                The Y coordinate (REQUIRED)
    *          text             The text to show (REQUIRED)
    *          font             The font to use
    *          size             The size of the text (in pt)
    *          bold             Whether the text shouldd be bold or not
    *          marker           Whether to show a marker that indicates the X/Y coordinates
    *          valign           The vertical alignment
    *          halign           The horizontal alignment
    *          bounding         Whether to draw a bounding box for the text
    *          boundingStroke   The strokeStyle of the bounding box
    *          boundingFill     The fillStyle of the bounding box
    */
    RGraph.Text2 = function (obj, opt)
    {
        /**
        * An RGraph object can be given, or a string or the 2D rendering context
        * The coords are placed on the obj.coordsText variable ONLY if it's an RGraph object. The function
        * still returns the cooords though in all cases.
        */
        if (obj && obj.isRGraph) {
            var co = obj.context;
            var ca = obj.canvas;
        } else if (typeof obj == 'string') {
            var ca = document.getElementById(obj);
            var co = ca.getContext('2d');
        } else if (typeof obj.getContext == 'function') {
            var ca = obj;
            var co = ca.getContext('2d');
        } else if (obj.toString().indexOf('CanvasRenderingContext2D') != -1) {
            var co = obj;
            var ca = obj.context;
        }

        var x              = opt.x;
        var y              = opt.y;
        var originalX      = x;
        var originalY      = y;
        var text           = opt.text;
        var text_multiline = text.split(/\r?\n/g);
        var numlines       = text_multiline.length;
        var font           = opt.font ? opt.font : 'Arial';
        var size           = opt.size ? opt.size : 10;
        var size_pixels    = size * 1.5;
        var bold           = opt.bold;
        var halign         = opt.halign ? opt.halign : 'left';
        var valign         = opt.valign ? opt.valign : 'bottom';
        var tag            = typeof opt.tag == 'string' && opt.tag.length > 0 ? opt.tag : '';
        var marker         = opt.marker;
        var angle          = opt.angle || 0;
        
        /**
        * Changed the name of boundingFill/boundingStroke - this allows you to still use those names
        */
        if (typeof opt.boundingFill == 'string')   opt['bounding.fill']   = opt.boundingFill;
        if (typeof opt.boundingStroke == 'string') opt['bounding.stroke'] = opt.boundingStroke;

        var bounding                = opt.bounding;
        var bounding_stroke         = opt['bounding.stroke'] ? opt['bounding.stroke'] : 'black';
        var bounding_fill           = opt['bounding.fill'] ? opt['bounding.fill'] : 'rgba(255,255,255,0.7)';
        var bounding_shadow         = opt['bounding.shadow'];
        var bounding_shadow_color   = opt['bounding.shadow.color'] || '#ccc';
        var bounding_shadow_blur    = opt['bounding.shadow.blur'] || 3;
        var bounding_shadow_offsetx = opt['bounding.shadow.offsetx'] || 3;
        var bounding_shadow_offsety = opt['bounding.shadow.offsety'] || 3;
        var bounding_linewidth      = opt['bounding.linewidth'] || 1;



        /**
        * Initialize the return value to an empty object
        */
        var ret = {};



        /**
        * The text arg must be a string or a number
        */
        if (typeof text == 'number') {
            text = String(text);
        }

        if (typeof text != 'string') {
            alert('[RGRAPH TEXT] The text given must a string or a number');
            return;
        }
        
        
        
        /**
        * This facilitates vertical text
        */
        if (angle != 0) {
            co.save();
            co.translate(x, y);
            co.rotate((Math.PI / 180) * angle)
            x = 0;
            y = 0;
        }


        
        /**
        * Set the font
        */
        co.font = (opt.bold ? 'bold ' : '') + size + 'pt ' + font;



        /**
        * Measure the width/height. This must be done AFTER the font has been set
        */
        var width=0;
        for (var i=0; i<numlines; ++i) {
            width = Math.max(width, co.measureText(text_multiline[i]).width);
        }
        var height = size_pixels * numlines;




        /**
        * Accommodate old MSIE 7/8
        */
        //if (document.all && ISOLD) {
            //y += 2;
        //}



        /**
        * If marker is specified draw a marker at the X/Y coordinates
        */
        if (opt.marker) {
            var marker_size = 10;
            var strokestyle = co.strokeStyle;
            co.beginPath();
                co.strokeStyle = 'red';
                co.moveTo(x, y - marker_size);
                co.lineTo(x, y + marker_size);
                co.moveTo(x - marker_size, y);
                co.lineTo(x + marker_size, y);
            co.stroke();
            co.strokeStyle = strokestyle;
        }



        /**
        * Set the horizontal alignment
        */
        if (halign == 'center') {
            co.textAlign = 'center';
            var boundingX = x - 2 - (width / 2);
        } else if (halign == 'right') {
            co.textAlign = 'right';
            var boundingX = x - 2 - width;
        } else {
            co.textAlign = 'left';
            var boundingX = x - 2;
        }


        /**
        * Set the vertical alignment
        */
        if (valign == 'center') {
            
            co.textBaseline = 'middle';
            // Move the text slightly
            y -= 1;
            
            y -= ((numlines - 1) / 2) * size_pixels;
            var boundingY = y - (size_pixels / 2) - 2;
        
        } else if (valign == 'top') {
            co.textBaseline = 'top';

            var boundingY = y - 2;

        } else {

            co.textBaseline = 'bottom';
            
            // Move the Y coord if multiline text
            if (numlines > 1) {
                y -= ((numlines - 1) * size_pixels);
            }

            var boundingY = y - size_pixels - 2;
        }
        
        var boundingW = width + 4;
        var boundingH = height + 4;



        /**
        * Draw a bounding box if required
        */
        if (bounding) {

            var pre_bounding_linewidth     = co.lineWidth;
            var pre_bounding_strokestyle   = co.strokeStyle;
            var pre_bounding_fillstyle     = co.fillStyle;
            var pre_bounding_shadowcolor   = co.shadowColor;
            var pre_bounding_shadowblur    = co.shadowBlur;
            var pre_bounding_shadowoffsetx = co.shadowOffsetX;
            var pre_bounding_shadowoffsety = co.shadowOffsetY;

            co.lineWidth   = bounding_linewidth;
            co.strokeStyle = bounding_stroke;
            co.fillStyle   = bounding_fill;

            if (bounding_shadow) {
                co.shadowColor   = bounding_shadow_color;
                co.shadowBlur    = bounding_shadow_blur;
                co.shadowOffsetX = bounding_shadow_offsetx;
                co.shadowOffsetY = bounding_shadow_offsety;
            }

            //obj.context.strokeRect(boundingX, boundingY, width + 6, (size_pixels * numlines) + 4);
            //obj.context.fillRect(boundingX, boundingY, width + 6, (size_pixels * numlines) + 4);
            co.strokeRect(boundingX, boundingY, boundingW, boundingH);
            co.fillRect(boundingX, boundingY, boundingW, boundingH);

            // Reset the linewidth,colors and shadow to it's original setting
            co.lineWidth     = pre_bounding_linewidth;
            co.strokeStyle   = pre_bounding_strokestyle;
            co.fillStyle     = pre_bounding_fillstyle;
            co.shadowColor   = pre_bounding_shadowcolor
            co.shadowBlur    = pre_bounding_shadowblur
            co.shadowOffsetX = pre_bounding_shadowoffsetx
            co.shadowOffsetY = pre_bounding_shadowoffsety
        }

        
        
        /**
        * Draw the text
        */
        if (numlines > 1) {
            for (var i=0; i<numlines; ++i) {
                co.fillText(text_multiline[i], x, y + (size_pixels * i));
            }
        } else {
            co.fillText(text, x, y);
        }
        
        
        
        /**
        * If the text is at 90 degrees restore() the canvas - getting rid of the rotation
        * and the translate that we did
        */
        if (angle != 0) {
            if (angle == 90) {
                if (halign == 'left') {
                    if (valign == 'bottom') {boundingX = originalX - 2; boundingY = originalY - 2; boundingW = height + 4; boundingH = width + 4;}
                    if (valign == 'center') {boundingX = originalX - (height / 2) - 2; boundingY = originalY - 2; boundingW = height + 4; boundingH = width + 4;}
                    if (valign == 'top')    {boundingX = originalX - height - 2; boundingY = originalY - 2; boundingW = height + 4; boundingH = width + 4;}
                
                } else if (halign == 'center') {
                    if (valign == 'bottom') {boundingX = originalX - 2; boundingY = originalY - (width / 2) - 2; boundingW = height + 4; boundingH = width + 4;}
                    if (valign == 'center') {boundingX = originalX - (height / 2) -  2; boundingY = originalY - (width / 2) - 2; boundingW = height + 4; boundingH = width + 4;}
                    if (valign == 'top')    {boundingX = originalX - height -  2; boundingY = originalY - (width / 2) - 2; boundingW = height + 4; boundingH = width + 4;}
                
                } else if (halign == 'right') {
                    if (valign == 'bottom') {boundingX = originalX - 2; boundingY = originalY - width - 2; boundingW = height + 4; boundingH = width + 4;}
                    if (valign == 'center') {boundingX = originalX - (height / 2) - 2; boundingY = originalY - width - 2; boundingW = height + 4; boundingH = width + 4;}
                    if (valign == 'top')    {boundingX = originalX - height - 2; boundingY = originalY - width - 2; boundingW = height + 4; boundingH = width + 4;}
                }

            } else if (angle == 180) {

                if (halign == 'left') {
                    if (valign == 'bottom') {boundingX = originalX - width - 2; boundingY = originalY - 2; boundingW = width + 4; boundingH = height + 4;}
                    if (valign == 'center') {boundingX = originalX - width - 2; boundingY = originalY - (height / 2) - 2; boundingW = width + 4; boundingH = height + 4;}
                    if (valign == 'top')    {boundingX = originalX - width - 2; boundingY = originalY - height - 2; boundingW = width + 4; boundingH = height + 4;}
                
                } else if (halign == 'center') {
                    if (valign == 'bottom') {boundingX = originalX - (width / 2) - 2; boundingY = originalY - 2; boundingW = width + 4; boundingH = height + 4;}
                    if (valign == 'center') {boundingX = originalX - (width / 2) - 2; boundingY = originalY - (height / 2) - 2; boundingW = width + 4; boundingH = height + 4;}
                    if (valign == 'top')    {boundingX = originalX - (width / 2) - 2; boundingY = originalY - height - 2; boundingW = width + 4; boundingH = height + 4;}
                
                } else if (halign == 'right') {
                    if (valign == 'bottom') {boundingX = originalX - 2; boundingY = originalY - 2; boundingW = width + 4; boundingH = height + 4;}
                    if (valign == 'center') {boundingX = originalX - 2; boundingY = originalY - (height / 2) - 2; boundingW = width + 4; boundingH = height + 4;}
                    if (valign == 'top')    {boundingX = originalX - 2; boundingY = originalY - height - 2; boundingW = width + 4; boundingH = height + 4;}
                }
            
            } else if (angle == 270) {

                if (halign == 'left') {
                    if (valign == 'bottom') {boundingX = originalX - height - 2; boundingY = originalY - width - 2; boundingW = height + 4; boundingH = width + 4;}
                    if (valign == 'center') {boundingX = originalX - (height / 2) - 4; boundingY = originalY - width - 2; boundingW = height + 4; boundingH = width + 4;}
                    if (valign == 'top')    {boundingX = originalX - 2; boundingY = originalY - width - 2; boundingW = height + 4; boundingH = width + 4;}
                
                } else if (halign == 'center') {
                    if (valign == 'bottom') {boundingX = originalX - height - 2; boundingY = originalY - (width/2) - 2; boundingW = height + 4; boundingH = width + 4;}
                    if (valign == 'center') {boundingX = originalX - (height/2) - 4; boundingY = originalY - (width/2) - 2; boundingW = height + 4; boundingH = width + 4;}
                    if (valign == 'top')    {boundingX = originalX - 2; boundingY = originalY - (width/2) - 2; boundingW = height + 4; boundingH = width + 4;}
                
                } else if (halign == 'right') {
                    if (valign == 'bottom') {boundingX = originalX - height - 2; boundingY = originalY - 2; boundingW = height + 4; boundingH = width + 4;}
                    if (valign == 'center') {boundingX = originalX - (height/2) - 2; boundingY = originalY - 2; boundingW = height + 4; boundingH = width + 4;}
                    if (valign == 'top')    {boundingX = originalX - 2; boundingY = originalY - 2; boundingW = height + 4; boundingH = width + 4;}
                }
            }

            co.restore();
        }




        /**
        * Reset the text alignment so that text rendered
        */
        co.textBaseline = 'alphabetic';
        co.textAlign    = 'left';





        /**
        * Fill the ret variable with details of the text
        */
        ret.x      = boundingX;
        ret.y      = boundingY;
        ret.width  = boundingW;
        ret.height = boundingH
        ret.object = obj;
        ret.text   = text;
        ret.tag    = tag;



        /**
        * Save and then return the details of the text (but oly
        * if it's an RGraph object that was given)
        */
        if (obj && obj.isRGraph && obj.coordsText) {
            obj.coordsText.push(ret);
        }

        return ret;
    }



    /**
    * Takes a sequential index abd returns the group/index variation of it. Eg if you have a
    * sequential index from a grouped bar chart this function can be used to convert that into
    * an appropriate group/index combination
    * 
    * @param nindex number The sequential index
    * @param data   array  The original data (which is grouped)
    * @return              The group/index information
    */
    RGraph.sequentialIndexToGrouped = function (index, data)
    {
        var group         = 0;
        var grouped_index = 0;

        while (--index >= 0) {
            
            // Allow for numbers as well as arrays in the dataset
            if (typeof data[group] == 'number') {
                group++
                grouped_index = 0;
                continue;
            }

            grouped_index++;
            
            if (grouped_index >= data[group].length) {
                group++;
                grouped_index = 0;
            }
        }
        
        return [group, grouped_index];
    }



// Some other functions. Because they're rarely changed - they're hand minified
RGraph.LinearGradient=function(obj,x1,y1,x2,y2,color1,color2){var gradient=obj.context.createLinearGradient(x1,y1,x2,y2);var numColors=arguments.length-5;for (var i=5;i<arguments.length;++i){var color=arguments[i];var stop=(i-5)/(numColors-1);gradient.addColorStop(stop,color);}return gradient;}
RGraph.RadialGradient=function(obj,x1,y1,r1,x2,y2,r2,color1,color2){var gradient=obj.context.createRadialGradient(x1,y1,r1,x2,y2,r2);var numColors=arguments.length-7;for(var i=7;i<arguments.length; ++i){var color=arguments[i];var stop=(i-7)/(numColors-1);gradient.addColorStop(stop,color);}return gradient;}
RGraph.array_shift=function(arr){var ret=[];for(var i=1;i<arr.length;++i){ret.push(arr[i]);}return ret;}
RGraph.AddEventListener=function(id,e,func){var type=arguments[3]?arguments[3]:'unknown';RGraph.Registry.Get('chart.event.handlers').push([id,e,func,type]);}
RGraph.ClearEventListeners=function(id){if(id&&id=='window'){window.removeEventListener('mousedown',window.__rgraph_mousedown_event_listener_installed__,false);window.removeEventListener('mouseup',window.__rgraph_mouseup_event_listener_installed__,false);}else{var canvas = document.getElementById(id);canvas.removeEventListener('mouseup',canvas.__rgraph_mouseup_event_listener_installed__,false);canvas.removeEventListener('mousemove',canvas.__rgraph_mousemove_event_listener_installed__,false);canvas.removeEventListener('mousedown',canvas.__rgraph_mousedown_event_listener_installed__,false);canvas.removeEventListener('click',canvas.__rgraph_click_event_listener_installed__,false);}}
RGraph.HidePalette=function(){var div=RGraph.Registry.Get('palette');if(typeof(div)=='object'&&div){div.style.visibility='hidden';div.style.display='none';RGraph.Registry.Set('palette',null);}}
RGraph.random=function(min,max){var dp=arguments[2]?arguments[2]:0;var r=Math.random();return Number((((max - min) * r) + min).toFixed(dp));}
RGraph.NoShadow=function(obj){obj.context.shadowColor='rgba(0,0,0,0)';obj.context.shadowBlur=0;obj.context.shadowOffsetX=0;obj.context.shadowOffsetY=0;}
RGraph.SetShadow=function(obj,color,offsetx,offsety,blur){obj.context.shadowColor=color;obj.context.shadowOffsetX=offsetx;obj.context.shadowOffsetY=offsety;obj.context.shadowBlur=blur;}
RGraph.array_reverse=function(arr){var newarr=[];for(var i=arr.length-1;i>=0;i--){newarr.push(arr[i]);}return newarr;}
RGraph.Registry.Set=function(name,value){RGraph.Registry.store[name]=value;return value;}
RGraph.Registry.Get=function(name){return RGraph.Registry.store[name];}
RGraph.degrees2Radians=function(degrees){return degrees*(PI/180);}
RGraph.log=(function(n,base){var log=Math.log;return function(n,base){return log(n)/(base?log(base):1);};})();
RGraph.is_array=function(obj){return obj!=null&&obj.constructor.toString().indexOf('Array')!=-1;}
RGraph.trim=function(str){return RGraph.ltrim(RGraph.rtrim(str));}
RGraph.ltrim=function(str){return str.replace(/^(\s|\0)+/, '');}
RGraph.rtrim=function(str){return str.replace(/(\s|\0)+$/, '');}
RGraph.GetHeight=function(obj){return obj.canvas.height;}
RGraph.GetWidth=function(obj){return obj.canvas.width;}
RGraph.is_null=function(arg){if(arg==null||(typeof(arg))=='object'&&!arg){return true;}return false;}
RGraph.Timer=function(label){if(typeof(RGraph.TIMER_LAST_CHECKPOINT)=='undefined'){RGraph.TIMER_LAST_CHECKPOINT=Date.now();}var now=Date.now();console.log(label+': '+(now-RGraph.TIMER_LAST_CHECKPOINT).toString());RGraph.TIMER_LAST_CHECKPOINT=now;}
RGraph.Async=function(func){return setTimeout(func,arguments[1]?arguments[1]:1);}
RGraph.isIE=function(){return navigator.userAgent.indexOf('MSIE')>0;};ISIE=RGraph.isIE();
RGraph.isIE6=function(){return navigator.userAgent.indexOf('MSIE 6')>0;};ISIE6=RGraph.isIE6();
RGraph.isIE7=function(){return navigator.userAgent.indexOf('MSIE 7')>0;};ISIE7=RGraph.isIE7();
RGraph.isIE8=function(){return navigator.userAgent.indexOf('MSIE 8')>0;};ISIE8=RGraph.isIE8();
RGraph.isIE9=function(){return navigator.userAgent.indexOf('MSIE 9')>0;};ISIE9=RGraph.isIE9();
RGraph.isIE10=function(){return navigator.userAgent.indexOf('MSIE 10')>0;};ISIE10=RGraph.isIE10();
RGraph.isIE9up=function(){navigator.userAgent.match(/MSIE (\d+)/);return Number(RegExp.$1)>=9;};ISIE9UP=RGraph.isIE9up();
RGraph.isIE10up=function(){navigator.userAgent.match(/MSIE (\d+)/);return Number(RegExp.$1)>=10;};ISIE10UP=RGraph.isIE10up();
RGraph.isOld=function(){return ISIE6||ISIE7||ISIE8;};ISOLD=RGraph.isOld();
RGraph.Reset=function(canvas){canvas.width=canvas.width;RGraph.ObjectRegistry.Clear(canvas);canvas.__rgraph_aa_translated__=false;}
function pd(variable){RGraph.pr(variable);}
function p(variable){RGraph.pr(arguments[0],arguments[1],arguments[3]);}
function a(variable){alert(variable);}
function cl(variable){return console.log(variable);}

    /**
    * o------------------------------------------------------------------------------o
    * | This file is part of the RGraph package - you can learn more at:             |
    * |                                                                              |
    * |                          http://www.rgraph.net                               |
    * |                                                                              |
    * | This package is licensed under the RGraph license. For all kinds of business |
    * | purposes there is a small one-time licensing fee to pay and for non          |
    * | commercial  purposes it is free to use. You can read the full license here:  |
    * |                                                                              |
    * |                      http://www.rgraph.net/license                           |
    * o------------------------------------------------------------------------------o
    */

    /**
    * Initialise the various objects
    */
    if (typeof(RGraph) == 'undefined') RGraph = {isRGraph:true,type:'common'};



    /**
    * This is the window click event listener. It redraws all canvas tags on the page.
    */
    RGraph.InstallWindowMousedownListener = function (obj)
    {
        if (!window.__rgraph_mousedown_event_listener_installed__) {
            var func = function (e)
            {
                /**
                * For firefox add the window.event object
                */
                if (navigator.userAgent.indexOf('Firefox') >= 0) window.event = e;
                
                e = RGraph.FixEventObject(e);
    
    
                /**
                * First fire the user specified window.onmousedown_rgraph listener if there is any.
                */
    
                if (typeof(window.onmousedown_rgraph) == 'function') {
                    window.onmousedown_rgraph(e);
                }
    

                if (RGraph.HideTooltip && RGraph.Registry.Get('chart.tooltip')) {
                    RGraph.Clear(RGraph.Registry.Get('chart.tooltip').__canvas__);
                    RGraph.Redraw();
                    RGraph.HideTooltip();
                }
            }
            window.addEventListener('mousedown', func, false);
            
            // Set this so the event listener isnt repeatedly installed
            window.__rgraph_mousedown_event_listener_installed__ = func;
        }
    }



    /**
    * This is the window click event listener. It redraws all canvas tags on the page.
    */
    RGraph.InstallWindowMouseupListener = function (obj)
    {
        if (!window.__rgraph_mouseup_event_listener_installed__) {
            var func = function (e)
            {
                /**
                * For firefox add the window.event object
                */
                if (navigator.userAgent.indexOf('Firefox') >= 0) window.event = e;
                
                e = RGraph.FixEventObject(e);
    
    
                /**
                * Stop any annotating that may be going on
                */
                if (RGraph.Annotating_window_onmouseup) {
                    RGraph.Annotating_window_onmouseup(e);
                    return;
                }
    
                /**
                * First fire the user specified window.onmouseup_rgraph listener if there is any
                */
                if (typeof(window.onmouseup_rgraph) == 'function') {
                    window.onmouseup_rgraph(e);
                }
    
                /**
                * End adjusting
                */
                if (RGraph.Registry.Get('chart.adjusting') || RGraph.Registry.Get('chart.adjusting.gantt')) {
                    RGraph.FireCustomEvent(RGraph.Registry.Get('chart.adjusting'), 'onadjustend');
                }
    
                RGraph.Registry.Set('chart.adjusting', null);
                RGraph.Registry.Set('chart.adjusting.shape', null);
                RGraph.Registry.Set('chart.adjusting.gantt', null);
    
    
                // ==============================================
                // Finally, redraw the chart
                // ==============================================

                var tags = document.getElementsByTagName('canvas');
                for (var i=0; i<tags.length; ++i) {
                    if (tags[i].__object__ && tags[i].__object__.isRGraph) {
                        if (!tags[i].__object__.Get('chart.annotatable')) {
                            if (!tags[i].__rgraph_trace_cover__ && !noredraw) {
                                RGraph.Clear(tags[i]);
                            } else {
                                var noredraw = true;
                            }
                        }
                    }
                }
    
                if (!noredraw) {
                    RGraph.Redraw();
                }
            }
            window.addEventListener('mouseup', func, false);

            // Set this so the event listener isn't repeatedly installed
            window.__rgraph_mouseup_event_listener_installed__ = func;
        }
    }







    /**
    * This is the canvas mouseup event listener. It installs the mouseup event for the
    * canvas. The mouseup event then checks the relevant object.
    * 
    * @param object obj The chart object
    */
    RGraph.InstallCanvasMouseupListener = function (obj)
    {
        if (!obj.canvas.__rgraph_mouseup_event_listener_installed__) {
            var func = function (e)
            {
                /**
                * For firefox add the window.event object
                */
                if (navigator.userAgent.indexOf('Firefox') >= 0) window.event = e;
    
                e = RGraph.FixEventObject(e);
    
    
                /**
                * First fire the user specified onmouseup listener if there is any
                */
                if (typeof(e.target.onmouseup_rgraph) == 'function') {
                    e.target.onmouseup_rgraph(e);
                }
    
    
                // *************************************************************************
                // Tooltips
                // *************************************************************************
    
    
                // This causes things at the edge of the chart area - eg line chart hotspots - not to fire because the
                // cursor is out of the chart area
                var objects = RGraph.ObjectRegistry.getObjectsByXY(e);
                //var objects = RGraph.ObjectRegistry.getObjectsByCanvasID(e.target.id);

                if (objects) {
                    for (var i=0; i<objects.length; ++i) {
                        
                        var obj = objects[i];
                        var id  = objects[i].id;
    
    
    
                        // =========================================================================
                        // Tooltips
                        // ========================================================================
    
                    
                        if (!RGraph.is_null(obj) && RGraph.Tooltip) {
    
                            var shape = obj.getShape(e);
    
                            if (shape && shape['tooltip']) {
    
                                var text = shape['tooltip'];
    
                                if (text) {
    
                                    var type = shape['object'].type;
    
                                    if (   type == 'line'
                                        || type == 'rscatter'
                                        || (type == 'scatter' && !obj.Get('chart.boxplot'))
                                        || type == 'radar') {
    
                                        var canvasXY = RGraph.getCanvasXY(obj.canvas);
                                        var x = canvasXY[0] + shape['x'];
                                        var y = canvasXY[1] + shape['y'];
    
                                    } else {
                                        var x = e.pageX;
                                        var y = e.pageY;
                                    }
                                    
                                    // Taken out November 2012
                                    //if (obj.Get('chart.tooltips.coords.page')) {
                                    //    var x = e.pageX;
                                    //    var y = e.pageY;
                                    //}
    
                                    RGraph.Clear(obj.canvas);
                                    RGraph.Redraw();
                                    obj.Highlight(shape);
                                    RGraph.Registry.Set('chart.tooltip.shape', shape);
                                    RGraph.Tooltip(obj, text, x, y, shape['index'], e);
    
                                    // Add the shape that triggered the tooltip
                                    if (RGraph.Registry.Get('chart.tooltip')) {
                                        RGraph.Registry.Get('chart.tooltip').__shape__ = shape;
    
                                        RGraph.EvaluateCursor(e);
                                    }
    
                                    e.cancelBubble = true;
                                    e.stopPropagation();
                                    return false;
                                }
                            }
                        }
    
    
    
    
    
                        // =========================================================================
                        // Adjusting
                        // ========================================================================
        
        
        
                        if (RGraph.Registry.Get('chart.adjusting') || RGraph.Registry.Get('chart.adjusting.gantt')) {
                            RGraph.FireCustomEvent(RGraph.Registry.Get('chart.adjusting'), 'onadjustend');
                        }
        
                        RGraph.Registry.Set('chart.adjusting', null);
                        RGraph.Registry.Set('chart.adjusting.shape', null);
                        RGraph.Registry.Set('chart.adjusting.gantt', null);
    
                        /**
                        * If the mouse pointer is over a "front" chart this prevents charts behind it
                        * from firing their events.
                        */
                        if (shape || (obj.overChartArea && obj.overChartArea(e)) ) {
                            break;
                        }
                    }
            
        
                    // =========================================================================
                    // Interactive key
                    // ========================================================================
        
    
                    if (typeof(InteractiveKey_line_mouseup) == 'function') InteractiveKey_line_mouseup(e);
                    if (typeof(InteractiveKey_pie_mouseup) == 'function')  InteractiveKey_pie_mouseup(e);
                }
            }
            obj.canvas.addEventListener('mouseup', func, false);
        
            // Set this so the canvas mouseup event listener isn't repeatedly installed
            obj.canvas.__rgraph_mouseup_event_listener_installed__ = func;
        }
    }



    /**
    * This is the canvas mousemove event listener.
    * 
    * @param object obj The chart object
    */
    RGraph.InstallCanvasMousemoveListener = function (obj)
    {
        if (!obj.canvas.__rgraph_mousemove_event_listener_installed__) {
            var func = function (e)
            {
                /**
                * For firefox add the window.event object
                */
                if (navigator.userAgent.indexOf('Firefox') >= 0) window.event = e;
                e = RGraph.FixEventObject(e);
    
    
    
                /**
                * First fire the user specified onmousemove listener if there is any
                */
                if (typeof(e.target.onmousemove_rgraph) == 'function') {
                    e.target.onmousemove_rgraph(e);
                }
    
    
    
                /**
                * Go through all the objects and check them to see if anything needs doing
                */
                var objects = RGraph.ObjectRegistry.getObjectsByXY(e);
                //var objects = RGraph.ObjectRegistry.getObjectsByCanvasID(e.target.id);
    
                if (objects && objects.length) {
                    for (var i=0; i<objects.length; ++i) {
    
                        var obj = objects[i];
                        var id  = obj.id;
    
                        if (!obj.getShape) {
                            continue;
                        }
    
    
                        var shape = obj.getShape(e);
    
    
                        // ================================================================================================ //
                        // This facilitates the chart.events.mousemove option
                        // ================================================================================================ //
                        
                        var func = obj.Get('chart.events.mousemove');
    
                        if (!func && typeof(obj.onmousemove) == 'function') {
                            var func = obj.onmousemove;
                        }
    
                        /**
                        * 
                        */
                        if (shape) {
                            var index = shape['object'].type == 'scatter' ? shape['index_adjusted'] : shape['index'];
                            if (typeof(obj['$' + index]) == 'object' && typeof(obj['$' + index].onmousemove) == 'function') {
                                var func2 = obj['$' + index].onmousemove;
                            }
                        }
    
                        /**
                        * This bit saves the current pointer style if there isn't one already saved
                        */
                        if (shape && (typeof(func) == 'function' || typeof(func2) == 'function')) {
    
                            if (obj.Get('chart.events.mousemove.revertto') == null) {
                                obj.Set('chart.events.mousemove.revertto', e.target.style.cursor);
                            }
    
                            if (typeof(func)  == 'function')  func(e, shape);
                            if (typeof(func2) == 'function') func2(e, shape);
    
    //return;
    
                        } else if (typeof(obj.Get('chart.events.mousemove.revertto')) == 'string') {
            
                            RGraph.cursor.push('default');
                            obj.Set('chart.events.mousemove.revertto', null);
                        }
    
    
    
                        // ================================================================================================ //
                        // Tooltips
                        // ================================================================================================ //
    

                        if (   shape
                            && (obj.Get('chart.tooltips') && obj.Get('chart.tooltips')[shape['index']] || shape['tooltip'])
                            && (obj.Get('chart.tooltips.event') == 'onmousemove' || obj.Get('chart.tooltips.event') == 'mousemove')
                            && (RGraph.is_null(RGraph.Registry.Get('chart.tooltip')) || RGraph.Registry.Get('chart.tooltip').__index__ != shape['index'] || (typeof(shape['dataset']) == 'number' && shape['dataset'] != RGraph.Registry.Get('chart.tooltip').__shape__['dataset']) || obj.uid != RGraph.Registry.Get('chart.tooltip').__object__.uid)
                           ) {

                            RGraph.Clear(obj.canvas);
                            RGraph.Redraw();
                            obj.canvas.__rgraph_mouseup_event_listener_installed__(e);
    
                            return;
                        }
            
            
                        // ================================================================================================ //
                        // Adjusting
                        // ================================================================================================ //
            
    
                        if (obj && obj.Get('chart.adjustable')) {
                            obj.Adjusting_mousemove(e);
                        }
                    
    
                        /**
                        * This facilitates breaking out of the loop when a shape has been found - 
                        * ie the cursor is over a shape an upper chart
                        */
                        if (shape || (obj.overChartArea && obj.overChartArea(e) )) {
                            break;
                        }
                    }
                }
    
                // ================================================================================================ //
                // Crosshairs
                // ================================================================================================ //
    

                if (e.target && e.target.__object__ && e.target.__object__.Get('chart.crosshairs')) {
                    RGraph.DrawCrosshairs(e, e.target.__object__);
                }
            
            
                // ================================================================================================ //
                // Interactive key
                // ================================================================================================ //
    
    
                if (typeof(InteractiveKey_line_mousemove) == 'function') InteractiveKey_line_mousemove(e);
                if (typeof(InteractiveKey_pie_mousemove) == 'function') InteractiveKey_pie_mousemove(e);
    
    
                // ================================================================================================ //
                // Annotating
                // ================================================================================================ //
    
    
                if (e.target.__object__ && e.target.__object__.Get('chart.annotatable') && RGraph.Annotating_canvas_onmousemove) {
                    RGraph.Annotating_canvas_onmousemove(e);
                }
    
    
    
                /**
                * Determine the pointer
                */
                RGraph.EvaluateCursor(e);
            }
            obj.canvas.addEventListener('mousemove', func, false);

            // Set this so the event listener isn't constantly added
            obj.canvas.__rgraph_mousemove_event_listener_installed__ = func;
        }
    }



    /**
    * This is the canvas mousedown event listener.
    * 
    * @param object obj The chart object
    */
    RGraph.InstallCanvasMousedownListener = function (obj)
    {
        if (!obj.canvas.__rgraph_mousedown_event_listener_installed__) {
            var func = function (e)
            {
                /**
                * For firefox add the window.event object
                */
                if (navigator.userAgent.indexOf('Firefox') >= 0) window.event = e;
                
                e = RGraph.FixEventObject(e);
    
    
                /**
                * First fire the user specified onmousedown listener if there is any
                */
                if (typeof(e.target.onmousedown_rgraph) == 'function') {
                    e.target.onmousedown_rgraph(e);
                }
    
                /**
                * Annotating
                */
                if (e.target.__object__ && e.target.__object__.Get('chart.annotatable') && RGraph.Annotating_canvas_onmousedown) {
                    RGraph.Annotating_canvas_onmousedown(e);
                    return;
                }
    
                var obj = RGraph.ObjectRegistry.getObjectByXY(e);
    
                if (obj) {

                    var id    = obj.id;
    
                    /*************************************************************
                    * Handle adjusting for all object types
                    *************************************************************/
                    if (obj && obj.isRGraph && obj.Get('chart.adjustable')) {
                        
                        /**
                        * Check the cursor is in the correct area
                        */
                        var obj = RGraph.ObjectRegistry.getObjectByXY(e);
    
                        if (obj && obj.isRGraph) {
                        
                            // If applicable, get the appropriate shape and store it in the registry
                            switch (obj.type) {
                                case 'bar':   var shape = obj.getShapeByX(e); break;
                                case 'gantt':
                                    var shape = obj.getShape(e);
                                    if (shape) {
                                        var mouseXY = RGraph.getMouseXY(e);
                                        RGraph.Registry.Set('chart.adjusting.gantt', {
                                                                                      'index': shape['index'],
                                                                                      'object': obj,
                                                                                      'mousex': mouseXY[0],
                                                                                      'mousey': mouseXY[1],
                                                                                      'event_start': obj.data[shape['index']][0],
                                                                                      'event_duration': obj.data[shape['index']][1],
                                                                                      'mode': (mouseXY[0] > (shape['x'] + shape['width'] - 5) ? 'resize' : 'move'),
                                                                                      'shape': shape
                                                                                     });
                                    }
                                    break;
                                case 'line':  var shape = obj.getShape(e); break;
                                default:      var shape = null;
                            }
    
                            RGraph.Registry.Set('chart.adjusting.shape', shape);
    
    
                            // Fire the onadjustbegin event
                            RGraph.FireCustomEvent(obj, 'onadjustbegin');
    
                            RGraph.Registry.Set('chart.adjusting', obj);
        
    
                            // Liberally redraw the canvas
                            RGraph.Clear(obj.canvas);
                            RGraph.Redraw();
        
                            // Call the mousemove event listener so that the canvas is adjusted even though the mouse isn't moved
                            obj.canvas.__rgraph_mousemove_event_listener_installed__(e);
                        }
                    }
    
    
                    RGraph.Clear(obj.canvas);
                    RGraph.Redraw();
                }
            }
            obj.canvas.addEventListener('mousedown', func, false);
            
            obj.canvas.__rgraph_mousedown_event_listener_installed__ = func;
        }
    }





    /**
    * This is the canvas click event listener. Used by the pseudo event listener
    * 
    * @param object obj The chart object
    */
    RGraph.InstallCanvasClickListener = function (obj)
    {
        if (!obj.canvas.__rgraph_click_event_listener_installed__) {
            var func = function (e)
            {
                /**
                * For firefox add the window.event object
                */
                if (navigator.userAgent.indexOf('Firefox') >= 0) window.event = e;
                
                e = RGraph.FixEventObject(e);
    
    
                /**
                * First fire the user specified onmousedown listener if there is any
                */
                if (typeof(e.target.onclick_rgraph) == 'function') {
                    e.target.onclick_rgraph(e);
                }
    
                var objects = RGraph.ObjectRegistry.getObjectsByXY(e);

                for (var i=0; i<objects.length; ++i) {

                    var obj   = objects[i];
                    var id    = obj.id;
                    var shape = obj.getShape(e);

                    /**
                    * This bit saves the current pointer style if there isn't one already saved
                    */
                    var func = obj.Get('chart.events.click');
                    
                    if (!func && typeof(obj.onclick) == 'function') {
                        func = obj.onclick;
                    }
    
                    if (shape && typeof(func) == 'function') {
                        func(e, shape);
                        
                        /**
                        * If objects are layered on top of each other this return
                        * stops objects underneath from firing once the "top"
                        * objects user event has fired
                        */
                        return;
                    }
    
                    /**
                    * The property takes priority over this.
                    */
                    if (shape) {
    
                        var index = shape['object'].type == 'scatter' ? shape['index_adjusted'] : shape['index'];
        
                        if (typeof(index) == 'number' && obj['$' + index]) {
                            
                            var func = obj['$' + index].onclick;
                            
                            if (typeof(func) == 'function') {
                                
                                func(e, shape);
                                
                                /**
                                * If objects are layered on top of each other this return
                                * stops objects underneath from firing once the "top"
                                * objects user event has fired
                                */
                                return;
                            }
                        }
                    }
                    
                    /**
                    * This facilitates breaking out of the loop when a shape has been found - 
                    * ie the cursor is over a shape an upper chart
                    */
                    if (shape || (obj.overChartArea && obj.overChartArea(e)) ) {
                        break;
                    }
                }
            }
            obj.canvas.addEventListener('click', func, false);
            
            obj.canvas.__rgraph_click_event_listener_installed__ = func;
        }
    }



    /**
    * This function evaluates the various cursor settings and if there's one for pointer, changes it to that
    */
    RGraph.EvaluateCursor = function (e)
    {
        var mouseXY = RGraph.getMouseXY(e);
        var mouseX  = mouseXY[0];
        var mouseY  = mouseXY[1];
        var canvas  = e.target;

        /**
        * Tooltips cause the mouse pointer to change
        */
        var objects = RGraph.ObjectRegistry.getObjectsByCanvasID(canvas.id);
        
        for (var i=0; i<objects.length; ++i) {
            if ((objects[i].getShape && objects[i].getShape(e)) || (objects[i].overChartArea && objects[i].overChartArea(e))) {
                var obj = objects[i];
                var id  = obj.id;
            }
        }

        if (!RGraph.is_null(obj)) {
            if (obj.getShape && obj.getShape(e)) {

                var shape = obj.getShape(e);

                if (obj.Get('chart.tooltips')) {

                    var text = RGraph.parseTooltipText(obj.Get('chart.tooltips'), shape['index']);
                    
                    if (!text && shape['object'].type == 'scatter' && shape['index_adjusted']) {
                        text = RGraph.parseTooltipText(obj.Get('chart.tooltips'), shape['index_adjusted']);
                    }

                    /**
                    * This essentially makes front charts "hide" the back charts
                    */
                    if (text) {
                        var pointer = true;
                    }
                }
            }

            /**
            * Now go through the key coords and see if it's over that.
            */
            if (!RGraph.is_null(obj) && obj.Get('chart.key.interactive')) {
                for (var j=0; j<obj.coords.key.length; ++j) {
                    if (mouseX > obj.coords.key[j][0] && mouseX < (obj.coords.key[j][0] + obj.coords.key[j][2]) && mouseY > obj.coords.key[j][1] && mouseY < (obj.coords.key[j][1] + obj.coords.key[j][3])) {
                        var pointer = true;
                    }
                }
            }
        }

        /**
        * It can be specified in the user mousemove event - remember it can now be specified in THREE ways
        */
        if (!RGraph.is_null(shape) && !RGraph.is_null(obj)) {

            if (!RGraph.is_null(obj.Get('chart.events.mousemove')) && typeof(obj.Get('chart.events.mousemove')) == 'function') {
                var str = (obj.Get('chart.events.mousemove')).toString();
                if (str.match(/pointer/) && str.match(/cursor/) && str.match(/style/)) {
                    var pointer = true;
                }
            }

            if (!RGraph.is_null(obj.onmousemove) && typeof(obj.onmousemove) == 'function') {
                var str = (obj.onmousemove).toString();
                if (str.match(/pointer/) && str.match(/cursor/) && str.match(/style/)) {
                    var pointer = true;
                }
            }
            
            var index = shape['object'].type == 'scatter' ? shape['index_adjusted'] : shape['index'];
            if (!RGraph.is_null(obj['$' + index]) && typeof(obj['$' + index].onmousemove) == 'function') {
                var str = (obj['$' + index].onmousemove).toString();
                if (str.match(/pointer/) && str.match(/cursor/) && str.match(/style/)) { 
                    var pointer = true;
                }
            }
        }

        /**
        * Is the chart resizable? Go through all the objects again
        */
        var objects = RGraph.ObjectRegistry.objects.byCanvasID;

        for (var i=0; i<objects.length; ++i) {
            if (objects[i] && objects[i][1].Get('chart.resizable')) {
                var resizable = true;
            }
        }

        if (resizable && mouseX > (e.target.width - 32) && mouseY > (e.target.height - 16)) {
            pointer = true;
        }


        if (pointer) {
            e.target.style.cursor = 'pointer';
        } else if (e.target.style.cursor == 'pointer') {
            e.target.style.cursor = 'default';
        } else {
            e.target.style.cursor = null;
        }

        

        // =========================================================================
        // Resize cursor
        // =========================================================================


        if (resizable && mouseX >= (e.target.width - 15) && mouseY >= (e.target.height - 15)) {
            e.target.style.cursor = 'move';
        }


        // =========================================================================
        // Interactive key
        // =========================================================================



        if (typeof(mouse_over_key) == 'boolean' && mouse_over_key) {
            e.target.style.cursor = 'pointer';
        }

        
        // =========================================================================
        // Gantt chart adjusting
        // =========================================================================


        if (obj && obj.type == 'gantt' && obj.Get('chart.adjustable')) {
            if (obj.getShape && obj.getShape(e)) {
                e.target.style.cursor = 'ew-resize';
            } else {
                e.target.style.cursor = 'default';
            }
        }

        
        // =========================================================================
        // Line chart adjusting
        // =========================================================================


        if (obj && obj.type == 'line' && obj.Get('chart.adjustable')) {
            if (obj.getShape && obj.getShape(e)) {
                e.target.style.cursor = 'ns-resize';
            } else {
                e.target.style.cursor = 'default';
            }
        }

        
        // =========================================================================
        // Annotatable
        // =========================================================================


        if (e.target.__object__ && e.target.__object__.Get('chart.annotatable')) {
            e.target.style.cursor = 'crosshair';
        }
    }



    /**
    * This function handles the tooltip text being a string, function
    * 
    * @param mixed tooltip This could be a string or a function. If it's a function it's called and
    *                       the return value is used as the tooltip text
    * @param numbr idx The index of the tooltip.
    */
    RGraph.parseTooltipText = function (tooltips, idx)
    {
        // No tooltips
        if (!tooltips) {
            return null;
        }

        // Get the tooltip text
        if (typeof(tooltips) == 'function') {
            var text = tooltips(idx);

        // A single tooltip. Only supported by the Scatter chart
        } else if (typeof(tooltips) == 'string') {
            var text = tooltips;

        } else if (typeof(tooltips) == 'object' && typeof(tooltips)[idx] == 'function') {
            var text = tooltips[idx](idx);

        } else if (typeof(tooltips)[idx] == 'string' && tooltips[idx]) {
            var text = tooltips[idx];

        } else {
            var text = '';
        }

        if (text == 'undefined') {
            text = '';
        } else if (text == 'null') {
            text = '';
        }

        // Conditional in case the tooltip file isn't included
        return RGraph.getTooltipTextFromDIV ? RGraph.getTooltipTextFromDIV(text) : text;
    }



    /**
    * Draw crosshairs if enabled
    * 
    * @param object obj The graph object (from which we can get the context and canvas as required)
    */
    RGraph.DrawCrosshairs = function (e, obj)
    {
        var e            = RGraph.FixEventObject(e);
        var width        = obj.canvas.width;
        var height       = obj.canvas.height;
        var mouseXY      = RGraph.getMouseXY(e);
        var x            = mouseXY[0];
        var y            = mouseXY[1];
        var gutterLeft   = obj.gutterLeft;
        var gutterRight  = obj.gutterRight;
        var gutterTop    = obj.gutterTop;
        var gutterBottom = obj.gutterBottom;
        var prop         = obj.properties;

        RGraph.RedrawCanvas(obj.canvas);

        if (   x >= gutterLeft
            && y >= gutterTop
            && x <= (width - gutterRight)
            && y <= (height - gutterBottom)
           ) {

            var linewidth = prop['chart.crosshairs.linewidth'] ? prop['chart.crosshairs.linewidth'] : 1;
            obj.context.lineWidth = linewidth ? linewidth : 1;

            obj.context.beginPath();
            obj.context.strokeStyle = prop['chart.crosshairs.color'];





            /**
            * The chart.crosshairs.snap option
            */
            if (prop['chart.crosshairs.snap']) {
            
                // Linear search for the closest point
                var point = null;
                var dist  = null;
                var len   = null;
                
                if (obj.type == 'line') {
            
                    for (var i=0; i<obj.coords.length; ++i) {
                    
                        var len = RGraph.getHypLength(obj.coords[i][0], obj.coords[i][1], x, y);
            
                        // Check the mouse X coordinate
                        if (typeof(dist) != 'number' || len < dist) {
                            var point = i;
                            var dist = len;
                        }
                    }
                
                    x = obj.coords[point][0];
                    y = obj.coords[point][1];
                    
                    // Get the dataset
                    for (var dataset=0; dataset<obj.coords2.length; ++dataset) {
                        for (var point=0; point<obj.coords2[dataset].length; ++point) {
                            if (obj.coords2[dataset][point][0] == x && obj.coords2[dataset][point][1] == y) {
                                obj.canvas.__crosshairs_snap_dataset__ = dataset;
                                obj.canvas.__crosshairs_snap_point__   = point;
                            }
                        }
                    }
            
                } else {
            
                    for (var i=0; i<obj.coords.length; ++i) {
                        for (var j=0; j<obj.coords[i].length; ++j) {
                            
                            // Check the mouse X coordinate
                            var len = RGraph.getHypLength(obj.coords[i][j][0], obj.coords[i][j][1], x, y);
            
                            if (typeof(dist) != 'number' || len < dist) {
            
                                var dataset = i;
                                var point   = j;
                                var dist   = len;
                            }
                        }
            
                    }
                    obj.canvas.__crosshairs_snap_dataset__ = dataset;
                    obj.canvas.__crosshairs_snap_point__   = point;
            
                    x = obj.coords[dataset][point][0];
                    y = obj.coords[dataset][point][1];
                }
            }






            // Draw a top vertical line
            if (prop['chart.crosshairs.vline']) {
                obj.context.moveTo(Math.round(x), Math.round(gutterTop));
                obj.context.lineTo(Math.round(x), Math.round(height - gutterBottom));
            }

            // Draw a horizontal line
            if (prop['chart.crosshairs.hline']) {
                obj.context.moveTo(Math.round(gutterLeft), Math.round(y));
                obj.context.lineTo(Math.round(width - gutterRight), Math.round(y));
            }

            obj.context.stroke();
            
            
            /**
            * Need to show the coords?
            */
            if (obj.type == 'scatter' && prop['chart.crosshairs.coords']) {

                var xCoord = (((x - gutterLeft) / (width - gutterLeft - gutterRight)) * (prop['chart.xmax'] - prop['chart.xmin'])) + prop['chart.xmin'];
                    xCoord = xCoord.toFixed(prop['chart.scale.decimals']);
                var yCoord = obj.max - (((y - prop['chart.gutter.top']) / (height - gutterTop - gutterBottom)) * obj.max);

                if (obj.type == 'scatter' && obj.properties['chart.xaxispos'] == 'center') {
                    yCoord = (yCoord - (obj.max / 2)) * 2;
                }

                yCoord = yCoord.toFixed(prop['chart.scale.decimals']);

                var div      = RGraph.Registry.Get('chart.coordinates.coords.div');
                var mouseXY  = RGraph.getMouseXY(e);
                var canvasXY = RGraph.getCanvasXY(obj.canvas);
                
                if (!div) {
                    var div = document.createElement('DIV');
                    div.__object__     = obj;
                    div.style.position = 'absolute';
                    div.style.backgroundColor = 'white';
                    div.style.border = '1px solid black';
                    div.style.fontFamily = 'Arial, Verdana, sans-serif';
                    div.style.fontSize = '10pt'
                    div.style.padding = '2px';
                    div.style.opacity = 1;
                    div.style.WebkitBorderRadius = '3px';
                    div.style.borderRadius = '3px';
                    div.style.MozBorderRadius = '3px';
                    document.body.appendChild(div);
                    
                    RGraph.Registry.Set('chart.coordinates.coords.div', div);
                }
                
                // Convert the X/Y pixel coords to correspond to the scale
                div.style.opacity = 1;
                div.style.display = 'inline';

                if (!prop['chart.crosshairs.coords.fixed']) {
                    div.style.left = Math.max(2, (e.pageX - div.offsetWidth - 3)) + 'px';
                    div.style.top = Math.max(2, (e.pageY - div.offsetHeight - 3))  + 'px';
                } else {
                    div.style.left = canvasXY[0] + gutterLeft + 3 + 'px';
                    div.style.top  = canvasXY[1] + gutterTop + 3 + 'px';
                }

                div.innerHTML = '<span style="color: #666">' + prop['chart.crosshairs.coords.labels.x'] + ':</span> ' + xCoord + '<br><span style="color: #666">' + prop['chart.crosshairs.coords.labels.y'] + ':</span> ' + yCoord;

                obj.canvas.addEventListener('mouseout', RGraph.HideCrosshairCoords, false);

                obj.canvas.__crosshairs_labels__ = div;
                obj.canvas.__crosshairs_x__ = xCoord;
                obj.canvas.__crosshairs_y__ = yCoord;

            } else if (prop['chart.crosshairs.coords']) {
                alert('[RGRAPH] Showing crosshair coordinates is only supported on the Scatter chart');
            }

            /**
            * Fire the oncrosshairs custom event
            */
            RGraph.FireCustomEvent(obj, 'oncrosshairs');

        } else {
            RGraph.HideCrosshairCoords();
        }
    }

0    /**
    * o------------------------------------------------------------------------------o
    * | This file is part of the RGraph package - you can learn more at:             |
    * |                                                                              |
    * |                          http://www.rgraph.net                               |
    * |                                                                              |
    * | This package is licensed under the RGraph license. For all kinds of business |
    * | purposes there is a small one-time licensing fee to pay and for non          |
    * | commercial  purposes it is free to use. You can read the full license here:  |
    * |                                                                              |
    * |                      http://www.rgraph.net/license                           |
    * o------------------------------------------------------------------------------o
    */

    if (typeof(RGraph) == 'undefined') RGraph = {isRGraph:true,type:'common'};
    
    /**
    * This is used in two functions, hence it's here
    */
    RGraph.Highlight          = {};
    RGraph.tooltips           = {};
    RGraph.tooltips.padding   = '3px';
    RGraph.tooltips.font_face = 'Tahoma';
    RGraph.tooltips.font_size = '10pt';


    /**
    * Shows a tooltip next to the mouse pointer
    * 
    * @param canvas object The canvas element object
    * @param text   string The tooltip text
    * @param int     x      The X position that the tooltip should appear at. Combined with the canvases offsetLeft
    *                       gives the absolute X position
    * @param int     y      The Y position the tooltip should appear at. Combined with the canvases offsetTop
    *                       gives the absolute Y position
    * @param int     idx    The index of the tooltip in the graph objects tooltip array
    * @param object  e      The event object
    */
    RGraph.Tooltip = function (obj, text, x, y, idx, e)
    {
        /**
        * chart.tooltip.override allows you to totally take control of rendering the tooltip yourself
        */
        if (typeof(obj.Get('chart.tooltips.override')) == 'function') {
            return obj.Get('chart.tooltips.override')(obj, text, x, y, idx);
        }

        /**
        * Save the X/Y coords
        */
        var originalX = x;
        var originalY = y;

        /**
        * This facilitates the "id:xxx" format
        */
        text = RGraph.getTooltipTextFromDIV(text);

        /**
        * First clear any exising timers
        */
        var timers = RGraph.Registry.Get('chart.tooltip.timers');

        if (timers && timers.length) {
            for (i=0; i<timers.length; ++i) {
                clearTimeout(timers[i]);
            }
        }
        RGraph.Registry.Set('chart.tooltip.timers', []);

        /**
        * Hide the context menu if it's currently shown
        */
        if (obj.Get('chart.contextmenu')) {
            RGraph.HideContext();
        }

        var effect = obj.Get('chart.tooltips.effect') ? obj.Get('chart.tooltips.effect').toLowerCase() : 'fade';


        /**
        * Show a tool tip
        */
        var tooltipObj  = document.createElement('DIV');
        tooltipObj.className             = obj.Get('chart.tooltips.css.class');
        tooltipObj.style.display         = 'none';
        tooltipObj.style.position        = RGraph.isFixed(obj.canvas) ? 'fixed' : 'absolute';
        tooltipObj.style.left            = 0;
        tooltipObj.style.top             = 0;
        tooltipObj.style.backgroundColor = 'rgb(255,255,239)';
        tooltipObj.style.color           = 'black';
        if (!document.all) tooltipObj.style.border = '';
        tooltipObj.style.visibility      = 'visible';
        tooltipObj.style.paddingLeft     = RGraph.tooltips.padding;
        tooltipObj.style.paddingRight    = RGraph.tooltips.padding;
        tooltipObj.style.fontFamily      = RGraph.tooltips.font_face;
        tooltipObj.style.fontSize        = RGraph.tooltips.font_size;
        tooltipObj.style.zIndex          = 3;

        // Only apply a border if there's content
        if (RGraph.trim(text).length > 0) {
            tooltipObj.style.border             = '1px #bbb solid';
        }

        tooltipObj.style.borderRadius       = '5px';
        tooltipObj.style.MozBorderRadius    = '5px';
        tooltipObj.style.WebkitBorderRadius = '5px';
        tooltipObj.style.WebkitBoxShadow    = 'rgba(96,96,96,0.5) 0 0 15px';
        tooltipObj.style.MozBoxShadow       = 'rgba(96,96,96,0.5) 0 0 15px';
        tooltipObj.style.boxShadow          = 'rgba(96,96,96,0.5) 0 0 15px';
        tooltipObj.style.filter             = 'progid:DXImageTransform.Microsoft.Shadow(color=#666666,direction=135)';
        tooltipObj.style.opacity            = 0;
        //tooltipObj.style.overflow           = 'hidden';
        tooltipObj.innerHTML                = text;
        tooltipObj.__text__                 = text; // This is set because the innerHTML can change when it's set
        tooltipObj.__canvas__               = obj.canvas;
        tooltipObj.style.display            = 'inline';
        tooltipObj.id                       = '__rgraph_tooltip_' + obj.canvas.id + '_' + obj.uid + '_'+ idx;
        tooltipObj.__event__                = obj.Get('chart.tooltips.event') || 'click';
        tooltipObj.__object__               = obj;
        
        if (typeof(idx) == 'number') {
            tooltipObj.__index__ = idx;
            origIdx = idx;
        }
        
        if (obj.type == 'line' || obj.type == 'radar') {
            for (var ds=0; ds<obj.data.length; ++ds) {
                if (idx >= obj.data[ds].length) {
                    idx -= obj.data[ds].length;
                } else {
                    break;
                }
            }
            
            tooltipObj.__dataset__ = ds;
            tooltipObj.__index2__  = idx;
        }

        document.body.appendChild(tooltipObj);

        var width  = tooltipObj.offsetWidth;
        var height = tooltipObj.offsetHeight;


        /**
        * Set the width on the tooltip so it doesn't resize if the window is resized
        */
        tooltipObj.style.width = width + 'px';



        tooltipObj.style.top  = (y - height - 2) + 'px';

        /**
        * If the function exists call the object specific tooltip positioning function
        */
        if (typeof(obj.positionTooltip) == 'function') {
            if (tooltipObj.innerHTML.length > 0) {

                obj.positionTooltip(obj, x, y, tooltipObj, origIdx ? origIdx : idx);

                if (obj.Get('chart.tooltips.coords.page')) {

                    tooltipObj.style.left = e.pageX - (width / 2) - 4.25 + 'px';
                    tooltipObj.style.top = e.pageY - height - 10 + 'px';
                    
                    document.getElementById('__rgraph_tooltip_pointer__').style.left = (parseInt(tooltipObj.offsetWidth) / 2)  - 8.5 + 'px';
                }
            }
        } else {
            tooltipObj.style.left = e.pageX - (width / 2) - 4.25 + 'px';
            tooltipObj.style.top = e.pageY - height - 7 + 'px';
        }


        if (effect == 'fade' || effect == 'expand' || effect == 'contract' || effect == 'snap') {
            setTimeout(function () {tooltipObj.style.opacity = 0.1;}, 25);
            setTimeout(function () {tooltipObj.style.opacity = 0.2;}, 50);
            setTimeout(function () {tooltipObj.style.opacity = 0.3;}, 75);
            setTimeout(function () {tooltipObj.style.opacity = 0.4;}, 100);
            setTimeout(function () {tooltipObj.style.opacity = 0.5;}, 125);
            setTimeout(function () {tooltipObj.style.opacity = 0.6;}, 150);
            setTimeout(function () {tooltipObj.style.opacity = 0.7;}, 175);
            setTimeout(function () {tooltipObj.style.opacity = 0.8;}, 200);
            setTimeout(function () {tooltipObj.style.opacity = 0.9;}, 225);
            
            if (effect == 'expand' || effect == 'contract' || effect == 'snap') {
                console.log('[RGRAPH] The snap, expand and contract tooltip effects are deprecated. Available effects now are fade and none');
            }
        }

        setTimeout("if (RGraph.Registry.Get('chart.tooltip')) { RGraph.Registry.Get('chart.tooltip').style.opacity = 1;}", effect == 'none' ? 50 : 250);

        /**
        * If the tooltip it self is clicked, cancel it
        */
        tooltipObj.onmousedown = function (e){e.stopPropagation();}
        tooltipObj.onmouseup   = function (e){e.stopPropagation();}
        tooltipObj.onclick     = function (e){if (e.button == 0) {e.stopPropagation();}}







        /**
        * Keep a reference to the tooltip in the registry
        */
        RGraph.Registry.Set('chart.tooltip', tooltipObj);

        /**
        * Fire the tooltip event
        */
        RGraph.FireCustomEvent(obj, 'ontooltip');
    }
    
    
    /**
    * 
    */
    RGraph.getTooltipTextFromDIV = function (text)
    {
        // This regex is duplicated firher down on roughly line 888
        var result = /^id:(.*)/.exec(text);

        if (result && result[1] && document.getElementById(result[1])) {
            text = document.getElementById(result[1]).innerHTML;
        } else if (result && result[1]) {
            text = '';
        }
        
        return text;
    }


    /**
    * 
    */
    RGraph.getTooltipWidth = function (text, obj)
    {
        var div = document.createElement('DIV');
            div.className             = obj.Get('chart.tooltips.css.class');
            div.style.paddingLeft     = RGraph.tooltips.padding;
            div.style.paddingRight    = RGraph.tooltips.padding;
            div.style.fontFamily      = RGraph.tooltips.font_face;
            div.style.fontSize        = RGraph.tooltips.font_size;
            div.style.visibility      = 'hidden';
            div.style.position        = 'absolute';
            div.style.top            = '300px';
            div.style.left             = 0;
            div.style.display         = 'inline';
            div.innerHTML             = RGraph.getTooltipTextFromDIV(text);
        document.body.appendChild(div);

        return div.offsetWidth;
    }


    /**
    * Hides the currently shown tooltip
    */
    RGraph.HideTooltip = function ()
    {
        var tooltip = RGraph.Registry.Get('chart.tooltip');
        var uid     = arguments[0] && arguments[0].uid ? arguments[0].uid : null;

        if (tooltip && tooltip.parentNode && (!uid || uid == tooltip.__canvas__.uid)) {
            tooltip.parentNode.removeChild(tooltip);
            tooltip.style.display = 'none';                
            tooltip.style.visibility = 'hidden';
            RGraph.Registry.Set('chart.tooltip', null);
        }
    }

    
    
    /**
    * This installs the window mousedown event listener. It clears any highlight that may
    * be present.
    * 
    * @param object obj The chart object
    *
    RGraph.InstallWindowMousedownTooltipListener = function (obj)
    {
        if (RGraph.Registry.Get('__rgraph_event_listeners__')['window_mousedown']) {
            return;
        }
        
        // When the canvas is cleared, reset this flag so that the event listener is installed again
        RGraph.AddCustomEventListener(obj, 'onclear', function (obj) {RGraph.Registry.Get('__rgraph_event_listeners__')['window_mousedown'] = false;})

        // NOTE: Global on purpose
        rgraph_window_mousedown = function (e)
        {
            if (RGraph.Registry.Get('chart.tooltip')) {

                var obj    = RGraph.Registry.Get('chart.tooltip').__object__;
                var canvas = obj.canvas;

                /**
                * Get rid of the tooltip and redraw all canvases on the page
                *
                RGraph.HideTooltip();
                
                /**
                * No need to clear if highlighting is disabled
                * 
                * TODO Really, need to check ALL of the pertinent objects that
                * are drawing on the canvas using the ObjectRegistry -
                * ie RGraph.ObjectRegistry.getObjectsByCanvasID()
                *
                if (obj.Get('chart.tooltips.highlight')) {
                    RGraph.RedrawCanvas(canvas);
                }
            }
        }
        window.addEventListener('mousedown', rgraph_window_mousedown, false);
        RGraph.AddEventListener('window_' + obj.id, 'mousedown', rgraph_window_mousedown);
    }
    */


    /**
    * This installs the canvas mouseup event listener. This is the function that
    * actually shows the appropriate (if any) tooltip.
    * 
    * @param object obj The chart object
    *
    RGraph.InstallCanvasMouseupTooltipListener = function (obj)
    {
        if (RGraph.Registry.Get('__rgraph_event_listeners__')[obj.canvas.id + '_mouseup']) {
            return;
        }
        RGraph.Registry.Get('__rgraph_event_listeners__')[obj.canvas.id + '_mouseup'] = true;

        // When the canvas is cleared, reset this flag so that the event listener is installed again
        RGraph.AddCustomEventListener(obj, 'onclear', function (obj) {RGraph.Registry.Get('__rgraph_event_listeners__')[obj.canvas.id + '_mouseup'] = false});

        // Install the onclick event handler for the tooltips
        //
        // // NOTE: Global on purpose
        rgraph_canvas_mouseup_func = function (e)
        {
            var x = arguments[1] ? arguments[1] : e.pageX;
            var y = arguments[2] ? arguments[2] : e.pageY;

            var objects = RGraph.ObjectRegistry.getObjectsByCanvasID(e.target.id);

            // It's important to go backwards through the array so that the front charts
            // are checked first, then the charts at the back
            for (var i=(objects.length - 1); i>=0; --i) {
                
                var shape = objects[i].getShape(e);

                if (shape && shape['object'] && !RGraph.Registry.Get('chart.tooltip')) {

                    /**
                    * This allows the Scatter chart funky tooltips style
                    *
                    if (objects[i].type == 'scatter' && shape['dataset'] > 0) {
                        for (var j=0; j<(objects[i].data.length - 1); ++j) {
                            shape['index'] += objects[i].data[j].length;
                        }
                    }

                    var text = RGraph.parseTooltipText(objects[i].Get('chart.tooltips'), shape['index']);
    
                    if (text) {
                    
                        if (shape['object'].Get('chart.tooltips.hotspot.xonly')) {
                            var canvasXY = RGraph.getCanvasXY(objects[i].canvas);
                            x = canvasXY[0] + shape[1];
                            y = canvasXY[1] + shape[2];
                        }

                        RGraph.Tooltip(objects[i], text, x, y, shape['index']);
                        objects[i].Highlight(shape);
    
                        e.stopPropagation();
                        e.cancelBubble = true;
                        return false;
                    }
                }
            }
        }
        obj.canvas.addEventListener('mouseup', rgraph_canvas_mouseup_func, false);
        RGraph.AddEventListener(obj.id, 'mouseup', rgraph_canvas_mouseup_func);
    }
    */



    /**
    * This installs the canvas mousemove event listener. This is the function that
    * changes the mouse pointer if need be.
    * 
    * @param object obj The chart object
    *
    RGraph.InstallCanvasMousemoveTooltipListener = function (obj)
    {
        if (RGraph.Registry.Get('__rgraph_event_listeners__')[obj.canvas.id + '_mousemove']) {
            return;
        }
        RGraph.Registry.Get('__rgraph_event_listeners__')[obj.canvas.id + '_mousemove'] = true;
        
        // When the canvas is cleared, reset this flag so that the event listener is installed again
        RGraph.AddCustomEventListener(obj, 'onclear', function (obj) {RGraph.Registry.Get('__rgraph_event_listeners__')[obj.canvas.id + '_mousemove'] = false})

        // Install the mousemove event handler for the tooltips
        //
        // NOTE: Global on purpose
        rgraph_canvas_mousemove_func = function (e)
        {
            var objects = RGraph.ObjectRegistry.getObjectsByCanvasID(e.target.id);

            for (var i=0; i<objects.length; ++i) {

                var shape = objects[i].getShape(e);

                if (shape && shape['object']) {

                    /**
                    * This allows the Scatter chart funky tooltips style
                    *
                    if (objects[i].type == 'scatter' && shape['dataset'] > 0) {
                        for (var j=0; j<(objects[i].data.length - 1); ++j) {
                            shape['index'] += objects[i].data[j].length;
                        }
                    }

                    var text = RGraph.parseTooltipText(objects[i].Get('chart.tooltips'), shape['index']);


                    if (text) {

                        e.target.style.cursor = 'pointer';

                        /**
                        * This facilitates the event triggering the tooltips being mousemove
                        *

                        if (   typeof(objects[i].Get('chart.tooltips.event')) == 'string'
                            && objects[i].Get('chart.tooltips.event') == 'onmousemove'
                            && (!RGraph.Registry.Get('chart.tooltip') || shape['index'] != RGraph.Registry.Get('chart.tooltip').__index__ || shape['object'].uid != RGraph.Registry.Get('chart.tooltip').__object__.uid)
                           ) {
                           
                           // Hide any current tooltip
                           rgraph_window_mousedown(e);
                           
                           rgraph_canvas_mouseup_func(e);
                        }
                    }
                }
            }
        }
        obj.canvas.addEventListener('mousemove', rgraph_canvas_mousemove_func, false);
        RGraph.AddEventListener(obj.id, 'mousemove', rgraph_canvas_mousemove_func);
    }
    */



    /**
    * This function highlights a rectangle
    * 
    * @param object obj    The chart object
    * @param number shape  The coordinates of the rect to highlight
    */
    RGraph.Highlight.Rect = function (obj, shape)
    {
        if (obj.Get('chart.tooltips.highlight')) {
            
            var canvas  = obj.canvas;
            var context = obj.context;
        
            // Safari seems to need this
            obj.context.lineWidth = 1;

            /**
            * Draw a rectangle on the canvas to highlight the appropriate area
            */
            context.beginPath();

                context.strokeStyle = obj.Get('chart.highlight.stroke');
                context.fillStyle   = obj.Get('chart.highlight.fill');
    
                context.strokeRect(shape['x'],shape['y'],shape['width'],shape['height']);
                context.fillRect(shape['x'],shape['y'],shape['width'],shape['height']);
            context.stroke;
            context.fill();
        }
    }



    /**
    * This function highlights a point
    * 
    * @param object obj    The chart object
    * @param number shape  The coordinates of the rect to highlight
    */
    RGraph.Highlight.Point = function (obj, shape)
    {
        if (obj.Get('chart.tooltips.highlight')) {
            var canvas  = obj.canvas;
            var context = obj.context;
    
            /**
            * Draw a rectangle on the canvas to highlight the appropriate area
            */
            context.beginPath();
                context.strokeStyle = obj.Get('chart.highlight.stroke');
                context.fillStyle   = obj.Get('chart.highlight.fill');
                var radius   = obj.Get('chart.highlight.point.radius') || 2;
                context.arc(shape['x'],shape['y'],radius, 0, TWOPI, 0);
            context.stroke();
            context.fill();
        }
    }



    /**
    * This (as the name suggests preloads any images it can find in the tooltip text
    * 
    * @param object obj The chart object
    */
    RGraph.PreLoadTooltipImages = function (obj)
    {
        var tooltips = obj.Get('chart.tooltips');
        
        if (RGraph.hasTooltips(obj)) {
        
            if (obj.type == 'rscatter') {
                tooltips = [];
                for (var i=0; i<obj.data.length; ++i) {
                    tooltips.push(obj.data[3]);
                }
            }
            
            for (var i=0; i<tooltips.length; ++i) {
                // Add the text to an offscreen DIV tag
                var div = document.createElement('DIV');
                    div.style.position = 'absolute';
                    div.style.opacity = 0;
                    div.style.top = '-100px';
                    div.style.left = '-100px';
                    div.innerHTML  = tooltips[i];
                document.body.appendChild(div);
                
                // Now get the IMG tags and create them
                var img_tags = div.getElementsByTagName('IMG');
    
                // Create the image in an off-screen image tag
                for (var j=0; j<img_tags.length; ++j) {
                        if (img_tags && img_tags[i]) {
                        var img = document.createElement('IMG');
                            img.style.position = 'absolute';
                            img.style.opacity = 0;
                            img.style.top = '-100px';
                            img.style.left = '-100px';
                            img.src = img_tags[i].src
                        document.body.appendChild(img);
                        
                        setTimeout(function () {document.body.removeChild(img);}, 250);
                    }
                }
    
                // Now remove the div
                document.body.removeChild(div);
            }
        }
    }



    /**
    * This is the tooltips canvas onmousemove listener
    */
    RGraph.Tooltips_mousemove  = function (obj, e)
    {
        var shape = obj.getShape(e);
        var changeCursor_tooltips = false

        if (   shape
            && typeof(shape['index']) == 'number'
            && obj.Get('chart.tooltips')[shape['index']]
           ) {

            var text = RGraph.parseTooltipText(obj.Get('chart.tooltips'), shape['index']);

            if (text) {

                /**
                * Change the cursor
                */
                changeCursor_tooltips = true;

                if (obj.Get('chart.tooltips.event') == 'onmousemove') {

                    // Show the tooltip if it's not the same as the one already visible
                    if (
                           !RGraph.Registry.Get('chart.tooltip')
                        || RGraph.Registry.Get('chart.tooltip').__object__.uid != obj.uid
                        || RGraph.Registry.Get('chart.tooltip').__index__ != shape['index']
                       ) {

                        RGraph.HideTooltip();
                        RGraph.Clear(obj.canvas);
                        RGraph.Redraw();
                        RGraph.Tooltip(obj, text, e.pageX, e.pageY, shape['index']);
                        obj.Highlight(shape);
                    }
                }
            }
        
        /**
        * More highlighting
        */
        } else if (shape && typeof(shape['index']) == 'number') {

            var text = RGraph.parseTooltipText(obj.Get('chart.tooltips'), shape['index']);

            if (text) {
                changeCursor_tooltips = true
            }
        }

        return changeCursor_tooltips;
    }

    /**
    * o------------------------------------------------------------------------------o
    * | This file is part of the RGraph package - you can learn more at:             |
    * |                                                                              |
    * |                          http://www.rgraph.net                               |
    * |                                                                              |
    * | This package is licensed under the RGraph license. For all kinds of business |
    * | purposes there is a small one-time licensing fee to pay and for non          |
    * | commercial  purposes it is free to use. You can read the full license here:  |
    * |                                                                              |
    * |                      http://www.rgraph.net/license                           |
    * o------------------------------------------------------------------------------o
    */
    
    /**
    * This is a library of a few functions that make it easier to do
    * effects like fade-ins or eaxpansion.
    */

    /**
    * Initialise the various objects
    */
    if (typeof(RGraph) == 'undefined') RGraph = {isRGraph:true,type:'common'};
    
    RGraph.Effects = {};
    RGraph.Effects.Fade             = {}; RGraph.Effects.jQuery           = {}
    RGraph.Effects.jQuery.HBlinds   = {}; RGraph.Effects.jQuery.VBlinds   = {}
    RGraph.Effects.jQuery.Slide     = {}; RGraph.Effects.Pie              = {}
    RGraph.Effects.Bar              = {}; RGraph.Effects.Line             = {}
    RGraph.Effects.Line.jQuery      = {}; RGraph.Effects.Fuel             = {}
    RGraph.Effects.Rose             = {}; RGraph.Effects.Odo              = {}
    RGraph.Effects.Gauge            = {}; RGraph.Effects.Meter            = {}
    RGraph.Effects.HBar             = {}; RGraph.Effects.HProgress        = {}
    RGraph.Effects.VProgress        = {}; RGraph.Effects.Radar            = {}
    RGraph.Effects.Waterfall        = {}; RGraph.Effects.Gantt            = {}
    RGraph.Effects.Thermometer      = {}; RGraph.Effects.Scatter          = {}
    RGraph.Effects.Scatter.jQuery   = {}; RGraph.Effects.CornerGauge      = {}
    RGraph.Effects.jQuery.HScissors = {}; RGraph.Effects.jQuery.VScissors = {}



    /**
    * Fadein
    * 
    * This function simply uses the CSS opacity property - initially set to zero and
    * increasing to 1 over the period of 0.5 second
    * 
    * @param object obj The graph object
    */
    RGraph.Effects.Fade.In = function (obj)
    {
        var canvas   = obj.canvas;
        var duration = (arguments[1] && arguments[1].duration ? arguments[1].duration : 250);
        var frames   = (arguments[1] && arguments[1].frames ? arguments[1].frames : 5);

        // Initially the opacity should be zero
        canvas.style.opacity = 0;
        
        // Draw the chart
        RGraph.Clear(obj.canvas);
        RGraph.RedrawCanvas(obj.canvas);

        // Now fade the chart in
        for (var i=1; i<=frames; ++i) {
            setTimeout('document.getElementById("' + canvas.id + '").style.opacity = ' + (i * (1 / frames)), i * (duration / frames));
        }
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], duration);
        }
    }


    /**
    * Fadeout
    * 
    * This function is a reversal of the above function - fading out instead of in
    * 
    * @param object obj The graph object
    */
    RGraph.Effects.Fade.Out = function (obj)
    {
        var canvas   = obj.canvas;
        var duration = (arguments[1] && arguments[1].duration ? arguments[1].duration : 250);
        var frames   = (arguments[1] && arguments[1].frames ? arguments[1].frames : 5);
        
        // Draw the chart
        RGraph.Clear(obj.canvas);
        RGraph.RedrawCanvas(obj.canvas);
        
        // Now fade the chart in
        for (var i=frames; i>=0; --i) {
            setTimeout('document.getElementById("' + canvas.id + '").style.opacity = ' + (i * (1 / frames)), (frames - i) * (duration / frames));
        }
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], duration);
        }
    }


    /**
    * Expand
    * 
    * This effect is like the tooltip effect of the same name. I starts in the middle
    * and expands out to full size.
    * 
    * @param object obj The graph object
    */
    RGraph.Effects.jQuery.Expand = function (obj)
    {
        // Check for jQuery
        if (typeof(jQuery) == 'undefined') {
            alert('[ERROR] Could not find jQuery object - have you included the jQuery file?');
        }
        
        var bounce = (!arguments[1] || (arguments[1] && (arguments[1].bounce || typeof(arguments[1].bounce) == 'undefined'))) ? true : false;

        var canvas = obj.canvas;
        
        if (!canvas.__rgraph_div_placeholder__) {
            var div    = RGraph.Effects.ReplaceCanvasWithDIV(canvas);
            canvas.__rgraph_div_placeholder__ = div;
        } else {
            div = canvas.__rgraph_div_placeholder__;
        }

        div.style.position = 'relative';
        canvas.style.position = 'absolute';
        canvas.style.top  = (canvas.height / 2) + 'px';
        canvas.style.left = (canvas.width / 2) + 'px';
        canvas.style.width  = 0;
        canvas.style.height = 0;


        canvas.style.opacity = 0;


        RGraph.Clear(obj.canvas);
        RGraph.RedrawCanvas(obj.canvas);

        if (bounce) {
            jQuery('#' + obj.id).animate({
                opacity: 1,
                width: '120%',
                height: '120%',
                left: (canvas.width * -0.1) + 'px',
                top: (canvas.height * -0.1) + 'px'
            }, 500, function (){
                        jQuery('#' + obj.id).animate({width: '90%', height: '90%', top: (canvas.height * 0.05) + 'px', left: (canvas.width * 0.05) + 'px'}, 250, function ()
                        {
                            jQuery('#' + obj.id).animate({width: '101%', height: '101%', top: (canvas.height * -0.005) + 'px', left: (canvas.width * -0.005) + 'px'}, 250, function ()
                            {
                                jQuery('#' + obj.id).animate({width: '100%', height: '100%', top: 0, left: 0}, 250);
                            });
                        });
                     });
        } else {
            jQuery('#' + obj.id).animate({
                opacity: 1,
                width: '100%',
                height: '100%',
                left: 0,
                top: 0
            }, 1000)
        }
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], 1000);
        }
    }




    /**
    * Contract
    * 
    * This effect is a good one to use with the Expand effect to make a transition
    * 
    * @param object obj The graph object
    * @param null       Not used
    * @param            Optional callback to run when the effect iss done.
    */
    RGraph.Effects.jQuery.Contract = function (obj)
    {
        // Check for jQuery
        if (typeof(jQuery) == 'undefined') {
            alert('[ERROR] Could not find jQuery object - have you included the jQuery file?');
        }
        
        var canvas = obj.canvas;
        
        if (!canvas.__rgraph_div_placeholder__) {
            var div    = RGraph.Effects.ReplaceCanvasWithDIV(canvas);
            canvas.__rgraph_div_placeholder__ = div;
        } else {
            div = canvas.__rgraph_div_placeholder__;
        }

        div.style.position = 'relative';
        canvas.style.position = 'absolute';
        canvas.style.top      = 0;
        canvas.style.left     = 0;


        jQuery('#' + obj.id).animate({
            width: (canvas.width * 1.2) + 'px',
            height: (canvas.height * 1.2) + 'px',
            left: (canvas.width * -0.1) + 'px',
            top: (canvas.height * -0.1) + 'px'
        }, 250, function ()
        {
                jQuery('#' + obj.id).animate({
                    opacity: 0,
                    width: 0,
                    height: 0,
                    left: (canvas.width * 0.5) + 'px',
                    top: (canvas.height * 0.5) + 'px'
                }, 750)
        });
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], 1000);
        }
    }



    /**
    * A function used to replace the canvas witha Div, which inturn holds the canvas. This way the page
    * layout doesn't shift in the canvas is resized.
    * 
    * @param object canvas The canvas to replace.
    */
    RGraph.Effects.ReplaceCanvasWithDIV  = function (canvas)
    {
        if (!canvas.replacementDIV) {
            // Create the place holder DIV
            var div = document.createElement('DIV');
                div.style.width = canvas.width + 'px';
                div.style.height = canvas.height + 'px';
                div.style.cssFloat = canvas.style.cssFloat;
                div.style.left = canvas.style.left;
                div.style.top = canvas.style.top;
                //div.style.position = canvas.style.position;
                div.style.display = 'inline-block';
            canvas.parentNode.insertBefore(div, canvas);
            
    
            // Remove the canvas from the document
            canvas.parentNode.removeChild(canvas);
            
            // Add it back in as a child of the place holder
            div.appendChild(canvas);
            
            // Reset the positioning information on the canvas
            canvas.style.position = 'relative';
            canvas.style.left = (div.offsetWidth / 2) + 'px';
            canvas.style.top = (div.offsetHeight / 2) + 'px';
            canvas.style.cssFloat = '';
        
            // Add a reference to the canvas to the DIV so that repeated plays of the anumation
            // don't keep replacing the canvas with a new DIV
            canvas.replacementDIV = div;

        } else {
            var div = canvas.replacementDIV;
        }
        
        return div;
    }


    /**
    * Snap
    * 
    * Similar to the tooltip effect of the same name, this moves the canvas in from the top left corner
    * 
    * @param object obj The graph object
    */
    RGraph.Effects.jQuery.Snap = function (obj)
    {
        var delay = 500;

        var div = RGraph.Effects.ReplaceCanvasWithDIV(obj.canvas);
        
        obj.canvas.style.position = 'absolute';
        obj.canvas.style.top = 0;
        obj.canvas.style.left = 0;
        obj.canvas.style.width = 0;
        obj.canvas.style.height = 0;
        obj.canvas.style.opacity = 0;
        
        var targetLeft   = div.offsetLeft;
        var targetTop    = div.offsetTop;
        var targetWidth  = div.offsetWidth;
        var targetHeight = div.offsetHeight;

        RGraph.Clear(obj.canvas);
        RGraph.RedrawCanvas(obj.canvas);

        jQuery('#' + obj.id).animate({
            opacity: 1,
            width: targetWidth + 'px',
            height: targetHeight + 'px',
            left: targetLeft + 'px',
            top: targetTop + 'px'
        }, delay);
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], delay + 50);
        }
    }



    /**
    * Reveal
    * 
    * This effect issmilat to the Expand effect - the canvas is slowly revealed from
    * the centre outwards
    * 
    * @param object obj The chart object
    */
    RGraph.Effects.jQuery.Reveal = function (obj)
    {
        var opts   = arguments[1] ? arguments[1] : null;
        var delay  = 1000;
        var canvas = obj.canvas;
        var xy     = RGraph.getCanvasXY(obj.canvas);


        /**
        * Hide the canvas and draw it
        */
        obj.canvas.style.visibility = 'hidden';
        RGraph.Clear(obj.canvas);
        RGraph.RedrawCanvas(obj.canvas);


        var divs = [
                    ['reveal_left', xy[0], xy[1], obj.canvas.width  / 2, obj.canvas.height],
                    ['reveal_right',(xy[0] + (obj.canvas.width  / 2)),xy[1],(obj.canvas.width  / 2),obj.canvas.height],
                    ['reveal_top',xy[0],xy[1],obj.canvas.width,(obj.canvas.height / 2)],
                    ['reveal_bottom',xy[0],(xy[1] + (obj.canvas.height  / 2)),obj.canvas.width,(obj.canvas.height / 2)]
                   ];
        
        for (var i=0; i<divs.length; ++i) {
            var div = document.createElement('DIV');
                div.id = divs[i][0];
                div.style.width =  divs[i][3]+ 'px';
                div.style.height = divs[i][4] + 'px';
                div.style.left   = divs[i][1] + 'px';
                div.style.top   = divs[i][2] + 'px';
                div.style.position = 'absolute';
                div.style.backgroundColor = opts && typeof(opts['color']) == 'string' ? opts['color'] : 'white';
            document.body.appendChild(div);
        }
        
        /**
        * Now the covering DIVs are in place show the canvas again
        */
        obj.canvas.style.visibility = 'visible';


        jQuery('#reveal_left').animate({width: 0}, delay);
        jQuery('#reveal_right').animate({left: '+=' + (obj.canvas.width / 2),width: 0}, delay);
        jQuery('#reveal_top').animate({height: 0}, delay);
        jQuery('#reveal_bottom').animate({top: '+=' + (obj.canvas.height / 2),height: 0}, delay);
        
        // Remove the DIVs from the DOM 100ms after the animation ends
        setTimeout(
            function ()
            {
                document.body.removeChild(document.getElementById("reveal_top"))
                document.body.removeChild(document.getElementById("reveal_bottom"))
                document.body.removeChild(document.getElementById("reveal_left"))
                document.body.removeChild(document.getElementById("reveal_right"))
            }
            , delay);
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], delay);
        }
    }



    /**
    * RevealCircular
    * 
    * This effect is smilar to the Reveal effect - the canvas is slowly revealed from
    * the centre outwards using a circular shape
    * 
    * @param object   obj The chart object
    * @param object       An object of options
    * @param function     An optional callback function that runs when the effect is finished
    */
    RGraph.Effects.RevealCircular = function (obj)
    {
        var opts      = arguments[1] ? arguments[1] : null;
        var callback  = arguments[2] ? arguments[2] : null;
        var frames    = 30;
        var RG        = RGraph;
        var ca        = obj.canvas;
        var co        = obj.context;
        var ra        = 0; // The initial radius of the circle that is clipped to
        var cx        = ca.width / 2;
        var cy        = ca.height / 2;
        var target_ra = Math.max(ca.height, ca.width) / 2;
        
        // This is the iterator function which gradually increases the radius of the clip circle
        function Grow ()
        {
            // Begin by clearing the canvas
            RG.Clear(ca);

            co.save();
                // First draw the circle and clip to it
                co.beginPath();
                co.arc(cx, cy, ra, 0, TWOPI, false);
                co.clip();
                
                // Now draw the chart
                obj.Draw();
            co.restore();


            // Increment the radius
            if (ra < target_ra) {
                ra += target_ra / 30;
                RG.Effects.UpdateCanvas(Grow);
            
            } else if (typeof(callback) == 'function') {
                callback(obj);
            }
        }
        
        Grow();
    }



    /**
    * Conceal
    * 
    * This effect is the reverse of the Reveal effect - instead of revealing the canvas it
    * conceals it. Combined with the reveal effect would make for a nice wipe effect.
    * 
    * @param object obj The chart object
    */
    RGraph.Effects.jQuery.Conceal = function (obj)
    {
        var opts   = arguments[1] ? arguments[1] : null;
        var delay  = 1000;
        var canvas = obj.canvas;
        var xy     = RGraph.getCanvasXY(obj.canvas);


        var divs = [
                    ['conceal_left', xy[0], xy[1], 0, obj.canvas.height],
                    ['conceal_right',(xy[0] + obj.canvas.width),xy[1],0,obj.canvas.height],
                    ['conceal_top',xy[0],xy[1],obj.canvas.width,0],
                    ['conceal_bottom',xy[0],(xy[1] + obj.canvas.height),obj.canvas.width,0]
                   ];
        
        for (var i=0; i<divs.length; ++i) {
            var div = document.createElement('DIV');
                div.id = divs[i][0];
                div.style.width =  divs[i][3]+ 'px';
                div.style.height = divs[i][4] + 'px';
                div.style.left   = divs[i][1] + 'px';
                div.style.top   = divs[i][2] + 'px';
                div.style.position = 'absolute';
                div.style.backgroundColor = opts && typeof(opts['color']) == 'string' ? opts['color'] : 'white';
            document.body.appendChild(div);
        }


        jQuery('#conceal_left').animate({width: '+=' + (obj.canvas.width / 2)}, delay);
        jQuery('#conceal_right').animate({left: '-=' + (obj.canvas.width / 2),width: (obj.canvas.width / 2)}, delay);
        jQuery('#conceal_top').animate({height: '+=' + (obj.canvas.height / 2)}, delay);
        jQuery('#conceal_bottom').animate({top: '-=' + (obj.canvas.height / 2),height: (obj.canvas.height / 2)}, delay);
        
        // Remove the DIVs from the DOM 100ms after the animation ends
        setTimeout(
            function ()
            {
                document.body.removeChild(document.getElementById("conceal_top"))
                document.body.removeChild(document.getElementById("conceal_bottom"))
                document.body.removeChild(document.getElementById("conceal_left"))
                document.body.removeChild(document.getElementById("conceal_right"))
            }
            , delay);
            
        setTimeout(function () {RGraph.Clear(obj.canvas);}, delay);
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], delay);
        }
    }


    /**
    * Horizontal Blinds (open)
    * 
    * @params object obj The graph object
    */
    RGraph.Effects.jQuery.HBlinds.Open = function (obj)
    {
        var canvas  = obj.canvas;
        var opts   = arguments[1] ? arguments[1] : [];
        var delay  = 1000;
        var color  = opts['color'] ? opts['color'] : 'white';
        var xy     = RGraph.getCanvasXY(canvas);
        var height = canvas.height / 5;
        
        /**
        * First draw the chart
        */
        RGraph.Clear(obj.canvas);
        RGraph.RedrawCanvas(obj.canvas);

        for (var i=0; i<5; ++i) {
            var div = document.createElement('DIV');
                div.id = 'blinds_' + i;
                div.style.width =  canvas.width + 'px';
                div.style.height = height + 'px';
                div.style.left   = xy[0] + 'px';
                div.style.top   = (xy[1] + (canvas.height * (i / 5))) + 'px';
                div.style.position = 'absolute';
                div.style.backgroundColor = color;
            document.body.appendChild(div);

            jQuery('#blinds_' + i).animate({height: 0}, delay);
        }

        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_0'));}, delay);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_1'));}, delay);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_2'));}, delay);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_3'));}, delay);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_4'));}, delay);
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], delay);
        }
    }


    /**
    * Horizontal Blinds (close)
    * 
    * @params object obj The graph object
    */
    RGraph.Effects.jQuery.HBlinds.Close = function (obj)
    {
        var canvas  = obj.canvas;
        var opts   = arguments[1] ? arguments[1] : [];
        var delay  = 1000;
        var color  = opts['color'] ? opts['color'] : 'white';
        var xy     = RGraph.getCanvasXY(canvas);
        var height = canvas.height / 5;

        for (var i=0; i<5; ++i) {
            var div = document.createElement('DIV');
                div.id = 'blinds_' + i;
                div.style.width =  canvas.width + 'px';
                div.style.height = 0;
                div.style.left   = xy[0] + 'px';
                div.style.top   = (xy[1] + (canvas.height * (i / 5))) + 'px';
                div.style.position = 'absolute';
                div.style.backgroundColor = color;
            document.body.appendChild(div);

            jQuery('#blinds_' + i).animate({height: height + 'px'}, delay);
        }
        
        setTimeout(function () {RGraph.Clear(obj.canvas);}, delay + 100);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_0'));}, delay + 100);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_1'));}, delay + 100);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_2'));}, delay + 100);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_3'));}, delay + 100);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_4'));}, delay + 100);
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], delay);
        }
    }


    /**
    * Vertical Blinds (open)
    * 
    * @params object obj The graph object
    */
    RGraph.Effects.jQuery.VBlinds.Open = function (obj)
    {
        var canvas  = obj.canvas;
        var opts   = arguments[1] ? arguments[1] : [];
        var delay  = 1000;
        var color  = opts['color'] ? opts['color'] : 'white';
        var xy     = RGraph.getCanvasXY(canvas);
        var width  = canvas.width / 10;
        
        /**
        * First draw the chart
        */
        RGraph.Clear(obj.canvas);
        RGraph.RedrawCanvas(obj.canvas);

        for (var i=0; i<10; ++i) {
            var div = document.createElement('DIV');
                div.id = 'blinds_' + i;
                div.style.width =  width + 'px';
                div.style.height = canvas.height + 'px';
                div.style.left   = (xy[0] + (canvas.width * (i / 10))) + 'px';
                div.style.top   = (xy[1]) + 'px';
                div.style.position = 'absolute';
                div.style.backgroundColor = color;
            document.body.appendChild(div);

            jQuery('#blinds_' + i).animate({width: 0}, delay);
        }

        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_0'));}, delay + 100);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_1'));}, delay + 100);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_2'));}, delay + 100);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_3'));}, delay + 100);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_4'));}, delay + 100);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_5'));}, delay + 100);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_6'));}, delay + 100);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_7'));}, delay + 100);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_8'));}, delay + 100);
        setTimeout(function () {document.body.removeChild(document.getElementById('blinds_9'));}, delay + 100);
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], delay);
        }
    }


    /**
    * Vertical Blinds (close)
    * 
    * @params object obj The graph object
    */
    RGraph.Effects.jQuery.VBlinds.Close = function (obj)
    {
        var canvas  = obj.canvas;
        var opts   = arguments[1] ? arguments[1] : [];
        var delay  = 1000;
        var color  = opts['color'] ? opts['color'] : 'white';
        var xy     = RGraph.getCanvasXY(canvas);
        var width  = canvas.width / 10;
        
        // Don't draw the chart

        for (var i=0; i<10; ++i) {
            var div = document.createElement('DIV');
                div.id = 'blinds_' + i;
                div.style.width =  0;
                div.style.height = canvas.height + 'px';
                div.style.left   = (xy[0] + (canvas.width * (i / 10))) + 'px';
                div.style.top   = (xy[1]) + 'px';
                div.style.position = 'absolute';
                div.style.backgroundColor = color;
            document.body.appendChild(div);

            jQuery('#blinds_' + i).animate({width: width}, delay);
        }

        setTimeout(function () {RGraph.Clear(obj.canvas, color);}, delay + 100);

        if (opts['remove']) {
            setTimeout(function () {document.body.removeChild(document.getElementById('blinds_0'));}, delay + 100);
            setTimeout(function () {document.body.removeChild(document.getElementById('blinds_1'));}, delay + 100);
            setTimeout(function () {document.body.removeChild(document.getElementById('blinds_2'));}, delay + 100);
            setTimeout(function () {document.body.removeChild(document.getElementById('blinds_3'));}, delay + 100);
            setTimeout(function () {document.body.removeChild(document.getElementById('blinds_4'));}, delay + 100);
            setTimeout(function () {document.body.removeChild(document.getElementById('blinds_5'));}, delay + 100);
            setTimeout(function () {document.body.removeChild(document.getElementById('blinds_6'));}, delay + 100);
            setTimeout(function () {document.body.removeChild(document.getElementById('blinds_7'));}, delay + 100);
            setTimeout(function () {document.body.removeChild(document.getElementById('blinds_8'));}, delay + 100);
            setTimeout(function () {document.body.removeChild(document.getElementById('blinds_9'));}, delay + 100);
        }
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], delay);
        }
    }


    /**
    * Pie chart grow
    * 
    * Gradually increases the pie chart radius
    * 
    * @params object obj The graph object
    */
    RGraph.Effects.Pie.Grow = function (obj)
    {
        var canvas  = obj.canvas;
        var opts   = arguments[1] ? arguments[1] : [];
        var color  = opts['color'] ? opts['color'] : 'white';
        var xy     = RGraph.getCanvasXY(canvas);
        
        canvas.style.visibility = 'hidden';
        RGraph.RedrawCanvas(canvas);

        var radius = obj.getRadius();
        
        if (typeof(obj.Get('chart.radius')) == 'number') {
            radius = obj.Get('chart.radius');
        }
        
        //RGraph.Clear(obj.canvas);
        canvas.style.visibility = 'visible';

        obj.Set('chart.radius', 0);

        RGraph.Effects.Animate(obj, {'chart.radius': radius}, arguments[2]);
    }


    /**
    * Grow
    * 
    * The Bar chart Grow effect gradually increases the values of the bars
    * 
    * @param object   obj The graph object
    * @param object       An array of options
    * @param function     A function to call when the effect is complete
    */
    RGraph.Effects.Bar.Grow = function (obj)
    {
        // Callback
        var callback = arguments[2];

        // Save the data
        obj.original_data = RGraph.array_clone(obj.data);
        
        // Zero the data
        obj.__animation_frame__ = 0;

        // Stop the scale from changing by setting chart.ymax (if it's not already set)
        if (obj.Get('chart.ymax') == null) {

            var ymax = 0;

            for (var i=0; i<obj.data.length; ++i) {
                if (RGraph.is_array(obj.data[i]) && obj.Get('chart.grouping') == 'stacked') {
                    ymax = Math.max(ymax, Math.abs(RGraph.array_sum(obj.data[i])));

                } else if (RGraph.is_array(obj.data[i]) && obj.Get('chart.grouping') == 'grouped') {
                    ymax = Math.max(ymax, Math.abs(RGraph.array_max(obj.data[i])));
                } else {
                    ymax = Math.max(ymax, Math.abs(obj.data[i]));
                }
            }

            var scale = RGraph.getScale2(obj, {'max':ymax});
            obj.Set('chart.ymax', scale.max);
        }

        function Grow ()
        {
            var numFrames = 30;

            if (!obj.__animation_frame__) {
                obj.__animation_frame__  = 0;
                obj.__original_hmargin__ = obj.Get('chart.hmargin');
                obj.__hmargin__          = ((obj.canvas.width - obj.Get('chart.gutter.left') - obj.Get('chart.gutter.right')) / obj.data.length) / 2;
                obj.Set('chart.hmargin', obj.__hmargin__);
            }

            // Alter the Bar chart data depending on the frame
            for (var j=0; j<obj.original_data.length; ++j) {
                if (typeof(obj.data[j]) == 'object') {
                    for (var k=0; k<obj.data[j].length; ++k) {
                        obj.data[j][k] = (obj.__animation_frame__ / numFrames) * obj.original_data[j][k];
                    }
                } else {
                    obj.data[j] = (obj.__animation_frame__ / numFrames) * obj.original_data[j];
                }
            }

            /**
            * Increment the hmargin to the target
            */
            obj.Set('chart.hmargin', ((1 - (obj.__animation_frame__ / numFrames)) * (obj.__hmargin__ - obj.__original_hmargin__)) + obj.__original_hmargin__);


            RGraph.Clear(obj.canvas);
            RGraph.RedrawCanvas(obj.canvas);

            if (obj.__animation_frame__ < numFrames) {
                obj.__animation_frame__ += 1;

                RGraph.Effects.UpdateCanvas(Grow);
            // Call the callback function if it's defined
            } else {
                if (callback) {
                    callback(obj);
                }
            }
        }
        
        RGraph.Effects.UpdateCanvas(Grow);
    }


    /**
    * A wrapper function that encapsulate requestAnimationFrame
    * 
    * @param function func The animation function
    */
    RGraph.Effects.UpdateCanvas = function (func)
    {
        window.requestAnimationFrame =    window.requestAnimationFrame
                                       || window.webkitRequestAnimationFrame
                                       || window.msRequestAnimationFrame
                                       || window.amozRequestAnimationFrame
                                       || (function (func){setTimeout(func, 16.666);});
        
        window.requestAnimationFrame(func);
    }


    /**
    * Grow
    * 
    * The Fuel chart Grow effect gradually increases the values of the Fuel chart
    * 
    * @param object obj The graph object
    */
    RGraph.Effects.Fuel.Grow = function (obj)
    {
        var callback  = arguments[2];
        var numFrames = 30;
        var frame     = 0;
        var origValue = Number(obj.currentValue);
        
        if (obj.currentValue == null) {
            obj.currentValue = obj.min;
            origValue = obj.min;
        }

        var newValue  = obj.value;
        var diff      = newValue - origValue;
        var step      = (diff / numFrames);
        var frame     = 0;


        function Grow ()
        {
            frame++;

            obj.value = ((frame / numFrames) * diff) + origValue

            if (obj.value > obj.max) obj.value = obj.max;
            if (obj.value < obj.min) obj.value = obj.min;

            RGraph.Clear(obj.canvas);
            RGraph.RedrawCanvas(obj.canvas);

            if (frame < numFrames) {
                RGraph.Effects.UpdateCanvas(Grow);
            } else if (typeof(callback) == 'function') {
                callback(obj);
            }
        }

        Grow();
    }


    /**
    * The Animate function. Similar to the jQuery Animate() function - simply pass it a
    * map of the properties and their target values, and this function will animate
    * them to get to those values.
    * 
    * @param object obj The chart object
    * @param object map A map (an associative array) of the properties and their target values.
    * @param            An optional function which will be called when the animation is complete
    */
    RGraph.Effects.Animate = function (obj, map)
    {
        RGraph.RedrawCanvas(obj.canvas);

        RGraph.Effects.__total_frames__  = (map && map['frames']) ? map['frames'] : 30;

        function Animate_Iterator (func)
        {
            var id = [obj.id +  '_' + obj.type];

            // Very first time in - initialise the arrays
            if (typeof(RGraph.Effects.__current_frame__ ) == 'undefined') {
                RGraph.Effects.__current_frame__   = new Array();
                RGraph.Effects.__original_values__ = new Array();
                RGraph.Effects.__diffs__           = new Array();
                RGraph.Effects.__steps__           = new Array();
                RGraph.Effects.__callback__        = new Array();
            }

            // Initialise the arrays for THIS animation (not necessrily the first in the page)
            if (!RGraph.Effects.__current_frame__[id]) {
                RGraph.Effects.__current_frame__[id] = RGraph.Effects.__total_frames__;
                RGraph.Effects.__original_values__[id] = {};
                RGraph.Effects.__diffs__[id]           = {};
                RGraph.Effects.__steps__[id]           = {};
                RGraph.Effects.__callback__[id]        = func;
            }

            for (var i in map) {
                if (typeof(map[i]) == 'string' || typeof(map[i]) == 'number') {

                    // If this the first frame, record the proginal value
                    if (RGraph.Effects.__current_frame__[id] == RGraph.Effects.__total_frames__) {
                        RGraph.Effects.__original_values__[id][i] = obj.Get(i);
                        RGraph.Effects.__diffs__[id][i]           = map[i] - RGraph.Effects.__original_values__[id][i];
                        RGraph.Effects.__steps__[id][i]           = RGraph.Effects.__diffs__[id][i] / RGraph.Effects.__total_frames__;
                    }

                    obj.Set(i, obj.Get(i) + RGraph.Effects.__steps__[id][i]);

                    RGraph.RedrawCanvas(obj.canvas);
                }
            }

            // If the current frame number is above zero, run the animation iterator again
            if (--RGraph.Effects.__current_frame__[id] > 0) {
                //setTimeout(Animate_Iterator, 100)
                RGraph.Effects.UpdateCanvas(Animate_Iterator);
            
            // Optional callback
            } else {

                if (typeof(RGraph.Effects.__callback__[id]) == 'function') {
                    (RGraph.Effects.__callback__[id])(obj);
                }
                
                // Get rid of the arrays
                RGraph.Effects.__current_frame__[id]   = null;
                RGraph.Effects.__original_values__[id] = null;
                RGraph.Effects.__diffs__[id]           = null;
                RGraph.Effects.__steps__[id]           = null;
                RGraph.Effects.__callback__[id]        = null;

            }
        }

        Animate_Iterator(arguments[2]);
    }


    /**
    * Slide in
    * 
    * This function is a wipe that can be used when switching the canvas to a new graph
    * 
    * @param object obj The graph object
    */
    RGraph.Effects.jQuery.Slide.In = function (obj)
    {
        RGraph.Clear(obj.canvas);
        RGraph.RedrawCanvas(obj.canvas);

        var canvas = obj.canvas;
        var div    = RGraph.Effects.ReplaceCanvasWithDIV(obj.canvas);
        var delay = 1000;
        div.style.overflow= 'hidden';
        var from = typeof(arguments[1]) == 'object' && typeof(arguments[1]['from']) == 'string' ? arguments[1]['from'] : 'left';
        
        canvas.style.position = 'relative';
        
        if (from == 'left') {
            canvas.style.left = (0 - div.offsetWidth) + 'px';
            canvas.style.top  = 0;
        } else if (from == 'top') {
            canvas.style.left = 0;
            canvas.style.top  = (0 - div.offsetHeight) + 'px';
        } else if (from == 'bottom') {
            canvas.style.left = 0;
            canvas.style.top  = div.offsetHeight + 'px';
        } else {
            canvas.style.left = div.offsetWidth + 'px';
            canvas.style.top  = 0;
        }
        
        jQuery('#' + obj.id).animate({left:0,top:0}, delay);
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], delay);
        }
    }


    /**
    * Slide out
    * 
    * This function is a wipe that can be used when switching the canvas to a new graph
    * 
    * @param object obj The graph object
    */
    RGraph.Effects.jQuery.Slide.Out = function (obj)
    {
        var canvas = obj.canvas;
        var div    = RGraph.Effects.ReplaceCanvasWithDIV(obj.canvas);
        var delay = 1000;
        div.style.overflow= 'hidden';
        var to = typeof(arguments[1]) == 'object' && arguments[1] && typeof(arguments[1]['to']) == 'string' ? arguments[1]['to'] : 'left';
        
        canvas.style.position = 'relative';
        canvas.style.left = 0;
        canvas.style.top  = 0;
        
        if (to == 'left') {
            jQuery('#' + obj.id).animate({left: (0 - canvas.width) + 'px'}, delay);
        } else if (to == 'top') {
            jQuery('#' + obj.id).animate({left: 0, top: (0 - div.offsetHeight) + 'px'}, delay);
        } else if (to == 'bottom') {
            jQuery('#' + obj.id).animate({top: (0 + div.offsetHeight) + 'px'}, delay);
        } else {
            jQuery('#' + obj.id).animate({left: (0 + canvas.width) + 'px'}, delay);
        }
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], delay);
        }
    }


    /**
    * Unfold
    * 
    * This effect gradually increases the X/Y coordinatesfrom 0
    * 
    * @param object obj The chart object
    */
    RGraph.Effects.Line.Unfold = function (obj)
    {
        obj.Set('chart.animation.factor', obj.Get('chart.animation.unfold.initial'));
        RGraph.Effects.Animate(obj, {'chart.animation.factor': 1}, arguments[2]);
    }


    /**
    * RoundRobin
    * 
    * This effect is similar to the Pie chart RoundRobin effect
    * 
    * @param object   obj The chart object
    * @param              Not used - pass null
    * @param function     An optional callback function
    */
    RGraph.Effects.Rose.RoundRobin = function (obj)
    {
        var numFrames       = 60;
        var currentFrame    = 0;
        var original_margin = obj.Get('chart.margin');
        var margin          = (360 / obj.data.length) / 2;
        var callback        = arguments[2];

        obj.Set('chart.margin', margin);
        obj.Set('chart.animation.roundrobin.factor', 0);

        //RGraph.Effects.Animate(obj, {'chart.margin': original_margin, 'chart.animation.grow.factor': 1, 'frames': 45}, arguments[2]);
        function RoundRobin_inner ()
        {
            if (currentFrame++ < numFrames) {
                obj.Set('chart.animation.roundrobin.factor', currentFrame / numFrames);
                obj.Set('chart.margin', (currentFrame / numFrames) * original_margin);
                RGraph.Clear(obj.canvas);
                RGraph.RedrawCanvas(obj.canvas);
                
                RGraph.Effects.UpdateCanvas(RoundRobin_inner);

            } else {
                obj.Set('chart.animation.roundrobin.factor', 1);
                obj.Set('chart.margin', original_margin);
                RGraph.Clear(obj.canvas);
                RGraph.RedrawCanvas(obj.canvas);
                
                if (typeof(callback) == 'function') {
                    callback(obj);
                }
            }
        }
        
        RGraph.Effects.UpdateCanvas(RoundRobin_inner);
    }


    /**
    * UnfoldFromCenter
    * 
    * Line chart  unfold from center
    */
    RGraph.Effects.Line.UnfoldFromCenter = function (obj)
    {
        var numFrames = 30;

        var original_opacity = obj.canvas.style.opacity;
        obj.canvas.style.opacity = 0;
        
        obj.Draw();
        RGraph.RedrawCanvas(obj.canvas);

        var center_value = obj.Get('chart.xaxispos') == 'center' ? obj.Get('chart.ymin') : ((obj.max - obj.min) / 2) + obj.min;
        obj.Set('chart.ymax', obj.scale2.max);

        RGraph.Clear(obj.canvas);

        obj.canvas.style.opacity = original_opacity;
        var original_data = RGraph.array_clone(obj.original_data);
        var callback = arguments[2];

        if (!obj.__increments__) {
        
            obj.__increments__ = new Array();
        
            for (var dataset=0; dataset<original_data.length; ++dataset) {

                obj.__increments__[dataset] = new Array();

                for (var i=0; i<original_data[dataset].length; ++i) {
                    if (obj.Get('chart.filled') && obj.Get('chart.filled.accumulative') && dataset > 0) {
                        obj.__increments__[dataset][i] = original_data[dataset][i] / numFrames;
                        obj.original_data[dataset][i] = 0;
                    } else {
                        obj.__increments__[dataset][i] = (original_data[dataset][i] - center_value) / numFrames;
                        obj.original_data[dataset][i] = center_value;
                    }
                }
            }
        }

        function UnfoldFromCenter ()
        {
            RGraph.Clear(obj.canvas);
            RGraph.RedrawCanvas(obj.canvas);
        
            for (var dataset=0; dataset<original_data.length; ++dataset) {
                for (var i=0; i<original_data[dataset].length; ++i) {
                    obj.original_data[dataset][i] += obj.__increments__[dataset][i];
                }
            }

            if (--numFrames > 0) {
                RGraph.Effects.UpdateCanvas(UnfoldFromCenter);
            } else {
                obj.original_data = RGraph.array_clone(original_data);
                obj.__increments__ = null;
                RGraph.Clear(obj.canvas);
                RGraph.RedrawCanvas(obj.canvas);
                
                if (typeof(callback) == 'function') {
                    callback(obj);
                }
            }
        }
        
        UnfoldFromCenter();
    }



    /**
    * UnfoldFromCenterTrace
    */
    RGraph.Effects.Line.jQuery.UnfoldFromCenterTrace = function  (obj)
    {
        // Hide the canvas first
        obj.canvas.style.visibility = 'hidden';
        setTimeout(function () {obj.canvas.style.visibility = 'visible';}, 10);

        /**
        * First draw the chart so we can get the max
        */
        obj.Draw();
        RGraph.Clear(obj.canvas);


        var data = RGraph.array_clone(obj.original_data);
        var callback = arguments[2];

        /**
        * When the Trace function finishes it calls this function
        */
        function Unfold_callback ()
        {
            obj.original_data = data;
            RGraph.Effects.Line.UnfoldFromCenter(obj, null, callback);
        }

        /**
        * Determine the mid-point
        */
        var half = obj.Get('chart.xaxispos') == 'center' ? obj.min : ((obj.max - obj.min) / 2) + obj.min;
        obj.Set('chart.ymax', obj.max);

        for (var i=0; i<obj.original_data.length; ++i) {
            for (var j=0; j<obj.original_data[i].length; ++j) {
                obj.original_data[i][j] = (obj.Get('chart.filled') && obj.Get('chart.filled.accumulative') && i > 0) ? 0 : half;
            }
        }

        //RGraph.Clear(obj.canvas);
        RGraph.Effects.Line.jQuery.Trace(obj, {'duration':1000}, Unfold_callback);
    }



    /**
    * FoldToCenter
    * 
    * Line chart  FoldTocenter
    */
    RGraph.Effects.Line.FoldToCenter = function (obj)
    {
        var totalFrames = 30;
        var numFrame    = totalFrames;
        RGraph.RedrawCanvas(obj.canvas);
        var center_value = obj.scale2.max / 2;
        obj.Set('chart.ymax', obj.scale2.max);
        RGraph.Clear(obj.canvas);
        var original_data = RGraph.array_clone(obj.original_data);
        obj.Set('chart.shadow.blur', 0);
        var callback = arguments[2];
        
        function FoldToCenter ()
        {
            for (var i=0; i<obj.data.length; ++i) {
                if (obj.data[i].length) {
                    for (var j=0; j<obj.data[i].length; ++j) {
                        if (obj.original_data[i][j] > center_value) {
                            obj.original_data[i][j] = ((original_data[i][j] - center_value) * (numFrame/totalFrames)) + center_value;
                        } else {
                            obj.original_data[i][j] = center_value - ((center_value - original_data[i][j]) * (numFrame/totalFrames));
                        }
                    }
                }
            }
            
            RGraph.Clear(obj.canvas);
            RGraph.RedrawCanvas(obj.canvas)

            if (numFrame-- > 0) {
                RGraph.Effects.UpdateCanvas(FoldToCenter);
            } else if (typeof(callback) == 'function') {
                callback(obj);
            }
        }

        RGraph.Effects.UpdateCanvas(FoldToCenter);
    }


    /**
    * Odo Grow
    * 
    * This effect gradually increases the represented value
    * 
    * @param object   obj The chart object
    * @param              Not used - pass null
    * @param function     An optional callback function
    */
    RGraph.Effects.Odo.Grow = function (obj)
    {
        var numFrames = 30;
        var curFrame  = 0;
        var origValue = Number(obj.currentValue);
        var newValue  = obj.value;
        var diff      = newValue - origValue;
        var step      = (diff / numFrames);
        var callback  = arguments[2];

        function Grow_inner ()
        {
            obj.value = origValue + (curFrame * step);

            RGraph.Clear(obj.canvas);
            RGraph.RedrawCanvas(obj.canvas);

            if (++curFrame <= numFrames) {
                RGraph.Effects.UpdateCanvas(Grow_inner);
            } else if (callback) {
                callback(obj);
            }
        }
        
        //setTimeout(Grow, 100);
        Grow_inner();
    }


    /**
    * Meter Grow
    * 
    * This effect gradually increases the represented value
    * 
    * @param object   obj The chart object
    * @param              Not used - pass null
    * @param function     An optional callback function
    */
    RGraph.Effects.Meter.Grow = function (obj)
    {
        if (!obj.currentValue) {
            obj.currentValue = obj.min;
        }

        var totalFrames = 60;
        var numFrame    = 0;
        var diff        = obj.value - obj.currentValue;
        var step        = diff / totalFrames
        var callback    = arguments[2];
        var initial     = obj.currentValue;

        function Grow_meter_inner ()
        {
            obj.value = initial + (numFrame++ * step);

            RGraph.Clear(obj.canvas);
            RGraph.RedrawCanvas(obj.canvas);
        
            if (numFrame++ <= totalFrames) {
                RGraph.Effects.UpdateCanvas(Grow_meter_inner);
            } else if (typeof(callback) == 'function') {
                callback(obj);
            }
        }
        
        Grow_meter_inner();
    }


    /**
    * Grow
    * 
    * The HBar chart Grow effect gradually increases the values of the bars
    * 
    * @param object obj The graph object
    */
    RGraph.Effects.HBar.Grow = function (obj)
    {
        // Save the data
        obj.original_data = RGraph.array_clone(obj.data);
        
        // Zero the data
        obj.__animation_frame__ = 0;

        // Stop the scale from changing by setting chart.ymax (if it's not already set)
        if (obj.Get('chart.xmax') == 0) {

            var xmax = 0;

            for (var i=0; i<obj.data.length; ++i) {
                if (RGraph.is_array(obj.data[i]) && obj.Get('chart.grouping') == 'stacked') {
                    xmax = Math.max(xmax, RGraph.array_sum(obj.data[i]));
                } else if (RGraph.is_array(obj.data[i]) && obj.Get('chart.grouping') == 'grouped') {
                    xmax = Math.max(xmax, RGraph.array_max(obj.data[i]));
                } else {
                    xmax = Math.max(xmax, RGraph.array_max(obj.data[i]));
                }
            }

            var scale2 = RGraph.getScale2(obj, {'max':xmax});
            obj.Set('chart.xmax', scale2.max);
        }
        
        /**
        * Turn off shadow blur for the duration of the animation
        */
        if (obj.Get('chart.shadow.blur') > 0) {
            var __original_shadow_blur__ = obj.Get('chart.shadow.blur');
            obj.Set('chart.shadow.blur', 0);
        }

        function Grow ()
        {
            var numFrames = 30;

            if (!obj.__animation_frame__) {
                obj.__animation_frame__  = 0;
                obj.__original_vmargin__ = obj.Get('chart.vmargin');
                obj.__vmargin__          = ((obj.canvas.height - obj.Get('chart.gutter.top') - obj.Get('chart.gutter.bottom')) / obj.data.length) / 2;
                obj.Set('chart.vmargin', obj.__vmargin__);
            }

            // Alter the Bar chart data depending on the frame
            for (var j=0; j<obj.original_data.length; ++j) {
                
                // This stops the animatioon from being completely linear
                var easing = Math.pow(Math.sin((obj.__animation_frame__ * (90 / numFrames)) / (180 / PI)), 4);

                if (typeof(obj.data[j]) == 'object') {
                    for (var k=0; k<obj.data[j].length; ++k) {
                        obj.data[j][k] = (obj.__animation_frame__ / numFrames) * obj.original_data[j][k] * easing;
                    }
                } else {
                    obj.data[j] = (obj.__animation_frame__ / numFrames) * obj.original_data[j] * easing;
                }
            }

            /**
            * Increment the vmargin to the target
            */
            obj.Set('chart.vmargin', ((1 - (obj.__animation_frame__ / numFrames)) * (obj.__vmargin__ - obj.__original_vmargin__)) + obj.__original_vmargin__);


            RGraph.Clear(obj.canvas);
            RGraph.RedrawCanvas(obj.canvas);

            if (obj.__animation_frame__ < numFrames) {
                obj.__animation_frame__ += 1;
                
                RGraph.Effects.UpdateCanvas(Grow);
            
            // Turn any shadow blur back on
            } else {
                if (typeof(__original_shadow_blur__) == 'number' && __original_shadow_blur__ > 0) {
                    obj.Set('chart.shadow.blur', __original_shadow_blur__);
                    RGraph.Clear(obj.canvas);
                    RGraph.RedrawCanvas(obj.canvas);
                }
            }
        }
        
        RGraph.Effects.UpdateCanvas(Grow);
    }


    /**
    * Trace
    * 
    * This effect is for the Line chart, uses the jQuery library and slowly
    * uncovers the Line , but you can see the background of the chart. This effect
    * is quite new (1/10/2011) and as such should be used with caution.
    * 
    * @param object obj The graph object
    * @param object     Not used
    * @param int        A number denoting how long (in millseconds) the animation should last for. Defauld
    *                   is 1500
    */
    RGraph.Effects.Line.jQuery.Trace = function (obj)
    {
        var callback = typeof(arguments[2]) == 'function' ? arguments[2] : function () {};
        var opt = arguments[1] || [];
        
        if (!opt['duration']) {
            opt['duration'] = 1000;
        }

        RGraph.Clear(obj.canvas);
        //obj.Draw();
        RGraph.RedrawCanvas(obj.canvas);

        /**
        * Create the DIV that the second canvas will sit in
        */
        var div = document.createElement('DIV');
            var xy = RGraph.getCanvasXY(obj.canvas);
            div.id = '__rgraph_trace_animation_' + RGraph.random(0, 4351623) + '__';
            div.style.left = xy[0] + 'px';
            div.style.top = xy[1] + 'px';
            div.style.width = obj.Get('chart.gutter.left');
            div.style.height = obj.canvas.height + 'px';
            div.style.position = 'absolute';
            div.style.overflow = 'hidden';
        document.body.appendChild(div);
        
        obj.canvas.__rgraph_trace_div__ = div;

        /**
        * Make the second canvas
        */
        var id      = '__rgraph_line_trace_animation_' + RGraph.random(0, 99999999) + '__';
        var canvas2 = document.createElement('CANVAS');




        // Copy the 3D CSS transformation properties across from the original canvas
        var properties = ['WebkitTransform','MozTransform','OTransform','MSTransform','transform'];
        
        for (i in properties) {
            var name = properties[i];
            if (typeof(obj.canvas.style[name]) == 'string' && obj.canvas.style[name]) {
                canvas2.style[name] = obj.canvas.style[name];
            }
        }
        
        

        obj.canvas.__rgraph_line_canvas2__ = canvas2;
        canvas2.width = obj.canvas.width;
        canvas2.height = obj.canvas.height;
        canvas2.style.position = 'absolute';
        canvas2.style.left = 0;
        canvas2.style.top  = 0;


        // This stops the clear effect clearing the canvas - which can happen if you have multiple canvas tags on the page all with
        // dynamic effects that do redrawing
        canvas2.noclear = true;

        canvas2.id         = id;
        div.appendChild(canvas2);

        var reposition_canvas2 = function (e)
        {
            var xy = RGraph.getCanvasXY(obj.canvas);
            
            div.style.left = xy[0] + 'px';
            div.style.top = xy[1] + 'px';
        }
        window.addEventListener('resize', reposition_canvas2, false)
        
        /**
        * Make a copy of the original Line object
        */
        var obj2 = new RGraph.Line(id, RGraph.array_clone(obj.original_data));
        
        // Remove the new line from the ObjectRegistry so that it isn't redawn
        RGraph.ObjectRegistry.Remove(obj2);

        for (i in obj.properties) {
            if (typeof(i) == 'string') {
                obj2.Set(i, obj.properties[i]);
            }
        }

        //obj2.Set('chart.tooltips', null);
        obj2.Set('chart.labels', []);
        obj2.Set('chart.background.grid', false);
        obj2.Set('chart.background.barcolor1', 'rgba(0,0,0,0)');
        obj2.Set('chart.background.barcolor2', 'rgba(0,0,0,0)');
        obj2.Set('chart.ylabels', false);
        obj2.Set('chart.noaxes', true);
        obj2.Set('chart.title', '');
        obj2.Set('chart.title.xaxis', '');
        obj2.Set('chart.title.yaxis', '');
        obj2.Set('chart.filled.accumulative', obj.Get('chart.filled.accumulative'));
        obj.Set('chart.key', []);
        obj2.Draw();
        
        obj.canvas.__rgraph_trace_obj2__ = obj2;


        /**
        * This effectively hides the line
        */
        obj.Set('chart.line.visible', false);
        obj.Set('chart.colors', ['rgba(0,0,0,0)']);
        if (obj.Get('chart.filled')) {
            var original_fillstyle = obj.Get('chart.fillstyle');
            obj.Set('chart.fillstyle', 'rgba(0,0,0,0)');
            obj.Set('chart.animation.trace.original.fillstyle', original_fillstyle);
        }

        RGraph.Clear(obj.canvas);
        //obj.Draw();
        RGraph.RedrawCanvas(obj.canvas);
        
        /**
        * Place a DIV over the canvas to stop interaction with it
        */
        if (!obj.canvas.__rgraph_trace_cover__) {
            var div2 = document.createElement('DIV');
                div2.id = '__rgraph_trace_animation_' + RGraph.random(0, 4351623) + '__';
                div2.style.left = xy[0] + 'px';
                div2.style.top = xy[1] + 'px';
                div2.style.width = obj.canvas.width + 'px';
                div2.style.height = obj.canvas.height + 'px';
                div2.style.position = 'absolute';
                div2.style.overflow = 'hidden';
                div2.style.backgroundColor = 'rgba(0,0,0,0)';
                div.div2 = div2;
                obj.canvas.__rgraph_trace_cover__ = div2;
            document.body.appendChild(div2);
        } else {
            div2 = obj.canvas.__rgraph_trace_cover__;
        }

        /**
        * Animate the DIV that contains the canvas
        */
        jQuery('#' + div.id).animate({
            width: obj.canvas.width + 'px'
        }, opt['duration'], function () {RGraph.Effects.Line.Trace_callback(obj)});


        /**
        * Get rid of the second canvas and turn the line back on
        * on the original.
        */
        RGraph.Effects.Line.Trace_callback = function (obj)
        {
            var obj2 = obj.canvas.__rgraph_trace_obj2__;

            // Remove the window resize listener
            window.removeEventListener('resize', reposition_canvas2, false);

            div.style.display = 'none';
            div2.style.display = 'none';

            //div.removeChild(canvas2);
            obj.Set('chart.line.visible', true);
            
            // Revert the filled status back to as it was
            obj.Set('chart.filled', RGraph.array_clone(obj2.Get('chart.filled')));
            obj.Set('chart.fillstyle', obj.Get('chart.animation.trace.original.fillstyle'));
            obj.Set('chart.colors', RGraph.array_clone(obj2.Get('chart.colors')));
            obj.Set('chart.key', RGraph.array_clone(obj2.Get('chart.key')));

            RGraph.RedrawCanvas(obj.canvas);

            obj.canvas.__rgraph_trace_div__.style.display = 'none';
            obj.canvas.__rgraph_trace_div__ = null;
            obj.canvas.__rgraph_line_canvas2__.style.display = 'none';
            obj.canvas.__rgraph_line_canvas2__ = null;
            obj.canvas.__rgraph_trace_cover__.style.display = 'none';
            obj.canvas.__rgraph_trace_cover__ = null;
            
            
            callback(obj);
        }
    }



    /**
    * Trace2
    * 
    * This is a new version of the Trace effect which no longer requires jQuery and is more compatible
    * with other effects (eg Expand). This new effect is considerably simpler and less code.
    * 
    * @param object obj The graph object
    * @param object     Options for the effect. Currently only "frames" is available.
    * @param int        A function that is called when the ffect is complete
    */
    RGraph.Effects.Line.Trace2 = function (obj)
    {
        var callback  = arguments[2];
        var numFrames = (arguments[1] && arguments[1].frames) ? arguments[1].frames : 60;
        var frame     = 0;
        
        obj.Set('animation.trace.clip', 0);

        function Grow ()
        {
            if (frame > numFrames) {
                if (callback) {
                    callback(obj);
                }
                return;
            }

            obj.Set('animation.trace.clip', frame / numFrames );

            RGraph.RedrawCanvas(obj.canvas);

            frame++;
            RGraph.Effects.UpdateCanvas(Grow);
        }
        
        Grow();
    }



    /**
    * Trace (Radar chart)
    * 
    * This is a Trace effect for the Radar chart
    * 
    * @param object obj The graph object
    * @param object     Options for the effect. Currently only "frames" is available.
    * @param function   A function that is called when the ffect is complete
    */
    RGraph.Effects.Radar.Trace = function (obj)
    {
        var callback  = arguments[2];
        var numFrames = (arguments[1] && arguments[1].frames) ? arguments[1].frames : 60;
        var frame     = 0;

        obj.Set('animation.trace.clip', 0);

        function Grow ()
        {
            if (frame > numFrames) {
                if (callback) {
                    callback(obj);
                }
                return;
            }

            obj.Set('animation.trace.clip', frame / numFrames );

            RGraph.RedrawCanvas(obj.canvas);

            frame++;
            RGraph.Effects.UpdateCanvas(Grow);
        }
        
        Grow();
    }



    /**
    * RoundRobin
    * 
    * This effect does two things:
    *  1. Gradually increases the size of each segment
    *  2. Gradually increases the size of the radius from 0
    * 
    * @param object obj The graph object
    */
    RGraph.Effects.Pie.RoundRobin = function (obj)
    {
        var callback     = arguments[2] ? arguments[2] : null;
        var opt          = arguments[1];
        var currentFrame = 0;
        var numFrames    = (opt && opt['frames']) ? opt['frames'] : 90;
        var targetRadius =  obj.getRadius();
        
        obj.Set('chart.events', false);
        
        // Fix for donuts
        if (obj.properties['chart.variant'] == 'donut' && typeof(obj.properties['chart.variant.donut.width']) == 'number') {
            if (RGraph.is_null(opt)) {
                var opt = {radius: null}
            } else {
                opt.radius = null;
            }
        }


        function RoundRobin_inner ()
        {
            obj.Set('chart.effect.roundrobin.multiplier', Math.pow(Math.sin((currentFrame * (90 / numFrames)) / (180 / PI)), 2) * (currentFrame / numFrames) );

            if (!opt || typeof(opt['radius']) == 'undefined' || opt['radius'] == true) {
                obj.Set('chart.radius', targetRadius * obj.Get('chart.effect.roundrobin.multiplier'));
            }
            
            RGraph.RedrawCanvas(obj.canvas);

            if (currentFrame++ < numFrames) {
                RGraph.Effects.UpdateCanvas(RoundRobin_inner);
            
            } else {
                
                // Re-enable the events and redraw the chart.
                obj.Set('chart.events', true);
                RGraph.RedrawCanvas(obj.canvas);

                if (callback) {
                    callback(obj);
                }
            }
        }

        RGraph.Effects.UpdateCanvas(RoundRobin_inner);
    }


    /**
    * Implode (pie chart)
    * 
    * Here the segments are initially exploded - and gradually
    * contract inwards to create the Pie chart
    * 
    * @param object obj The Pie chart object
    */
    RGraph.Effects.Pie.Implode = function (obj)
    {
        var numFrames = 90;
        var distance  = Math.min(obj.canvas.width, obj.canvas.height);
        var exploded  = obj.Get('chart.exploded');
        var callback  = arguments[2];
        
        function Implode_inner ()
        {
            obj.Set('chart.exploded', Math.sin(numFrames / (180 / PI)) * distance);
            RGraph.Clear(obj.canvas)
            //obj.Draw();
            RGraph.RedrawCanvas(obj.canvas);

            if (numFrames > 0) {
                numFrames--;
                RGraph.Effects.UpdateCanvas(Implode_inner);
            } else {
                // Finish off the animation
                obj.Set('chart.exploded', exploded);
                RGraph.Clear(obj.canvas);
                RGraph.RedrawCanvas(obj.canvas);
                
                if (typeof(callback) == 'function') {
                    callback(obj);
                }
            }
        }
        
        RGraph.Effects.UpdateCanvas(Implode_inner);
    }



    /**
    * Pie chart explode
    * 
    * Explodes the Pie chart - gradually incrementing the size of the chart.explode property
    * 
    * @params object obj The graph object
    */
    RGraph.Effects.Pie.Explode = function (obj)
    {
        var canvas   = obj.canvas;
        var opts     = arguments[1] ? arguments[1] : [];
        var callback = arguments[2] ? arguments[2] : null;
        var frames   = opts['frames'] ? opts['frames'] : 60;

        obj.Set('chart.exploded', 0);

        RGraph.Effects.Animate(obj, {'frames': frames, 'chart.exploded': Math.max(canvas.width, canvas.height)}, callback);
    }



    /**
    * Gauge Grow
    * 
    * This effect gradually increases the represented value
    * 
    * @param object   obj The chart object
    * @param              Not used - pass null
    * @param function     An optional callback function
    */
    RGraph.Effects.Gauge.Grow = function (obj)
    {

        var callback  = arguments[2];
        var numFrames = 30;
        var frame     = 0;

        // Single pointer
        if (typeof(obj.value) == 'number') {

            var origValue = Number(obj.currentValue);

            if (obj.currentValue == null) {
                obj.currentValue = obj.min;
                origValue = obj.min;
            }

            var newValue  = obj.value;
            var diff      = newValue - origValue;
            var step      = (diff / numFrames);
            var frame     = 0;


            function Grow_single ()
            {

                frame++;

                obj.value = ((frame / numFrames) * diff) + origValue;

                if (obj.value > obj.max) obj.value = obj.max;
                if (obj.value < obj.min) obj.value = obj.min;
    
                RGraph.Clear(obj.canvas);
                RGraph.RedrawCanvas(obj.canvas);
    
                if (frame < 30) {
                    RGraph.Effects.UpdateCanvas(Grow_single);
                } else if (typeof(callback) == 'function') {
                    callback(obj);
                }
            }

            Grow_single();

        // Multiple pointers
        } else {

            if (obj.currentValue == null) {
                obj.currentValue = [];
                
                for (var i=0; i<obj.value.length; ++i) {
                    obj.currentValue[i] = obj.min;
                }
                
                origValue = RGraph.array_clone(obj.currentValue);
            }

            var origValue = RGraph.array_clone(obj.currentValue);
            var newValue  = RGraph.array_clone(obj.value);
            var diff      = [];
            var step      = [];
            
            for (var i=0; i<newValue.length; ++i) {
                diff[i] = newValue[i] - Number(obj.currentValue[i]);
                step[i] = (diff[i] / numFrames);
            }



            function Grow_multiple ()
            {
                frame++;
                
                for (var i=0; i<obj.value.length; ++i) {
                    
                    obj.value[i] = ((frame / numFrames) * diff[i]) + origValue[i];

                    if (obj.value[i] > obj.max) obj.value[i] = obj.max;
                    if (obj.value[i] < obj.min) obj.value[i] = obj.min;
        
                    RGraph.Clear(obj.canvas);
                    RGraph.RedrawCanvas(obj.canvas);
                    
                }
    
                if (frame < 30) {
                    RGraph.Effects.UpdateCanvas(Grow_multiple);
                } else if (typeof(callback) == 'function') {
                    callback(obj);
                }
            }
    
            Grow_multiple();
        }
    }


    /**
    * Radar chart grow
    * 
    * This effect gradually increases the magnitude of the points on the radar chart
    * 
    * @param object obj The chart object
    * @param null       Not used
    * @param function   An optional callback that is run when the effect is finished
    */
    RGraph.Effects.Radar.Grow = function (obj)
    {
        var totalframes   = 30;
        var framenum      = totalframes;
        var data          = RGraph.array_clone(obj.data);
        var callback      = arguments[2];
        obj.original_data = RGraph.array_clone(obj.original_data);

        function Grow_inner ()
        {
            for (var i=0; i<data.length; ++i) {
                
                if (obj.original_data[i] == null) {
                    obj.original_data[i] = [];
                }

                for (var j=0; j<data[i].length; ++j) {
                    obj.original_data[i][j] = ((totalframes - framenum)/totalframes)  * data[i][j];
                }
            }

            RGraph.Clear(obj.canvas);
            RGraph.RedrawCanvas(obj.canvas);

            if (framenum > 0) {
                framenum--;
                RGraph.Effects.UpdateCanvas(Grow_inner);
            } else if (typeof(callback) == 'function') {
                callback(obj);
            }
        }
        
        RGraph.Effects.UpdateCanvas(Grow_inner);
    }


    /**
    * Waterfall Grow
    * 
    * @param object obj The chart object
    * @param null Not used
    * @param function An optional function which is called when the animation is finished
    */
    RGraph.Effects.Waterfall.Grow = function (obj)
    {
        var totalFrames = 45;
        var numFrame    = 0;
        var data = RGraph.array_clone(obj.data);
        var callback = arguments[2];
        
        //Reset The data to zeros
        for (var i=0; i<obj.data.length; ++i) {
            obj.data[i] /= totalFrames;
        }
        
        /**
        * Fix the scale
        */
        if (obj.Get('chart.ymax') == null) {
            var max   = obj.getMax(data);
            var scale2 = RGraph.getScale2(obj, {'max':max});
            obj.Set('chart.ymax', scale2.max);
        }
        
        //obj.Set('chart.multiplier.x', 0);
        //obj.Set('chart.multiplier.w', 0);

        function Grow_inner ()
        {
            for (var i=0; i<obj.data.length; ++i) {
                obj.data[i] = data[i] * (numFrame/totalFrames);
            }
            
            var multiplier = Math.pow(Math.sin(((numFrame / totalFrames) * 90) / (180 / PI)), 20);
            //obj.Set('chart.multiplier.x', (numFrame / totalFrames) * multiplier);
            //obj.Set('chart.multiplier.w', (numFrame / totalFrames) * multiplier);
            
            RGraph.Clear(obj.canvas);
            RGraph.RedrawCanvas(obj.canvas);

            if (numFrame++ < totalFrames) {
                RGraph.Effects.UpdateCanvas(Grow_inner);
            } else if (typeof(callback) == 'function') {
                callback(obj);
            }
        }
        
        RGraph.Effects.UpdateCanvas(Grow_inner)
    }



    /**
    * Bar chart Wave effect This effect defaults to 30 frames - which is
    * approximately half a second
    * 
    * @param object obj The chart object
    */
    RGraph.Effects.Bar.Wave2 =
    RGraph.Effects.Bar.Wave = function (obj)
    {
        var totalframes   = (arguments[1] && arguments[1].frames) ? arguments[1].frames : 15;
        var original_data = [];

        obj.Draw();
        //var scale = RGraph.getScale2(obj, {'max':obj.max});
        obj.Set('chart.ymax', obj.scale2.max);
        RGraph.Clear(obj.canvas);
        
        for (var i=0; i<obj.data.length; ++i) {
        
            (function (idx)
            {
                original_data[i] = obj.data[i];
                obj.data[i] = typeof(obj.data[i]) == 'object' ? [] : 0;
                setTimeout(function () {Iterator(idx, totalframes);}, 100 * i)
            })(i);
        }
        
        function Iterator(idx, frames)
        {
            if (frames-- > 0) {

                // Update the data point
                if (typeof(obj.data[idx]) == 'number') {
                    obj.data[idx] = ((totalframes - frames) / totalframes) * original_data[idx]

                } else if (typeof(obj.data[idx]) == 'object') {
                    for (var k=0; k<original_data[idx].length; ++k) {
                        obj.data[idx][k] = ((totalframes - frames) / totalframes) * original_data[idx][k];
                    }
                }

                RGraph.Clear(obj.canvas);
                RGraph.RedrawCanvas(obj.canvas);
                RGraph.Effects.UpdateCanvas(function () {Iterator(idx, frames);});
            }
        }
    }



    /**
    * HProgress Grow effect (which is also the VPogress Grow effect)
    * 
    * @param object obj The chart object
    */
    RGraph.Effects.VProgress.Grow =
    RGraph.Effects.HProgress.Grow = function (obj)
    {
        var canvas        = obj.canvas;
        var context       = obj.context;
        var initial_value = obj.currentValue;
        var numFrames     = 30;
        var currentFrame  = 0

        if (typeof(obj.value) == 'object') {

            if (RGraph.is_null(obj.currentValue)) {
                obj.currentValue = [];
                for (var i=0; i<obj.value.length; ++i) {
                    obj.currentValue[i] = 0;
                }
            }

            var diff      = [];
            var increment = [];

            for (var i=0; i<obj.value.length; ++i) {
                diff[i]      = obj.value[i] - Number(obj.currentValue[i]);
                increment[i] = diff[i] / numFrames;
            }
            
            if (initial_value == null) {
                initial_value = [];
                for (var i=0; i< obj.value.length; ++i) {
                    initial_value[i] = 0;
                }
            }

        } else {
            var diff = obj.value - Number(obj.currentValue);
            var increment = diff  / numFrames;
        }
        var callback      = arguments[2] ? arguments[2] : null;

        function Grow ()
        {
            currentFrame++;

            if (currentFrame <= numFrames) {

                if (typeof(obj.value) == 'object') {
                    obj.value = [];
                    for (var i=0; i<initial_value.length; ++i) {
                        obj.value[i] = initial_value[i] + (increment[i] * currentFrame);
                    }
                } else {
                    obj.value = initial_value + (increment * currentFrame);
                }

                RGraph.Clear(obj.canvas);
                RGraph.RedrawCanvas(obj.canvas);
                
                RGraph.Effects.UpdateCanvas(Grow);

            } else if (callback) {
                callback(obj);
            }
        }
        
        RGraph.Effects.UpdateCanvas(Grow);
    }



    /**
    * Gantt chart Grow effect
    * 
    * @param object obj The chart object
    */
    RGraph.Effects.Gantt.Grow = function (obj)
    {
        var canvas       = obj.canvas;
        var context      = obj.context;
        var numFrames    = 30;
        var currentFrame = 0;
        var callback     = arguments[2] ? arguments[2] : null;
        var events       = obj.data;
        
        var original_events = RGraph.array_clone(events);

        function Grow_gantt_inner ()
        {
            if (currentFrame <= numFrames) {
                // Update the events
                for (var i=0; i<events.length; ++i) {
                    if (typeof(events[i][0]) == 'object') {
                        for (var j=0; j<events[i].length; ++j) {
                            events[i][j][1] = (currentFrame / numFrames) * original_events[i][j][1];
                        }
                    } else {

                        events[i][1] = (currentFrame / numFrames) * original_events[i][1];
                    }
                }

                obj.data = events;

                RGraph.Clear(obj.canvas);
                RGraph.RedrawCanvas(obj.canvas);
                
                currentFrame++;
                
                RGraph.Effects.UpdateCanvas(Grow_gantt_inner);

            } else if (callback) {            
                callback(obj);
            }
        }
        
        RGraph.Effects.UpdateCanvas(Grow_gantt_inner);
    }


    /**
    * This is a compatibility hack provided for Opera and Safari which
    * don't support ther Javascript 1.8.5 function.bind()
    */
    if (!Function.prototype.bind) {  
      Function.prototype.bind = function (oThis) {  
        if (typeof this !== "function") {  
          // closest thing possible to the ECMAScript 5 internal IsCallable function  
          if (console && console.log) {
            console.log('Function.prototype.bind - what is trying to be bound is not callable');
          }
        }  
      
        var aArgs = Array.prototype.slice.call(arguments, 1),   
            fToBind = this,   
            fNOP = function () {},  
            fBound = function () {  
              return fToBind.apply(this instanceof fNOP  
                                     ? this  
                                     : oThis || window,  
                                   aArgs.concat(Array.prototype.slice.call(arguments)));  
            };  
      
        fNOP.prototype = this.prototype;  
        fBound.prototype = new fNOP();  
      
        return fBound;  
      };  
    }


    /**
    * Rose chart explode
    * 
    * Explodes the Rose chart - gradually incrementing the size of the chart.explode property
    * 
    * @params object obj The graph object
    */
    RGraph.Effects.Rose.Explode = function (obj)
    {
        var canvas   = obj.canvas;
        var opts     = arguments[1] ? arguments[1] : [];
        var callback = arguments[2] ? arguments[2] : null;
        var frames   = opts['frames'] ? opts['frames'] : 60;

        obj.Set('chart.exploded', 0);

        RGraph.Effects.Animate(obj, {'frames': frames, 'chart.exploded': Math.min(canvas.width, canvas.height)}, callback);
    }


    /**
    * Rose chart implode
    * 
    * Implodes the Rose chart - gradually decreasing the size of the chart.explode property. It starts at the largest of
    * the canvas width./height
    * 
    * @params object obj The graph object
    */
    RGraph.Effects.Rose.Implode = function (obj)
    {
        var canvas   = obj.canvas;
        var opts     = arguments[1] ? arguments[1] : [];
        var callback = arguments[2] ? arguments[2] : null;
        var frames   = opts['frames'] ? opts['frames'] : 60;

        obj.Set('chart.exploded', Math.min(canvas.width, canvas.height));

        RGraph.Effects.Animate(obj, {'frames': frames, 'chart.exploded': 0}, callback);
    }



    /**
    * Gauge Grow
    * 
    * This effect gradually increases the represented value
    * 
    * @param object   obj The chart object
    * @param              Not used - pass null
    * @param function     An optional callback function
    */
    RGraph.Effects.Thermometer.Grow = function (obj)
    {
        var callback  = arguments[2];
        var numFrames = 30;
        var origValue = Number(obj.currentValue);

        if (obj.currentValue == null) {
            obj.currentValue = 0
            origValue        = 0;
        }

        var newValue  = obj.value;
        var diff      = newValue - origValue;
        var step      = (diff / numFrames);
        var frame = 0;

        function Grow ()
        {
            frame++
            
            // Set the new value
            obj.value = v = ((frame / numFrames) * diff) + origValue

            RGraph.Clear(obj.canvas);
            RGraph.RedrawCanvas(obj.canvas);

            if (frame < 30) {
                RGraph.Effects.UpdateCanvas(Grow);
            } else if (typeof(callback) == 'function') {
                callback(obj);
            }
        }

        RGraph.Effects.UpdateCanvas(Grow);
    }


    /**
    * Trace
    * 
    * This effect is for the Scatter chart, uses the jQuery library and slowly
    * uncovers the Line/marks, but you can see the background of the chart.
    * 
    * @param object obj The graph object
    * @param object     Options - you can specify duration to set how long the effect lasts for
    */
    RGraph.Effects.Scatter.jQuery.Trace = function (obj)
    {
        var callback  = typeof(arguments[2]) == 'function' ? arguments[2] : function () {};
        var opt       = arguments[1] || [];
        
        if (!opt['duration']) {
            opt['duration'] = 1500;
        }

        RGraph.Clear(obj.canvas);
        RGraph.RedrawCanvas(obj.canvas);

        /**
        * Create the DIV that the second canvas will sit in
        */
        var div = document.createElement('DIV');
            var xy = RGraph.getCanvasXY(obj.canvas);
            div.id = '__rgraph_trace_animation_' + RGraph.random(0, 4351623) + '__';
            div.style.left = xy[0] + 'px';
            div.style.top = xy[1] + 'px';
            div.style.width = obj.Get('chart.gutter.left');
            div.style.height = obj.canvas.height + 'px';
            div.style.position = 'absolute';
            div.style.overflow = 'hidden';
        document.body.appendChild(div);
        
        /**
        * Make the second canvas
        */
        var id      = '__rgraph_scatter_trace_animation_' + RGraph.random(0, 99999999) + '__';
        var canvas2 = document.createElement('CANVAS');
        canvas2.width = obj.canvas.width;
        canvas2.height = obj.canvas.height;
        canvas2.style.position = 'absolute';
        canvas2.style.left = 0;
        canvas2.style.top  = 0;
        
        // This stops the clear effect clearing the canvas - which can happen if you have multiple canvas tags on the page all with
        // dynamic effects that do redrawing
        canvas2.noclear = true;

        canvas2.id         = id;
        div.appendChild(canvas2);

        var reposition_canvas2 = function (e)
        {

            var xy = RGraph.getCanvasXY(obj.canvas);
            
            div.style.left = xy[0]   + 'px';
            div.style.top = xy[1] + 'px';
        }
        window.addEventListener('resize', reposition_canvas2, false)

        /**
        * Make a copy of the original Line object
        */
        var obj2 = new RGraph.Scatter(id, RGraph.array_clone(obj.data));

        // Remove the new line from the ObjectRegistry so that it isn't redawn
        RGraph.ObjectRegistry.Remove(obj2);

        for (i in obj.properties) {
            if (typeof(i) == 'string') {
                obj2.Set(i, obj.properties[i]);
            }
        }


        obj2.Set('chart.labels', []);
        obj2.Set('chart.background.grid', false);
        obj2.Set('chart.background.barcolor1', 'rgba(0,0,0,0)');
        obj2.Set('chart.background.barcolor2', 'rgba(0,0,0,0)');
        obj2.Set('chart.ylabels', false);
        obj2.Set('chart.noaxes', true);
        obj2.Set('chart.title', '');
        obj2.Set('chart.title.xaxis', '');
        obj2.Set('chart.title.yaxis', '');
        obj.Set('chart.key', []);
        obj2.Draw();


        /**
        * This effectively hides the line
        */
        obj.Set('chart.line.visible', false);


        RGraph.Clear(obj.canvas);
        RGraph.RedrawCanvas(obj.canvas);
        
        /**
        * Place a DIV over the canvas to stop interaction with it
        */
            if (!obj.canvas.__rgraph_scatter_trace_cover__) {
            var div2 = document.createElement('DIV');
                div2.id = '__rgraph_trace_animation_' + RGraph.random(0, 4351623) + '__';
                div2.style.left = xy[0] + 'px';
                div2.style.top = xy[1] + 'px';
                div2.style.width = obj.canvas.width + 'px';
                div2.style.height = obj.canvas.height + 'px';
                div2.style.position = 'absolute';
                div2.style.overflow = 'hidden';
                div2.style.backgroundColor = 'rgba(0,0,0,0)';
                div.div2 = div2;
                obj.canvas.__rgraph_scatter_trace_cover__ = div2
            document.body.appendChild(div2);
        } else {
            div2 = obj.canvas.__rgraph_scatter_trace_cover__;
        }

        /**
        * Animate the DIV that contains the canvas
        */
        jQuery('#' + div.id).animate({
            width: obj.canvas.width + 'px'
        }, opt['duration'], function () {

            // Remove the window resize listener
            window.removeEventListener('resize', reposition_canvas2, false);

            div.style.display  = 'none';
            div2.style.display = 'none';

            //div.removeChild(canvas2);
            obj.Set('chart.line.visible', true);

            // Revert the colors back to what they were
            obj.Set('chart.colors', RGraph.array_clone(obj2.Get('chart.colors')));
            obj.Set('chart.key', RGraph.array_clone(obj2.Get('chart.key')));

            RGraph.RedrawCanvas(obj.canvas);
            
            obj.canvas.__rgraph_trace_cover__ = null;

            callback(obj);
        });
    }



    /**
    * CornerGauge Grow
    * 
    * This effect gradually increases the represented value
    * 
    * @param object   obj The chart object
    * @param              Not used - pass null
    * @param function     An optional callback function
    */
    RGraph.Effects.CornerGauge.Grow = function (obj)
    {
        var callback  = arguments[2];
        var numFrames = 30;
        var frame     = 0;

        // Single pointer
        if (typeof(obj.value) == 'number') {

            var origValue = Number(obj.currentValue);
            
            if (obj.currentValue == null) {
                obj.currentValue = obj.min;
                origValue = obj.min;
            }
    
            var newValue  = obj.value;
            var diff      = newValue - origValue;
            var step      = (diff / numFrames);
            var frame     = 0;
    

            function Grow_single ()
            {
                frame++;

                obj.value = ((frame / numFrames) * diff) + origValue

                if (obj.value > obj.max) obj.value = obj.max;
                if (obj.value < obj.min) obj.value = obj.min;
    
                RGraph.Clear(obj.canvas);
                RGraph.RedrawCanvas(obj.canvas);
    
                if (frame < 30) {
                    RGraph.Effects.UpdateCanvas(Grow_single);
                } else if (typeof(callback) == 'function') {
                    callback(obj);
                }
            }

            Grow_single();

        // Multiple pointers
        } else {

            if (obj.currentValue == null) {
                obj.currentValue = [];
                
                for (var i=0; i<obj.value.length; ++i) {
                    obj.currentValue[i] = obj.min;
                }
                
                origValue = RGraph.array_clone(obj.currentValue);
            }

            var origValue = RGraph.array_clone(obj.currentValue);
            var newValue  = RGraph.array_clone(obj.value);
            var diff      = [];
            var step      = [];
            
            for (var i=0; i<newValue.length; ++i) {
                diff[i] = newValue[i] - Number(obj.currentValue[i]);
                step[i] = (diff[i] / numFrames);
            }



            function Grow_multiple ()
            {
                frame++;
                
                for (var i=0; i<obj.value.length; ++i) {
                    
                    obj.value[i] = ((frame / numFrames) * diff[i]) + origValue[i];

                    if (obj.value[i] > obj.max) obj.value[i] = obj.max;
                    if (obj.value[i] < obj.min) obj.value[i] = obj.min;
        
                    RGraph.Clear(obj.canvas);
                    RGraph.RedrawCanvas(obj.canvas);
                    
                }
    
                if (frame < 30) {
                    RGraph.Effects.UpdateCanvas(Grow_multiple);
                } else if (typeof(callback) == 'function') {
                    callback(obj);
                }
            }
    
            Grow_multiple();
        }
    }



    /**
    * Rose chart Grow
    * 
    * This effect gradually increases the size of the Rose chart
    * 
    * @param object   obj The chart object
    * @param              Not used - pass null
    * @param function     An optional callback function
    */
    RGraph.Effects.Rose.Grow = function (obj)
    {
        var callback  = arguments[2];
        var numFrames = 60;
        var frame     = 0;


        function Grow ()
        {
            frame++;
            
            obj.Set('chart.animation.grow.multiplier', frame / numFrames);

            RGraph.Clear(obj.canvas);
            RGraph.RedrawCanvas(obj.canvas);

            if (frame < numFrames) {
                ++frame;
                RGraph.Effects.UpdateCanvas(Grow);
            } else {
                obj.Set('chart.animation.grow.multiplier', 1);
                RGraph.Clear(obj.canvas);
                RGraph.RedrawCanvas(obj.canvas);
                
                if (typeof(callback) == 'function') {
                    callback(obj);
                }
            }
        }

        RGraph.Effects.UpdateCanvas(Grow);
    }



    /**
    * Horizontal Scissors (open)
    * 
    * @param object   obj The graph object
    * @param @object      An array of options
    * @param function     Optional callback function
    * 
    */
    RGraph.Effects.jQuery.HScissors.Open = function (obj)
    {
        var canvas = obj.isRGraph ? obj.canvas : obj;;
        var id     = canvas.id;
        var opts   = arguments[1] ? arguments[1] : [];
        var delay  = 1000;
        var color  = opts['color'] ? opts['color'] : 'white';
        var xy     = RGraph.getCanvasXY(canvas);
        var height = canvas.height / 5;
        
        /**
        * First draw the chart
        */
        RGraph.Clear(canvas);
        RGraph.RedrawCanvas(canvas);

        for (var i=0; i<5; ++i) {
            var div = document.getElementById(id + "scissors_" + i)
            if (!div) {
                var div = document.createElement('DIV');
                    div.id = id + 'scissors_' + i;
                    div.style.width =  canvas.width + 'px';
                    div.style.height = height + 'px';
                    div.style.left   = xy[0] + 'px';
                    div.style.top   = (xy[1] + (canvas.height * (i / 5))) + 'px';
                    div.style.position = 'absolute';
                    div.style.backgroundColor = color;
                document.body.appendChild(div);
            }
    
            if (i % 2 == 0) {
                jQuery('#' + id + 'scissors_' + i).animate({left: canvas.width + 'px', width: 0}, delay);
            } else {
                jQuery('#' + id + 'scissors_' + i).animate({width: 0}, delay);
            }
        }

        setTimeout(function () {document.body.removeChild(document.getElementById(id + 'scissors_0'));}, delay);
        setTimeout(function () {document.body.removeChild(document.getElementById(id + 'scissors_1'));}, delay);
        setTimeout(function () {document.body.removeChild(document.getElementById(id + 'scissors_2'));}, delay);
        setTimeout(function () {document.body.removeChild(document.getElementById(id + 'scissors_3'));}, delay);
        setTimeout(function () {document.body.removeChild(document.getElementById(id + 'scissors_4'));}, delay);
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], delay);
        }
    }



    /**
    * Horizontal Scissors (Close)
    * 
    * @param object   obj The graph object
    * @param @object      An array of options
    * @param function     Optional callback function
    * 
    */
    RGraph.Effects.jQuery.HScissors.Close = function (obj)
    {
        var canvas = obj.isRGraph ? obj.canvas : obj;
        var id     = canvas.id;
        var opts   = arguments[1] ? arguments[1] : [];
        var delay  = 1000;
        var color  = opts['color'] ? opts['color'] : 'white';
        var xy     = RGraph.getCanvasXY(canvas);
        var height = canvas.height / 5;
        
        /**
        * First draw the chart
        */
        RGraph.Clear(canvas);
        RGraph.RedrawCanvas(canvas);

        for (var i=0; i<5; ++i) {
            var div = document.createElement('DIV');
                div.id             = id + '_scissors_' + i;
                div.style.width    = 0;
                div.style.height   = height + 'px';
                div.style.left     = (i % 2 == 0 ? xy[0] + canvas.width : xy[0]) + 'px';
                div.style.top      = (xy[1] + (canvas.height * (i / 5))) + 'px';
                div.style.position = 'absolute';
                div.style.backgroundColor = color;
            document.body.appendChild(div);

            if (i % 2 == 0) {
                jQuery('#' + id + '_scissors_' + i).animate({left: xy[0] + 'px', width: canvas.width + 'px'}, delay);
            } else {
                jQuery('#' + id + '_scissors_' + i).animate({width: canvas.width + 'px'}, delay);
            }
        }
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], delay);
        }
    }



    /**
    * Vertical Scissors (open)
    * 
    * @param object   obj The graph object
    * @param @object      An array of options
    * @param function     Optional callback function
    * 
    */
    RGraph.Effects.jQuery.VScissors.Open = function (obj)
    {
        var canvas = obj.isRGraph ? obj.canvas : obj;;
        var id     = canvas.id;
        var opts   = arguments[1] ? arguments[1] : [];
        var delay  = 1000;
        var color  = opts['color'] ? opts['color'] : 'white';
        var xy     = RGraph.getCanvasXY(canvas);
        var width = canvas.width / 5;
        
        /**
        * First draw the chart
        */
        RGraph.Clear(canvas);
        RGraph.RedrawCanvas(canvas);

        for (var i=0; i<5; ++i) {
            var div = document.getElementById(id + "_vscissors_" + i)
            if (!div) {
                var div = document.createElement('DIV');
                    div.id = id + '_vscissors_' + i;
                    div.style.width =  width + 'px';
                    div.style.height = canvas.height + 'px';
                    div.style.left   = xy[0] + (canvas.width * (i / 5)) + 'px';
                    div.style.top   = xy[1] + 'px';
                    div.style.position = 'absolute';
                    div.style.backgroundColor = color;
                document.body.appendChild(div);
            }

            if (i % 2 == 0) {
                jQuery('#' + id + '_vscissors_' + i).animate({top: xy[1] + canvas.height + 'px', height: 0}, delay);
            } else {
                jQuery('#' + id + '_vscissors_' + i).animate({height: 0}, delay);
            }
        }

        setTimeout(function () {document.body.removeChild(document.getElementById(id + '_vscissors_0'));}, delay);
        setTimeout(function () {document.body.removeChild(document.getElementById(id + '_vscissors_1'));}, delay);
        setTimeout(function () {document.body.removeChild(document.getElementById(id + '_vscissors_2'));}, delay);
        setTimeout(function () {document.body.removeChild(document.getElementById(id + '_vscissors_3'));}, delay);
        setTimeout(function () {document.body.removeChild(document.getElementById(id + '_vscissors_4'));}, delay);
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], delay);
        }
    }


    /**
    * Vertical Scissors (close)
    * 
    * @param object   obj The graph object
    * @param @object      An array of options
    * @param function     Optional callback function
    * 
    */
    RGraph.Effects.jQuery.VScissors.Close = function (obj)
    {
        var canvas = obj.isRGraph ? obj.canvas : obj;
        var id     = canvas.id;
        var opts   = arguments[1] ? arguments[1] : [];
        var delay  = 1000;
        var color  = opts['color'] ? opts['color'] : 'white';
        var xy     = RGraph.getCanvasXY(canvas);
        var width  = canvas.width / 5;
        
        /**
        * First draw the chart
        */
        RGraph.Clear(canvas);
        RGraph.RedrawCanvas(canvas);

        for (var i=0; i<5; ++i) {
            var div = document.getElementById(id + "_vscissors_" + i)
            if (!div) {
                var div                = document.createElement('DIV');
                    div.id             = id + '_vscissors_' + i;
                    div.style.width    =  width + 'px';
                    div.style.height   = 0;
                    div.style.left     = xy[0] + (width * i) + 'px';
                    div.style.top      = (i % 2 == 0 ? xy[1] + canvas.height : xy[1]) + 'px';
                    div.style.position = 'absolute';
                    div.style.backgroundColor = color;
                document.body.appendChild(div);
            }

            if (i % 2 == 0) {
                jQuery('#' + id + '_vscissors_' + i).animate({top: xy[1] + 'px', height: canvas.height + 'px'}, delay);
            } else {
                jQuery('#' + id + '_vscissors_' + i).animate({height: canvas.height + 'px'}, delay);
            }
        }
        
        /**
        * Callback
        */
        if (typeof(arguments[2]) == 'function') {
            setTimeout(arguments[2], delay);
        }
    }

    /**
    * o------------------------------------------------------------------------------o
    * | This file is part of the RGraph package - you can learn more at:             |
    * |                                                                              |
    * |                          http://www.rgraph.net                               |
    * |                                                                              |
    * | This package is licensed under the RGraph license. For all kinds of business |
    * | purposes there is a small one-time licensing fee to pay and for non          |
    * | commercial  purposes it is free to use. You can read the full license here:  |
    * |                                                                              |
    * |                      http://www.rgraph.net/LICENSE.txt                       |
    * o------------------------------------------------------------------------------o
    */

    if (typeof(RGraph) == 'undefined') RGraph = {};

    /**
    * The chart constructor. This function sets up the object. It takes the ID (the HTML attribute) of the canvas as the
    * first argument and the data as the second. If you need to change this, you can.
    * 
    * NB: If tooltips are ever implemented they must go below the use event listeners!!
    * 
    * @param string id    The canvas tag ID
    * @param number min   The minimum value
    * @param number max   The maximum value
    * @param number value The value reported by the thermometer
    */
    RGraph.Thermometer = function (id, min, max, value)
    {
        this.id                = id;
        this.canvas            = document.getElementById(id);
        this.context           = this.canvas.getContext ? this.canvas.getContext("2d") : null;
        this.canvas.__object__ = this;
        this.uid               = RGraph.CreateUID();
        this.canvas.uid        = this.canvas.uid ? this.canvas.uid : RGraph.CreateUID();
        this.colorsParsed      = false;
        this.type              = 'thermometer';
        this.isRGraph          = true;
        this.min               = min;
        this.max               = max;
        this.value             = value;
        this.coords            = [];
        this.graphArea         = [];
        this.currentValue      = null;
        this.bulbRadius        = 0;
        this.bulbTopRadius     = 0;
        this.bulbTopCenterX    = 0
        this.bulbTopCenterY    = 0;
        this.coordsText        = [];

        RGraph.OldBrowserCompat(this.context);


        this.properties = {
            'chart.colors':                 ['Gradient(#c00:red:#f66:#fcc)'],
            'chart.gutter.left':            15,
            'chart.gutter.right':           15,
            'chart.gutter.top':             15,
            'chart.gutter.bottom':          15,
            'chart.ticksize':               5,
            'chart.text.color':             'black',
            'chart.text.font':              'Arial',
            'chart.text.size':              10,
            'chart.units.pre':              '',
            'chart.units.post':             '',
            'chart.zoom.factor':            1.5,
            'chart.zoom.fade.in':           true,
            'chart.zoom.fade.out':          true,
            'chart.zoom.hdir':              'right',
            'chart.zoom.vdir':              'down',
            'chart.zoom.frames':            25,
            'chart.zoom.delay':             16.666,
            'chart.zoom.shadow':            true,
            'chart.zoom.background':        true,
            'chart.title':                  '',
            'chart.title.side':             '',
            'chart.title.side.bold':        true,
            'chart.title.side.font':        null,
            'chart.shadow':                 true,
            'chart.shadow.offsetx':         0,
            'chart.shadow.offsety':         0,
            'chart.shadow.blur':            15,
            'chart.shadow.color':           'gray',
            'chart.resizable':              false,
            'chart.contextmenu':            null,
            'chart.adjustable':             false,
            'chart.value.label':            true,
            'chart.scale.visible':          false,
            'chart.scale.decimals':         0,
            'chart.labels.count':          5,
            'chart.annotatable':            false,
            'chart.annotate.color':         'black',
            'chart.scale.decimals':         0,
            'chart.scale.point':            '.',
            'chart.scale.thousand':         ',',
            'chart.tooltips':               null,
            'chart.tooltips.highlight':     true,
            'chart.tooltips.effect':        'fade',
            'chart.tooltips.event':         'onclick',
            'chart.highlight.stroke':       'rgba(0,0,0,0)',
            'chart.highlight.fill':         'rgba(255,255,255,0.7)'
        }



        /**
        * A simple check that the browser has canvas support
        */
        if (!this.canvas) {
            alert('[THERMOMETER] No canvas support');
            return;
        }
        
        /**
        * The thermometer can only have one data point so only this.$0 needs to be created
        */
        this.$0 = {}


        /**
        * Translate half a pixel for antialiasing purposes - but only if it hasn't beeen
        * done already
        */
        if (!this.canvas.__rgraph_aa_translated__) {
            this.context.translate(0.5,0.5);
            
            this.canvas.__rgraph_aa_translated__ = true;
        }


        /**
        * Now, because canvases can support multiple charts, canvases must always be registered
        */
        RGraph.Register(this);
    }




    /**
    * A setter.
    * 
    * @param name  string The name of the property to set
    * @param value mixed  The value of the property
    */
    RGraph.Thermometer.prototype.Set = function (name, value)
    {

        /**
        * This should be done first - prepend the property name with "chart." if necessary
        */
        if (name.substr(0,6) != 'chart.') {
            name = 'chart.' + name;
        }
        
        /**
        * Change of name
        */
        if (name == 'chart.ylabels.count') {
            name = 'chart.labels.count';
        }
        
        this.properties[name.toLowerCase()] = value;

        return this;
    }




    /**
    * A getter.
    * 
    * @param name  string The name of the property to get
    */
    RGraph.Thermometer.prototype.Get = function (name)
    {
        /**
        * This should be done first - prepend the property name with "chart." if necessary
        */
        if (name.substr(0,6) != 'chart.') {
            name = 'chart.' + name;
        }

        return this.properties[name];
    }




    /**
    * Draws the thermometer
    */
    RGraph.Thermometer.prototype.Draw = function ()
    {
        var ca   = this.canvas;
        var co   = this.context;
        var prop = this.properties;

        /**
        * Fire the custom RGraph onbeforedraw event (which should be fired before the chart is drawn)
        */
        RGraph.FireCustomEvent(this, 'onbeforedraw');

        /**
        * Parse the colors. This allows for simple gradient syntax
        */
        if (!this.colorsParsed) {
            this.parseColors();
            
            // Don't want to do this again
            this.colorsParsed = true;
        }

        /**
        * Set the current value
        */
        this.currentValue = this.value;
        
        /**
        * This is new in May 2011 and facilitates indiviual gutter settings,
        * eg chart.gutter.left
        */
        this.gutterLeft   = prop['chart.gutter.left'];
        this.gutterRight  = prop['chart.gutter.right'];
        this.gutterTop    = prop['chart.gutter.top'];
        this.gutterBottom = prop['chart.gutter.bottom'];
        
        /**
        * Get the scale
        */
        this.scale2 = RGraph.getScale2(this, {
                                            'max':this.max,
                                            'min':this.min,
                                            'strict':true,
                                            'scale.thousand':prop['chart.scale.thousand'],
                                            'scale.point':prop['chart.scale.point'],
                                            'scale.decimals':prop['chart.scale.decimals'],
                                            'ylabels.count':prop['chart.labels.count'],
                                            'scale.round':prop['chart.scale.round'],
                                            'units.pre': prop['chart.units.pre'],
                                            'units.post': prop['chart.units.post']
                                           });


        /**
        * Draw the background
        */
        this.DrawBackground();

        /**
        * Draw the bar that represents the value
        */
        this.DrawBar();

        /**
        * Draw the tickmarks/hatchmarks
        */
        this.DrawTickMarks();

        /**
        * Draw the label
        */
        this.DrawLabels();

        /**
        * Draw the title
        */
        if (this.Get('chart.title')) {
            this.DrawTitle();
        }
        
        /**
        * Draw the side title
        */
        if (this.Get('chart.title.side')) {
            this.DrawSideTitle();
        }
        
        /**
        * This function enables resizing
        */
        if (this.Get('chart.resizable')) {
            RGraph.AllowResizing(this);
        }
        
        
        /**
        * Setup the context menu if required
        */
        if (this.Get('chart.contextmenu')) {
            RGraph.ShowContext(this);
        }


        /**
        * This installs the event listeners
        */
        RGraph.InstallEventListeners(this);

        
        /**
        * Fire the custom RGraph ondraw event (which should be fired when you have drawn the chart)
        */
        RGraph.FireCustomEvent(this, 'ondraw');
        
        return this;
    }





    /**
    * Draws the thermometer itself
    */
    RGraph.Thermometer.prototype.DrawBackground = function ()
    {
        var canvas     = this.canvas;
        var context    = this.context;
        var bulbRadius = (this.canvas.width - this.gutterLeft - this.gutterRight) / 2;
        
        // This is the radius/x/y of the top "semi-circle"
        this.bulbTopRadius  = (this.canvas.width - this.gutterLeft - this.gutterRight - 24)/ 2
        this.bulbTopCenterX = this.gutterLeft + bulbRadius;
        this.bulbTopCenterY = this.gutterTop + bulbRadius;

        //This is the radius/x/y of the bottom bulb
        this.bulbBottomRadius  = bulbRadius;
        this.bulbBottomCenterX = this.gutterLeft + bulbRadius;
        this.bulbBottomCenterY = this.canvas.height - this.gutterBottom - bulbRadius;
        
        // Save the bulbRadius as an object variable
        this.bulbRadius = bulbRadius;

        // Draw the black background that becomes the border
        context.beginPath();
            context.fillStyle = 'black';

            if (this.Get('chart.shadow')) {
                RGraph.SetShadow(this, this.Get('chart.shadow.color'), this.Get('chart.shadow.offsetx'), this.Get('chart.shadow.offsety'), this.Get('chart.shadow.blur'));
            }

            context.fillRect(this.gutterLeft + 12,this.gutterTop + bulbRadius,this.canvas.width - this.gutterLeft - this.gutterRight - 24, this.canvas.height - this.gutterTop - this.gutterBottom - bulbRadius - bulbRadius);
            
            // Bottom bulb
            context.arc(this.bulbBottomCenterX, this.bulbBottomCenterY, bulbRadius, 0, TWOPI, 0);
            
            // Top bulb (which is actually a semi-circle)
            context.arc(this.bulbTopCenterX,this.bulbTopCenterY,this.bulbTopRadius,0,TWOPI,0);
        context.fill();
        
        // Save the radius of the top semi circle
        

        RGraph.NoShadow(this);

        // Draw the white inner content background that creates the border
        context.beginPath();
            context.fillStyle = 'white';
            context.fillRect(this.gutterLeft + 12 + 1,this.gutterTop + bulbRadius,this.canvas.width - this.gutterLeft - this.gutterRight - 24 - 2,this.canvas.height - this.gutterTop - this.gutterBottom - bulbRadius - bulbRadius);
            context.arc(this.gutterLeft + bulbRadius, this.canvas.height - this.gutterBottom - bulbRadius, bulbRadius - 1, 0, TWOPI, 0);
            context.arc(this.gutterLeft + bulbRadius,this.gutterTop + bulbRadius,((this.canvas.width - this.gutterLeft - this.gutterRight - 24)/ 2) - 1,0,TWOPI,0);
        context.fill();

        // Draw the bottom content of the thermometer
        context.beginPath();
            context.fillStyle = this.Get('chart.colors')[0];
            context.arc(this.gutterLeft + bulbRadius, this.canvas.height - this.gutterBottom - bulbRadius, bulbRadius - 1, 0, TWOPI, 0);
            context.rect(this.gutterLeft + 12 + 1, this.canvas.height - this.gutterBottom - bulbRadius - bulbRadius,this.canvas.width - this.gutterLeft - this.gutterRight - 24 - 2, bulbRadius);
        context.fill();

        // Save the X/Y/width/height
        this.graphArea[0] = this.gutterLeft + 12 + 1;
        this.graphArea[1] = this.gutterTop + bulbRadius;
        this.graphArea[2] = this.canvas.width - this.gutterLeft - this.gutterRight - 24 - 2;
        this.graphArea[3] = (this.canvas.height - this.gutterBottom - bulbRadius - bulbRadius) - (this.graphArea[1]);
    }



    /**
    * This draws the bar that indicates the value of the thermometer
    */
    RGraph.Thermometer.prototype.DrawBar = function ()
    {
        var barHeight = ((this.value - this.min) / (this.max - this.min)) * this.graphArea[3];
        var context   = this.context;

        // Draw the actual bar that indicates the value
        context.beginPath();
            context.fillStyle = this.Get('chart.colors')[0];
            
            // This solves an issue with ExCanvas showing a whiite cutout in the chart
            if (RGraph.isOld()) {
                context.arc(this.bulbBottomCenterX, this.bulbBottomCenterY, this.bulbBottomRadius - 1, 0, TWOPI, false)
            }

            context.rect(this.graphArea[0],
                         this.graphArea[1] + this.graphArea[3] - barHeight,
                         this.graphArea[2],
                         barHeight + 2);
        context.fill();
        
        this.coords[0] = [this.graphArea[0],this.graphArea[1] + this.graphArea[3] - barHeight,this.graphArea[2],barHeight];
    }


    
    /**
    * Draws the tickmarks of the thermometer
    */
    RGraph.Thermometer.prototype.DrawTickMarks = function ()
    {
        this.context.strokeStyle = 'black'
        var ticksize = this.Get('chart.ticksize');

        // Left hand side tickmarks
            this.context.beginPath();
                for (var i=this.graphArea[1]; i<=(this.graphArea[1] + this.graphArea[3]); i += (this.graphArea[3] / 10)) {
                    this.context.moveTo(this.gutterLeft + 12, Math.round(i));
                    this.context.lineTo(this.gutterLeft + 12 + ticksize, Math.round(i));
                }
            this.context.stroke();

        // Right hand side tickmarks
            this.context.beginPath();
                for (var i=this.graphArea[1]; i<=(this.graphArea[1] + this.graphArea[3]); i += (this.graphArea[3] / 10)) {
                    this.context.moveTo(this.canvas.width - (this.gutterRight + 12), Math.round(i));
                    this.context.lineTo(this.canvas.width - (this.gutterRight + 12 + ticksize), Math.round(i));
                }
            this.context.stroke();
    }

    
    /**
    * Draws the labels of the thermometer. Now (4th August 2011) draws
    * the scale too
    */
    RGraph.Thermometer.prototype.DrawLabels = function ()
    {
        /**
        * This draws draws the label that sits at the top of the chart
        */
        if (this.Get('chart.value.label')) {
            this.context.fillStyle = this.properties['chart.text.color'];
            var text = this.Get('chart.scale.visible') ? RGraph.number_format(this, this.value.toFixed(this.Get('chart.scale.decimals'))) : RGraph.number_format(this, this.value.toFixed(this.Get('chart.scale.decimals')), this.Get('chart.units.pre'), this.Get('chart.units.post'));

            RGraph.Text2(this, {'font': this.properties['chart.text.font'],
                                'size': this.properties['chart.text.size'],
                                'x':this.gutterLeft + this.bulbRadius,
                                'y': this.coords[0][1] + 7,
                                'text': text,
                                'valign':'top',
                                'halign':'center',
                                'bounding':true,
                                'boundingFill':'white',
                                'tag': 'value.label'
                               });
        }


        /**
        * Draw the scale if requested
        */
        if (this.Get('chart.scale.visible')) {
            this.DrawScale();
        }
    }

    
    /**
    * Draws the title
    */
    RGraph.Thermometer.prototype.DrawTitle = function ()
    {
        this.context.fillStyle = this.Get('chart.text.color');
            RGraph.Text2(this, {'font': this.properties['chart.text.font'],
                                'size': this.properties['chart.text.size'] + 2,
                                'x':this.gutterLeft + ((this.canvas.width - this.gutterLeft - this.gutterRight) / 2),
                                'y': this.gutterTop,
                                'text': String(this.Get('chart.title')),
                                'valign':'center',
                                'halign':'center',
                                'bold':true,
                                'tag': 'title'
                               });
    }

    
    /**
    * Draws the title
    */
    RGraph.Thermometer.prototype.DrawSideTitle = function ()
    {
        var font = this.Get('chart.title.side.font') ? this.Get('chart.title.side.font') : this.Get('chart.text.font');
        var size = this.Get('chart.title.side.size') ? this.Get('chart.title.side.size') : this.Get('chart.text.size') + 2;

        this.context.fillStyle = this.Get('chart.text.color');
        RGraph.Text2(this, {'font': this.properties['chart.text.font'],
                            'size': this.properties['chart.text.size'] + 2,
                            'x':this.gutterLeft - 3,
                            'y': ((this.canvas.height - this.gutterTop - this.gutterBottom) / 2) + this.gutterTop,
                            'text': String(this.Get('chart.title.side')),
                            'valign':'center',
                            'halign':'center',
                            'angle':270,
                            'bold':true,
                            'tag': 'title.side'
                           });
    }


    /**
    * Draw the scale if requested
    */
    RGraph.Thermometer.prototype.DrawScale = function ()
    {
        var ca   = this.canvas;
        var co   = this.context;
        var prop = this.properties;

        var numLabels = prop['chart.labels.count']; // The -1 is so that  the number of labels tallies with what is displayed
        var step      = (this.max - this.min) / numLabels;
        
        this.context.fillStyle = this.Get('chart.text.color');
        
        var font      = this.Get('chart.text.font');
        var size       = this.Get('chart.text.size');
        var units_pre  = this.Get('chart.units.pre');
        var units_post = this.Get('chart.units.post');
        var decimals   = this.Get('chart.scale.decimals');

        for (var i=1; i<=numLabels; ++i) {

            var x          = this.canvas.width - this.gutterRight;
            var y          = this.canvas.height - this.gutterBottom - (2 * this.bulbRadius) - ((this.graphArea[3] / numLabels) * i);
            var text       = RGraph.number_format(this, String((this.min + (i * step)).toFixed(decimals)), units_pre, units_post);

            RGraph.Text2(this, {'font':font,
                                'size':size,
                                'x':x-6,
                                'y':y,
                                'text':text,
                                'valign':'center',
                                'tag': 'scale'
                               });
        }
        
        // Draw zero
        RGraph.Text2(this, {'font':font,
                            'size':size,
                            'x':x-6,
                            'y':this.canvas.height - this.gutterBottom - (2 * this.bulbRadius),
                            'text':RGraph.number_format(this, (this.min).toFixed(decimals),units_pre,units_post),
                            'valign':'center',
                            'tag': 'scale'
                           });
    }


    /**
    * Returns the focused/clicked bar
    * 
    * @param event  e The event object
    * @param object   The chart object to use (OPTIONAL)
    */
    RGraph.Thermometer.prototype.getShape =
    RGraph.Thermometer.prototype.getBar = function (e)
    {
        for (var i=0; i<this.coords.length; i++) {

            var mouseCoords = RGraph.getMouseXY(e);
            var mouseX = mouseCoords[0];
            var mouseY = mouseCoords[1];

            var left   = this.coords[i][0];
            var top    = this.coords[i][1];
            var width  = this.coords[i][2];
            var height = this.coords[i][3];

            if (    (mouseX >= left && mouseX <= (left + width) && mouseY >= top && mouseY <= (top + height + this.bulbBottomRadius)) // The bulbBottomRadius is added as the rect and the bulb don't fully cover the red bit
                 || RGraph.getHypLength(this.bulbBottomCenterX, this.bulbBottomCenterY, mouseX, mouseY) <= this.bulbBottomRadius) {

            
                var tooltip = RGraph.parseTooltipText ? RGraph.parseTooltipText(this.Get('chart.tooltips'), i) : '';
                

                return {       0: this,   'object': this,
                               1: left,   'x': left,
                               2: top,    'y': top,
                               3: width,  'width': width,
                               4: height, 'height': height,
                               5: i,      'index': i,
                                          'tooltip': tooltip
                       };
            }
        }
        
        return null;
    }


    /**
    * This function returns the value that the mouse is positioned t, regardless of
    * the actual indicated value.
    * 
    * @param object e The event object
    */
    RGraph.Thermometer.prototype.getValue = function (arg)
    {
        if (arg.length == 2) {
            var mouseX = arg[0];
            var mouseY = arg[1];
        } else {
            var mouseCoords = RGraph.getMouseXY(arg);
            var mouseX      = mouseCoords[0];
            var mouseY      = mouseCoords[1];
        }

        var canvas  = this.canvas;
        var context = this.context;

        
        var value = this.graphArea[3] - (mouseY - this.graphArea[1]);
            value = (value / this.graphArea[3]) * (this.max - this.min);
            value = value + this.min;
        
        value = Math.max(value, this.min);
        value = Math.min(value, this.max);

        return value;
    }



    /**
    * Each object type has its own Highlight() function which highlights the appropriate shape
    * 
    * @param object shape The shape to highlight
    */
    RGraph.Thermometer.prototype.Highlight = function (shape)
    {
        if (this.Get('chart.tooltips.highlight')) {

            // Add the new highlight
            //RGraph.Highlight.Rect(this, shape);

            this.context.beginPath();
                this.context.strokeStyle = this.Get('chart.highlight.stroke');
                this.context.fillStyle   = this.Get('chart.highlight.fill');
                this.context.rect(shape['x'],shape['y'],shape['width'],shape['height'] + this.bulbBottomRadius);
                this.context.arc(this.bulbBottomCenterX, this.bulbBottomCenterY, this.bulbBottomRadius - 1, 0, TWOPI, false);
            this.context.stroke;
            this.context.fill();
        }
    }



    /**
    * The getObjectByXY() worker method. Don't call this - call:
    * 
    * RGraph.ObjectRegistry.getObjectByXY(e)
    * 
    * @param object e The event object
    */
    RGraph.Thermometer.prototype.getObjectByXY = function (e)
    {
        var mouseXY = RGraph.getMouseXY(e);

        if (
               mouseXY[0] > this.Get('chart.gutter.left')
            && mouseXY[0] < (this.canvas.width - this.Get('chart.gutter.right'))
            && mouseXY[1] >= this.Get('chart.gutter.top')
            && mouseXY[1] <= (this.canvas.height - this.Get('chart.gutter.bottom'))
            ) {

            return this;
        }
    }



    /**
    * This method handles the adjusting calculation for when the mouse is moved
    * 
    * @param object e The event object
    */
    RGraph.Thermometer.prototype.Adjusting_mousemove = function (e)
    {
        /**
        * Handle adjusting for the Thermometer
        */
        if (RGraph.Registry.Get('chart.adjusting') && RGraph.Registry.Get('chart.adjusting').uid == this.uid) {

            var mouseXY = RGraph.getMouseXY(e);
            var value   = this.getValue(e);
            
            if (typeof(value) == 'number') {

                // Fire the onadjust event
                RGraph.FireCustomEvent(this, 'onadjust');

                this.value = Number(value.toFixed(this.Get('chart.scale.decimals')));

                RGraph.Redraw();
            }
        }
    }



    /**
    * This function positions a tooltip when it is displayed
    * 
    * @param obj object    The chart object
    * @param int x         The X coordinate specified for the tooltip
    * @param int y         The Y coordinate specified for the tooltip
    * @param objec tooltip The tooltips DIV element
    */
    RGraph.Thermometer.prototype.positionTooltip = function (obj, x, y, tooltip, idx)
    {
        var coordX     = obj.coords[tooltip.__index__][0];
        var coordY     = obj.coords[tooltip.__index__][1];
        var coordW     = obj.coords[tooltip.__index__][2];
        var coordH     = obj.coords[tooltip.__index__][3];
        var canvasXY   = RGraph.getCanvasXY(obj.canvas);
        var gutterLeft = obj.Get('chart.gutter.left');
        var gutterTop  = obj.Get('chart.gutter.top');
        var width      = tooltip.offsetWidth;
        var height     = tooltip.offsetHeight;

        // Set the top position
        tooltip.style.left = 0;
        tooltip.style.top  = canvasXY[1] + coordY - height - 7 + 'px';
        
        // By default any overflow is hidden
        tooltip.style.overflow = '';

        // The arrow
        var img = new Image();
            img.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAFCAYAAACjKgd3AAAARUlEQVQYV2NkQAN79+797+RkhC4M5+/bd47B2dmZEVkBCgcmgcsgbAaA9GA1BCSBbhAuA/AagmwQPgMIGgIzCD0M0AMMAEFVIAa6UQgcAAAAAElFTkSuQmCC';
            img.style.position = 'absolute';
            img.id = '__rgraph_tooltip_pointer__';
            img.style.top = (tooltip.offsetHeight - 2) + 'px';
        tooltip.appendChild(img);
        
        // Reposition the tooltip if at the edges:

        // LEFT edge
        if ((canvasXY[0] + coordX - (width / 2)) < 10) {
            tooltip.style.left = (canvasXY[0] + coordX - (width * 0.1)) + (coordW / 2) + 'px';
            img.style.left = ((width * 0.1) - 8.5) + 'px';

        // RIGHT edge
        } else if ((canvasXY[0] + coordX + (width / 2)) > document.body.offsetWidth) {
            tooltip.style.left = canvasXY[0] + coordX - (width * 0.9) + (coordW / 2) + 'px';
            img.style.left = ((width * 0.9) - 8.5) + 'px';

        // Default positioning - CENTERED
        } else {
            tooltip.style.left = (canvasXY[0] + coordX + (coordW / 2) - (width * 0.5)) + 'px';
            img.style.left = ((width * 0.5) - 8.5) + 'px';
        }
    }



    /**
    * Returns the appropriate Y coord for a value
    * 
    * @param number value The value to return the coord for
    */
    RGraph.Thermometer.prototype.getYCoord = function (value)
    {
        if (value > this.max || value < this.min) {
            return null;
        }

        var y = (this.graphArea[1] + this.graphArea[3]) - (((value - this.min) / (this.max - this.min)) * this.graphArea[3]);

        return y;
    }



    /**
    * This returns true/false as to whether the cursor is over the chart area.
    * The cursor does not necessarily have to be over the bar itself.
    */
    RGraph.Thermometer.prototype.overChartArea = function  (e)
    {
        var mouseXY = RGraph.getMouseXY(e);
        var mouseX  = mouseXY[0];
        var mouseY  = mouseXY[1];
        
        // Is the mouse in the "graphArea"?
        // ( with a little extra height added)
        if (   mouseX >= this.graphArea[0]
            && mouseX <= (this.graphArea[0] + this.graphArea[2])
            && mouseY >= this.graphArea[1]
            && mouseY <= (this.graphArea[1] + this.graphArea[3] + this.bulbRadius)
            ) {
            
            return true;
        }
        
        // Is the mouse over the bottom bulb?
        if (RGraph.getHypLength(this.bulbBottomCenterX, this.bulbBottomCenterY, mouseX, mouseY) <= this.bulbRadius) {
            return true;
        }
        
        // Is the mouse over the semi-circle at the top?
        if (RGraph.getHypLength(this.bulbTopCenterX, this.bulbTopCenterY, mouseX, mouseY) <= this.bulbTopRadius) {
            return true;
        }

        return false;
    }


    /**
    * This allows for easy specification of gradients
    */
    RGraph.Thermometer.prototype.parseColors = function ()
    {
        var prop   = this.properties;
        var colors = prop['chart.colors'];

        for (var i=0; i<colors.length; ++i) {
            colors[i] = this.parseSingleColorForGradient(colors[i]);
        }
    }



    /**
    * This parses a single color value
    */
    RGraph.Thermometer.prototype.parseSingleColorForGradient = function (color)
    {
        if (!color || typeof(color) != 'string') {
            return color;
        }

        if (color.match(/^gradient\((.*)\)$/i)) {
            
            var parts = RegExp.$1.split(':');

            // Create the gradient
            var grad = this.context.createLinearGradient(this.properties['chart.gutter.left'],0,
                                                         this.canvas.width - this.properties['chart.gutter.right'],0);

            var diff = 1 / (parts.length - 1);

            grad.addColorStop(0, RGraph.trim(parts[0]));

            for (var j=1; j<parts.length; ++j) {
                grad.addColorStop(j * diff, RGraph.trim(parts[j]));
            }
        }
            
        return grad ? grad : color;
    }

