# This is my README

Installation
============

Requirements
------------
* Python
    http://www.python.org/getit/windows/
* gevent-socketio from `my gevent-socketio repository <https://github.com/jstasiak/gevent-socketio>`_
  (commit ``8835a91dffba4447564ffa30df95663a13e1997e``) and its dependencies
* pip
     https://pypi.python.org/pypi/pip
* django >= 1.3
    https://docs.djangoproject.com/en/dev/topics/install/
We do not need a database
* git ;)
All the documentation about git are on:
    https://confluence.atlassian.com/display/BITBUCKET/bitbucket+101
* PySerial:
    http://pyserial.sourceforge.net/

In Red Hat/CentOS/Fedora they can be obtained by following commands::

    sudo yum install libevent-devel python-devel python-setuptools gcc git-core
    sudo easy_install pip

In Debian/Ubuntu::

    sudo apt-get install libevent-dev python-dev python-setuptools gcc git-core
    sudo easy_install pip

You also want to have Socket.IO client, such client in version 0.9.6 is provided by this project.

* Django Real-time:
    https://raw.github.com/jstasiak/django-realtime

To install dependencies to use chat example please execute::

    pip install -r requiements.txt

Running server
--------------
    python manage.py rungevent 0:8000


Serial connection exemple:
-------------------------
temperature/essai_serial.py

Execute it using
    python essai_serial.py 
